import type { Response } from '@/interface';
import type { IOrderDetailItem, IOrderItem, IOrderParams, IUpdateOrderParams } from '@/interface/order/order';

import { createApi } from '@reduxjs/toolkit/query/react';

import axiosBaseQuery from '@/config/axiosBaseQuery';
import endpoints from '@/constants/endpoints';

export const orderApi = createApi({
  reducerPath: 'orderApi',
  baseQuery: axiosBaseQuery(),
  refetchOnMountOrArgChange: true,
  tagTypes: ['Order'],
  endpoints: build => ({
    orders: build.query<Response<IOrderItem>, Partial<IOrderParams> | void>({
      query: params => ({
        url: endpoints.getOrderEndpoints().orders,
        method: 'GET',
        params,
      }),
      providesTags: [{ type: 'Order', id: 'LIST' }],
    }),
    order: build.query<IOrderItem & IOrderDetailItem, { id: number }>({
      query: ({ id }) => ({
        url: endpoints.getOrderEndpoints(id).orderById,
        method: 'GET',
      }),
    }),
    // updateOrder: build.mutation<Pick<IOrderDetailItem, 'amount' | 'fee'>, { id: number } & IUpdateOrderParams>({
    //   query: ({ id, ...data }) => ({
    //     url: `/${id}`,
    //     method: 'PUT',
    //     data,
    //   }),
    //   invalidatesTags: [{ type: 'Order', id: 'LIST' }],
    // }),
  }),
});

export const { useOrdersQuery, useOrderQuery } = orderApi;
