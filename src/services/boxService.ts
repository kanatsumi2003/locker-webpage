import type { IBoxItem } from '@/interface/box/box';

import { createApi } from '@reduxjs/toolkit/query/react';

import axiosBaseQuery from '@/config/axiosBaseQuery';
import endpoints from '@/constants/endpoints';

export const boxApi = createApi({
  reducerPath: 'boxApi',
  baseQuery: axiosBaseQuery(),
  refetchOnMountOrArgChange: true,
  endpoints: build => ({
    boxes: build.query<IBoxItem[], { id: number }>({
      query: ({ id }) => ({
        url: endpoints.getLockerEndpoints(id).lockerByIdBoxes,
        method: 'GET',
      }),
    }),
  }),
});

export const { useBoxesQuery } = boxApi;
