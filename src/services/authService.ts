import type { ILoginParams, ILoginResponse } from '@/interface/auth/login';
import type { IProfile } from '@/interface/auth/profile';

import { createApi } from '@reduxjs/toolkit/query/react';

import axiosBaseQuery from '@/config/axiosBaseQuery';
import endpoints from '@/constants/endpoints';

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: axiosBaseQuery(),
  refetchOnMountOrArgChange: true,
  endpoints: build => ({
    login: build.mutation<ILoginResponse, ILoginParams>({
      query: data => ({
        url: endpoints.getAuthEndPoints().adminLogin,
        method: 'POST',
        data,
      }),
    }),
    profile: build.query<IProfile, undefined>({
      query: () => ({
        url: endpoints.getAuthEndPoints().adminProfile,
        method: 'GET',
      }),
    }),
  }),
});

export const { useLoginMutation, useProfileQuery } = authApi;
