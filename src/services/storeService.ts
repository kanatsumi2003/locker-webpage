import type { Response } from '@/interface';
import type { ICreateStoreParams, IStoreItem, IStoreParams } from '@/interface/store/store';

import { createApi } from '@reduxjs/toolkit/query/react';

import axiosBaseQuery from '@/config/axiosBaseQuery';
import endpoints from '@/constants/endpoints';

export const storeApi = createApi({
  reducerPath: 'storeApi',
  baseQuery: axiosBaseQuery(),
  refetchOnMountOrArgChange: true,
  keepUnusedDataFor: 5,
  tagTypes: ['Store', 'StoreDetail'],
  endpoints: build => ({
    // GET ALL STORES
    stores: build.query<Response<IStoreItem>, IStoreParams>({
      query: params => ({
        url: endpoints.getStoreEndpoints().stores,
        method: 'GET',
        params,
      }),
      providesTags: [{ type: 'Store', id: 'LIST' }],
    }),

    // CREATE STORE
    createStore: build.mutation<IStoreItem, ICreateStoreParams>({
      query: data => ({
        url: endpoints.getStoreEndpoints().stores,
        method: 'POST',
        data,
      }),
      invalidatesTags: [{ type: 'Store', id: 'LIST' }],
    }),

    // UPDATE STORE
    updateStore: build.mutation<IStoreItem, ICreateStoreParams & { id: number }>({
      query: ({ id, ...data }) => ({
        url: endpoints.getStoreEndpoints(id).storeById,
        method: 'PUT',
        data,
      }),
      invalidatesTags: [
        { type: 'Store', id: 'LIST' },
        { type: 'StoreDetail', id: 'DETAIL' },
      ],
    }),

    // CHANGE STORE STATUS
    updateStoreStatus: build.mutation<IStoreItem, Pick<IStoreItem, 'id' | 'status'>>({
      query: ({ id, ...data }) => ({
        url: endpoints.getStoreEndpoints(id).storeByIdStatus,
        method: 'PUT',
        data,
      }),
      invalidatesTags: [{ type: 'StoreDetail', id: 'DETAIL' }],
    }),

    // GET STORE BY ID
    store: build.query<IStoreItem, { id: number }>({
      query: ({ id }) => ({
        url: endpoints.getStoreEndpoints(id).storeById,
        method: 'GET',
      }),
      providesTags: [{ type: 'StoreDetail', id: 'DETAIL' }],
    }),
  }),
});

export const {
  useStoresQuery,
  useCreateStoreMutation,
  useUpdateStoreMutation,
  useUpdateStoreStatusMutation,
  useStoreQuery,
} = storeApi;
