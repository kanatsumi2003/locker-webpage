import type { Response } from '@/interface';
import type {
  ILockerBody,
  ILockerItem,
  ILockerParams,
  ILockerStatisticItem,
  ILockerTimelineItem,
  ILockerTimelineParams,
} from '@/interface/locker/locker';

import { createApi } from '@reduxjs/toolkit/query/react';

import axiosBaseQuery from '@/config/axiosBaseQuery';
import endpoints from '@/constants/endpoints';

export const lockerApi = createApi({
  reducerPath: 'lockerApi',
  baseQuery: axiosBaseQuery(),
  refetchOnMountOrArgChange: true,
  tagTypes: ['Locker', 'LockerDetail'],
  endpoints: build => ({
    // GET ALL LOCKERS
    lockers: build.query<Response<ILockerItem>, Partial<ILockerParams> | void>({
      query: params => ({
        url: endpoints.getLockerEndpoints().lockers,
        method: 'GET',
        params,
      }),
      providesTags: [{ type: 'Locker', id: 'LIST' }],
    }),

    // GET LOCKER BY ID
    locker: build.query<ILockerItem, { id: number }>({
      query: ({ id }) => ({
        url: endpoints.getLockerEndpoints(id).lockerById,
        method: 'GET',
      }),
      providesTags: [{ type: 'LockerDetail', id: 'DETAIL' }],
    }),

    // CREATE LOCKER
    createLocker: build.mutation<ILockerItem, ILockerBody>({
      query: data => ({
        url: endpoints.getLockerEndpoints().lockers,
        method: 'POST',
        data,
      }),
      invalidatesTags: [{ type: 'Locker', id: 'LIST' }],
    }),

    // UPDATE LOCKER
    updateLocker: build.mutation<{ status: boolean }, ILockerBody & { id: number }>({
      query: ({ id, ...data }) => ({
        url: endpoints.getLockerEndpoints(id).lockerById,
        method: 'PUT',
        data,
      }),
      invalidatesTags: [
        { type: 'Locker', id: 'LIST' },
        { type: 'LockerDetail', id: 'DETAIL' },
      ],
    }),

    // UPDATE LOCKER STATUS
    updateLockerStatus: build.mutation<{ status: boolean }, Pick<ILockerItem, 'id' | 'status'>>({
      query: ({ id, ...data }) => ({
        url: endpoints.getLockerEndpoints(id).lockerByIdStatus,
        method: 'PUT',
        data,
      }),
      invalidatesTags: [
        { type: 'Locker', id: 'LIST' },
        { type: 'LockerDetail', id: 'DETAIL' },
      ],
    }),

    // GET LOCKER TIMELINES BY ID
    lockerTimelines: build.query<Response<ILockerTimelineItem>, { id: number } & Partial<ILockerTimelineParams>>({
      query: ({ id, ...params }) => ({
        url: endpoints.getLockerEndpoints(id).lockerByIdTimelines,
        method: 'GET',
        params,
      }),
    }),

    // GET LOCKER STATISTICS BY ID
    lockerStatistics: build.query<Array<ILockerStatisticItem>, { id: number } & Partial<ILockerTimelineParams>>({
      query: ({ id, ...params }) => ({
        url: endpoints.getLockerEndpoints(id).lockerByIdStatistics,
        method: 'GET',
        params,
      }),
    }),
  }),
});

export const {
  useLockersQuery,
  useLazyLockersQuery,
  useCreateLockerMutation,
  useLockerQuery,
  useUpdateLockerMutation,
  useUpdateLockerStatusMutation,
  useLockerTimelinesQuery,
  useLockerStatisticsQuery,
} = lockerApi;
