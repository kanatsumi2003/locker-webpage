import { LOCAL_STORAGE_ITEMS } from '@/constants/common';

export default function getLocalToken() {
  const token = window.localStorage.getItem(LOCAL_STORAGE_ITEMS.ACCESS_TOKEN);

  return token;
}
