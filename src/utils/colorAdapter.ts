import { LOCKER_EVENT, LOCKER_STATUS } from '@/interface/locker/locker';
import { ORDER_STATUS, ORDER_TYPE } from '@/interface/order/order';

export const mapLockerStatusColor = (value: LOCKER_STATUS) => {
  switch (value) {
    case LOCKER_STATUS.ACTIVE:
      return '#95de64';
    case LOCKER_STATUS.INACTIVE:
      return '#f87171';
    case LOCKER_STATUS.DISCONNECTED:
      return '#a1a1aa';
    case LOCKER_STATUS.MAINTAINING:
      return '#fde68a';
    case LOCKER_STATUS.INITIALIZED:
      return '#d9d9d9';
    default:
      return '#a1a1aa';
  }
};

export const mapOrderTypeColor = (value: ORDER_TYPE) => {
  switch (value) {
    case ORDER_TYPE.LAUNDRY:
      return '#91caff';
    case ORDER_TYPE.STORAGE:
      return '#b7eb8f';
    default:
      return '#a1a1aa';
  }
};

export const mapOrderStatusColor = (value: ORDER_STATUS) => {
  switch (value) {
    case ORDER_STATUS.CANCELED:
      return '#f87171';
    case ORDER_STATUS.COMPLETED:
      return '#95de64';
    case ORDER_STATUS.PROCESSING:
      return '#b7eb8f';
    case ORDER_STATUS.RESERVED:
      return '#fde68a';
    case ORDER_STATUS.INITIALIZED:
      return '#d9d9d9';
    case ORDER_STATUS.RETURNED:
      return '#86efac';
    case ORDER_STATUS.WAITING:
      return '#bfbfbf';
    default:
      return '#a1a1aa';
  }
};

export const mapLockerEventColor = (value: LOCKER_EVENT) => {
  switch (value) {
    case LOCKER_EVENT.CONNECT:
      return '#b7eb8f';
    case LOCKER_EVENT.OVERLOAD:
      return '#ffccc7';
    case LOCKER_EVENT.UPDATE_INFORMATION:
      return '#b5f5ec';
    case LOCKER_EVENT.UPDATE_STATUS:
      return '#91caff';
    case LOCKER_EVENT.DISCONNECT:
      return '#d9d9d9';
    default:
      return '#d9d9d9';
  }
};
