import type { UploadChangeParam, UploadFile } from 'antd/es/upload';

export const getFile = (e: UploadChangeParam<UploadFile<any>>) => {
  return e && e.fileList && e.fileList[0].response?.url;
};
