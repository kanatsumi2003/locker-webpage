export function formatSearch(se: string) {
  se = decodeURIComponent(se);
  se = se.substr(1); //从起始索引号提取字符串中指定数目的字符
  const arr = se.split('&'); //把字符串分割为字符串数组
  const obj: Record<string, string> = {};
  let newarr = [];

  arr.forEach((v, i) => {
    //数组遍历
    console.log(v);
    console.log(i);
    newarr = v.split('=');

    if (typeof obj[newarr[0]] === 'undefined') {
      obj[newarr[0]] = newarr[1];
    }
  });

  return obj;
}

export const toLowerCaseNonAccentVietnamese = (str?: string) => {
  if (!str) {
    return '';
  }

  str = str.toLowerCase();

  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, '');
  str = str.replace(/\u02C6|\u0306|\u031B/g, '');

  return str;
};
