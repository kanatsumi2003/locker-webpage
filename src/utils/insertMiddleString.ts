import { LOCKER_EVENT } from '@/interface/locker/locker';

export const insertMiddleString = (str: string, index: number, value: string) => {
  return str.substr(0, index) + value + str.substr(index);
};

export const mappingEventValue = (event?: LOCKER_EVENT) => {
  switch (event) {
    case LOCKER_EVENT.CONNECT:
      return 'Connect';
    case LOCKER_EVENT.OVERLOAD:
      return 'Overload';
    case LOCKER_EVENT.UPDATE_INFORMATION:
      return 'Update Information';
    case LOCKER_EVENT.UPDATE_STATUS:
      return 'Update Status';
    case LOCKER_EVENT.DISCONNECT:
      return 'Disconnect';
    default:
      return '';
  }
};
