export const formatDecimalPrecision = (value: number | string) => {
  return value ? `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',') : undefined;
};
