export const required = (field: string): { required: boolean; message: string } => {
  return {
    required: true,
    message: `${field} is required.`,
  };
};

export const greaterThanZero = (field: string): { min: number; message: string } => {
  return {
    min: 0,
    message: `${field} must be greater than 0.`,
  };
};
