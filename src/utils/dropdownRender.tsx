import { Tag } from 'antd';

import { ACCOUNT_STATUS } from '@/interface/auth/profile';
import { BOX_STATUS } from '@/interface/box/box';
import { LOCKER_STATUS } from '@/interface/locker/locker';
import { ORDER_STATUS, ORDER_TYPE } from '@/interface/order/order';
import { SERVICE_STATUS } from '@/interface/service/service';
import { STAFF_STATUS } from '@/interface/staff/staff';
import { STORE_STATUS } from '@/interface/store/store';

export const dropdownRenderStoreStatus = (value: STORE_STATUS) => {
  const status = () => {
    switch (value) {
      case STORE_STATUS.ACTIVE:
        return 'success';
      default:
        return 'error';
    }
  };

  return (
    <Tag color={status()} style={{ minWidth: '5rem', textAlign: 'center', marginRight: 0 }}>
      <span>{value?.toUpperCase()}</span>
    </Tag>
  );
};

export const dropdownRenderLockerStatus = (value: LOCKER_STATUS) => {
  const status = () => {
    switch (value) {
      case LOCKER_STATUS.ACTIVE:
        return 'success';
      case LOCKER_STATUS.INACTIVE:
      case LOCKER_STATUS.DISCONNECTED:
        return 'error';
      case LOCKER_STATUS.MAINTAINING:
        return 'warning';
      default:
        return 'processing';
    }
  };

  return (
    <Tag color={status()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      <span>{value?.toUpperCase()}</span>
    </Tag>
  );
};

export const dropdownRenderOrderStatus = (value: ORDER_STATUS) => {
  const color = () => {
    switch (value) {
      case ORDER_STATUS.INITIALIZED:
        return 'orange';
      case ORDER_STATUS.WAITING:
        return 'cyan';
      case ORDER_STATUS.PROCESSING:
        return 'gold';
      case ORDER_STATUS.RETURNED:
        return 'lime';
      case ORDER_STATUS.COMPLETED:
        return 'success';
      case ORDER_STATUS.RESERVED:
        return 'volcano';
      default:
        return 'error';
    }
  };

  return (
    <Tag color={color()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      {value?.toUpperCase()}
    </Tag>
  );
};

export const dropdownRenderOrderType = (value: ORDER_TYPE) => {
  const color = () => {
    switch (value) {
      case ORDER_TYPE.LAUNDRY:
        return 'green';
      default:
        return 'blue';
    }
  };

  return (
    <Tag color={color()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      {value?.toUpperCase()}
    </Tag>
  );
};

export const dropdownRenderServiceStatus = (value: SERVICE_STATUS) => {
  const status = () => {
    switch (value) {
      case SERVICE_STATUS.ACTIVE:
        return 'success';
      default:
        return 'error';
    }
  };

  return (
    <Tag color={status()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      <span>{value?.toUpperCase()}</span>
    </Tag>
  );
};

export const dropdownRenderStaffStatus = (value: STAFF_STATUS) => {
  const status = () => {
    switch (value) {
      case STAFF_STATUS.ACTIVE:
        return 'success';
      case STAFF_STATUS.INACTIVE:
        return 'error';
      default:
        return 'processing';
    }
  };

  return (
    <Tag color={status()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      <span>{value?.toUpperCase()}</span>
    </Tag>
  );
};

export const dropdownRenderAccountStatus = (value: ACCOUNT_STATUS) => {
  const status = () => {
    switch (value) {
      case ACCOUNT_STATUS.ACTIVE:
        return 'success';
      case ACCOUNT_STATUS.INACTIVE:
        return 'error';
      default:
        return 'processing';
    }
  };

  return (
    <Tag color={status()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      <span>{value?.toUpperCase()}</span>
    </Tag>
  );
};

export const dropdownRenderBoxStatus = (value: BOX_STATUS) => {
  const status = () => {
    switch (value) {
      case BOX_STATUS.ACTIVE:
        return 'success';
      case BOX_STATUS.INACTIVE:
        return 'error';
      default:
        return 'processing';
    }
  };

  return (
    <Tag color={status()} style={{ minWidth: '5rem', textAlign: 'center' }}>
      <span>{value?.toUpperCase()}</span>
    </Tag>
  );
};
