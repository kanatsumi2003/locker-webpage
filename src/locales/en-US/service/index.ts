export const en_US_service = {
  'app.service.action.title': 'Service',
  'app.service.action.create': 'Create Locker',
  'app.service.action.update': 'Update Locker',
  'app.service.action.next': 'Next',
  'app.service.action.previous': 'Previous',
  'app.service.action.done': 'Done',
  'app.service.field.name': 'Service Name',
  'app.service.field.fee': 'Service Fee',
  'app.service.field.feeType': 'Service Fee Type',
  'app.service.field.description': 'Description',
};
