export const en_US_order = {
  'app.order.action.title': 'Order',
  'app.order.action.create': 'Create Order',
  'app.order.action.update': 'Update Order',
  'app.order.action.next': 'Next',
  'app.order.action.previous': 'Previous',
  'app.order.action.done': 'Done',
  'app.order.field.sender': 'Sender',
  'app.order.field.receiver': 'Receiver',
  'app.order.field.sendBox': 'Send Box',
  'app.order.field.receiveBox': 'Receive Box',
  'app.order.field.lockerName': 'Locker Name',
  'app.order.field.receiveTime': 'Receive Time',
  'app.order.field.actualReceiveTime': 'Actual Receive Time',
  'app.order.field.lockerAddress': 'Locker Address',
  'app.order.field.lockerCode': 'Locker Code',
  'app.order.field.lockerStatus': 'Locker Status',
  'app.order.field.unitPrice': 'Unit Price',
  'app.order.field.fee': 'Fee',
  'app.order.field.amount': 'Amount',
  'app.order.field.description': 'Description',
  'app.order.field.duration': 'Duration',
  'app.order.field.currentTotal': 'Current Total',
  'app.order.field.discount': 'Discount',
  'app.order.field.payout': 'Total Payout',
};
