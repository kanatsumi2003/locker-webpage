import type { Timestamp } from '..';

export enum ROLE {
  ADMIN = 'Admin',
  STAFF = 'Staff',
  CUSTOMER = 'Customer',
}

export enum ACCOUNT_STATUS {
  ACTIVE = 'Active',
  INACTIVE = 'Inactive',
  VERIFYING = 'Verifying',
}

export interface IProfile extends Timestamp {
  id: number;
  username: string;
  phoneNumber: string;
  role: ROLE;
  status: ACCOUNT_STATUS;
  fullName: string;
  description: string;
  avatar: string;
}
