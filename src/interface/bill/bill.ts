import type { Timestamp } from '..';

export enum PAYMENT_METHOD {
  VNPAY = 'VnPay',
  MOMO = 'Momo',
}

export interface IBillItem extends Timestamp {
  id: number;
  referenceOrderId: number;
  amount: number;
  method: PAYMENT_METHOD;
  content: string;
  referenceTransactionId: string;
}
