export interface DrawerState {
  title?: string;
  visible: boolean;
  flag?: string;
}
