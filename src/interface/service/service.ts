import type { Paging, Response, Timestamp } from '..';

export enum FEE_TYPE {
  BY_TIME = 'ByTime',
  BY_UNIT = 'ByUnitPrice',
  BY_INPUT = 'ByInputPrice',
}

export enum SERVICE_STATUS {
  ACTIVE = 'Active',
  INACTIVE = 'Inactive',
}

export interface IServiceItem extends Timestamp {
  id: number;
  name: string;
  image: string;
  price: number;
  description: string;
  unit: string;
  status: SERVICE_STATUS;
}

export interface IServiceParams extends Partial<Paging> {
  search?: string;
  status?: SERVICE_STATUS;
}
