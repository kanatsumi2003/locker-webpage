import type { Paging, Timestamp } from '..';
import type { IProfile } from '../auth/profile';
import type { IBillItem } from '../bill/bill';
import type { IBoxItem } from '../box/box';
import type { IRangePicker } from '../layout/index.interface';
import type { ILocationParams } from '../location/location';
import type { ILockerItem } from '../locker/locker';
import type { IServiceItem } from '../service/service';
import type { IStaffItem } from '../staff/staff';
import type { IStoreItem } from '../store/store';

export enum ORDER_STATUS {
  INITIALIZED = 'Initialized',
  WAITING = 'Waiting',
  PROCESSING = 'Processing',
  RETURNED = 'Returned',
  COMPLETED = 'Completed',
  CANCELED = 'Canceled',
  RESERVED = 'Reserved',
}

export enum ORDER_TYPE {
  LAUNDRY = 'Laundry',
  STORAGE = 'Storage',
}

export interface ITimeLineItem extends Timestamp {
  id: number;
  status: ORDER_STATUS;
  previousStatus: ORDER_STATUS;
  description: string;
}

export interface IDetailItem extends Timestamp {
  id: number;
  service: IServiceItem;
  quantity: number;
  price: number;
}

export interface IOrderItem extends Timestamp {
  id: number;
  type: ORDER_TYPE;
  pinCode?: number;
  pinCodeIssuedAt?: string;
  sendBox: IBoxItem;
  receiveBox: IBoxItem;
  sender: IProfile;
  receiver: IProfile;
  receiveAt: string;
  staff: IStaffItem;
  status: ORDER_STATUS;
  price: number;
  extraCount: number;
  extraFee: number;
  discount: number;
  description?: string;
  locker: ILockerItem;
  store: IStoreItem;
}

export interface IOrderDetailItem extends IOrderItem {
  timelines: ITimeLineItem[];
  details: IDetailItem[];
  bill: IBillItem;
}

export interface IOrderParams extends Partial<Paging> {
  query?: string;
  lockerId?: number;
  type?: ORDER_TYPE;
  status?: ORDER_STATUS;
  from?: string;
  to?: string;
  staffId?: number;
  customerId?: number;
  storeId?: number;
  serviceId?: number;
}

export interface IUpdateOrderParams {
  amount: number;
  fee: number;
  description: string;
}

export interface IServiceStatisticItem {
  service: string;
  totalOrders: number;
  totalRevenue: number;
}

export interface IDashboard {
  totalOrders: number;
  totalRevenue: number;
  serviceStatistic: IServiceStatisticItem[];
}

export interface IDashboardParams extends Partial<Paging>, ILocationParams, IRangePicker {
  year?: number;
  lockerId?: number;
  storeId?: number;
  search?: string;
}

export interface IDashboardStoreParams extends Partial<Paging> {
  search?: string;
  from?: string;
  to?: string;
}
