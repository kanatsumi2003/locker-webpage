const BASE_URL = import.meta.env.VITE_API_LOCKERS || '';
const BASE_URL_LOCKER = BASE_URL + '/lockers';
const BASE_URL_ORDER = BASE_URL + '/orders';
const BASE_URL_DASHBOARD = BASE_URL + '/dashboard';
const BASE_URL_ADDRESS = BASE_URL + '/addresses';
const BASE_URL_AUTH = BASE_URL + '/auth';
const BASE_URL_FILE = BASE_URL + '/file';
const BASE_URL_SERVICE = BASE_URL + '/services';
const BASE_URL_STAFF = BASE_URL + '/staffs';
const BASE_URL_STORE = BASE_URL + '/stores';
const BASE_URL_GEOLOCATION = import.meta.env.VITE_API_GEOLOCATION;

const getAuthEndPoints = () => {
  const ADMIN_AUTH_URL = BASE_URL_AUTH + '/admin';

  return {
    adminLogin: `${ADMIN_AUTH_URL}/login`,
    adminProfile: `${ADMIN_AUTH_URL}/profile`,
    refreshToken: `${BASE_URL_AUTH}/refresh`,
  };
};

const getStoreEndpoints = (id?: number) => {
  return {
    stores: `${BASE_URL_STORE}`,
    storeById: `${BASE_URL_STORE}/${id}`,
    storeByIdStatus: `${BASE_URL_STORE}/${id}/status`,
  };
};

const getLockerEndpoints = (id?: number) => {
  return {
    lockers: `${BASE_URL_LOCKER}`,
    lockerById: `${BASE_URL_LOCKER}/${id}`,
    lockerWithStaffs: `${BASE_URL_LOCKER}/${id}/staffs`,
    lockerByIdStatus: `${BASE_URL_LOCKER}/${id}/status`,
    lockerByIdBoxes: `${BASE_URL_LOCKER}/${id}/boxes`,
    lockerByIdTimelines: `${BASE_URL_LOCKER}/${id}/timelines`,
    lockerByIdStatistics: `${BASE_URL_LOCKER}/${id}/statistics`,
  };
};

const getHardwareEndpoints = (id: number, hardwareId?: number) => {
  return {
    hardwares: `${BASE_URL_LOCKER}/${id}/hardwares`,
    hardwareById: `${BASE_URL_LOCKER}/${id}/hardwares/${hardwareId}`,
  };
};

const getServiceEndpoints = (id?: number) => {
  return {
    services: `${BASE_URL_SERVICE}`,
    serviceById: `${BASE_URL_SERVICE}/${id}`,
    serviceByIdStatus: `${BASE_URL_SERVICE}/${id}/status`,
  };
};

const getOrderEndpoints = (id?: number) => {
  return {
    orders: `${BASE_URL_ORDER}`,
    orderById: `${BASE_URL_ORDER}/${id}`,
  };
};

const getAddressEndpoints = (q?: string) => {
  return {
    addresses: BASE_URL_ADDRESS,
    geoLocation: BASE_URL_GEOLOCATION + (q ? `&q=${q}` : ''),
  };
};

const getStaffEndpoints = () => {
  return {
    staff: `${BASE_URL_STAFF}`,
    staffById: (id: number) => `${BASE_URL_STAFF}/${id}`,
    staffByIdStatus: (id: number) => `${BASE_URL_STAFF}/${id}/status`,
  };
};

const getDashboardEndPoints = () => {
  return {
    overview: `${BASE_URL_DASHBOARD}/overview`,
    stores: `${BASE_URL_DASHBOARD}/stores`,
    revenue: `${BASE_URL_DASHBOARD}/revenue`,
    lockers: `${BASE_URL_DASHBOARD}/lockers`,
    lockerLocations: `${BASE_URL_DASHBOARD}/lockers/locations`,
    orders: `${BASE_URL_DASHBOARD}/orders`,
  };
};

const endpoints = {
  getAuthEndPoints,
  getStoreEndpoints,
  getLockerEndpoints,
  getHardwareEndpoints,
  getServiceEndpoints,
  getOrderEndpoints,
  getAddressEndpoints,
  getStaffEndpoints,
  getDashboardEndPoints,
};

export default endpoints;
