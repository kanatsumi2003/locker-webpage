export const LOCAL_STORAGE_ITEMS = Object.freeze({
  ACCESS_TOKEN: 'accessToken',
  REFRESH_TOKEN: 'refreshToken',
});

export const FULL_TIME_FORMAT = 'HH:mm:ss DD/MM/YYYY';

export const PATH = {
  LOGIN: '/login',
  HOME: '/',
  DASHBOARD: '/dashboard',
  LOCKER: '/lockers',
  LOCKER_DETAIL: '/lockers/:id',
  ORDER: '/orders',
  STORE: '/stores',
  STORE_DETAIL: '/stores/:id',
  SERVICE: '/services',
  SERVICE_DETAIL: '/services/:id',
  ORDER_DETAIL: '/orders/:id',
  STAFF: '/staffs',
  STAFF_DETAIL: '/staffs/:id',
};

export const ICON = {
  DASHBOARD: 'dashboard',
  STORE: 'store',
  LOCKER: 'locker',
  SERVICE: 'service',
  ORDER: 'order',
  STAFF: 'staff',
};

export const STATUS_CODE = {
  NOT_FOUND: 404,
  BAD_REQUEST: 400,
  NOT_ALLOWED: 403,
  INTERNAL_SERVER_ERROR: 500,
};

export const CURRENCY_UNIT = {
  VI: 'VND',
};

export const PASSWORD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
export const PASSWORD_REGEX_GENERATOR = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8}$/;
