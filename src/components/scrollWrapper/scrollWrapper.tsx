import './index.less';

import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import React, { useRef, useState } from 'react';

interface Props {
  children: React.ReactNode;
}

const ScrollWrapper: React.FC<Props> = ({ children }) => {
  const [isVisibleLeftButton, setIsVisibleLeftButton] = useState<boolean>(false);
  const [isVisibleRightButton, setIsVisibleRightButton] = useState<boolean>(false);
  const ref = useRef<HTMLDivElement>(null);

  window.addEventListener('resize', () => {
    if (!ref || !ref.current) {
      return;
    }

    if (ref.current!.offsetWidth < ref.current!.scrollWidth) {
      setIsVisibleRightButton(true);
    } else {
      setIsVisibleLeftButton(false);
      setIsVisibleRightButton(false);
    }
  });

  const handleScrollLeft = () => {
    ref.current!.scrollLeft -= ref.current!.scrollWidth;
    setIsVisibleLeftButton(false);
    setIsVisibleRightButton(true);
  };

  const handleScrollRight = () => {
    ref.current!.scrollLeft += ref.current!.scrollWidth;
    setIsVisibleLeftButton(true);
    setIsVisibleRightButton(false);
  };

  const handleScroll = (e: React.UIEvent<HTMLDivElement>) => {
    if (e.currentTarget.scrollLeft > 0) {
      setIsVisibleRightButton(false);
      setIsVisibleLeftButton(true);
    } else {
      setIsVisibleRightButton(true);
      setIsVisibleLeftButton(false);
    }
  };

  return (
    <div className="select-group-wrapper">
      <div className="select-group" ref={ref} onScroll={handleScroll}>
        <div
          className={`btn-wrapper left-btn ${isVisibleLeftButton ? 'visible' : 'invisible'}`}
          onClick={handleScrollLeft}
        >
          <Button icon={<LeftOutlined />} className="rounded-btn" />
        </div>

        <div className="space-x">{children}</div>

        <div
          className={`btn-wrapper right-btn ${isVisibleRightButton ? 'visible' : 'invisible'}`}
          onClick={handleScrollRight}
        >
          <Button icon={<RightOutlined />} className="rounded-btn right-btn" />
        </div>
      </div>
    </div>
  );
};

export default ScrollWrapper;
