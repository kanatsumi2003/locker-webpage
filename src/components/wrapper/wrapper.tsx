import './index.less';

import { Card } from 'antd';
import React from 'react';

interface Props {
  children: React.ReactElement;
}

const Wrapper = ({ children }: Props) => {
  return <Card className="component-wrapper">{children}</Card>;
};

export default Wrapper;
