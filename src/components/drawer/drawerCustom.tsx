import { Drawer } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';

import { PATH } from '@/constants/common';
import FormLocker from '@/pages/locker/formLocker';
import FormUpdateOrder from '@/pages/order/formUpdateOrder';
import OrderDetailForm from '@/pages/orderDetail';
import FormService from '@/pages/service/formService';
import FormStaff from '@/pages/staff/formStaff';
import FormStore from '@/pages/store/formStore';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';

interface ChildrenType {
  [key: string]: {
    child: JSX.Element;
  };
}

const DrawerCustom = () => {
  const location = useLocation();
  const dispatch = useDispatch();
  const { title, visible, flag } = useSelector(state => state.drawer);

  const children: ChildrenType = {
    [PATH.LOCKER]: {
      child: <FormLocker />,
    },
    [PATH.ORDER]: {
      child: flag === 'details' ? <OrderDetailForm /> : <FormUpdateOrder />,
    },
    [PATH.STORE]: {
      child: <FormStore />,
    },
    [PATH.SERVICE]: {
      child: <FormService />,
    },
    [PATH.STAFF]: {
      child: <FormStaff />,
    },
  };

  const onClose = () => {
    dispatch(closeDrawer());
  };

  useEffect(() => {
    if (!visible) {
      dispatch(refreshRowKeys());
    }
  }, [visible]);

  return (
    <Drawer
      width={window.innerWidth > 768 ? window.innerWidth / 2 : window.innerWidth}
      placement="right"
      size="large"
      open={visible}
      title={title}
      onClose={onClose}
    >
      {children[`/${location?.pathname?.split('/')[1]}`]?.child}
    </Drawer>
  );
};

export default DrawerCustom;
