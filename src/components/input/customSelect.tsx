import type { OptionType } from '@/interface';
import type { SelectProps } from 'antd';
import type { SizeType } from 'antd/es/config-provider/SizeContext';
import type { Dispatch, ReactNode, SetStateAction } from 'react';

import { Select } from 'antd';
import React, { useState } from 'react';

import { toLowerCaseNonAccentVietnamese } from '@/utils/formatSearch';

const { Option } = Select;

interface Props<T> extends SelectProps {
  name: keyof T;
  setParams: Dispatch<SetStateAction<T>>;
  children?: ReactNode;
  isRemoveOption?: boolean;
  options?: OptionType[];
  onClear?: () => void;
  size?: SizeType;
}

const CustomSelect = <T extends object = object>({
  name,
  setParams,
  children,
  isRemoveOption,
  options,
  onClear,
  size = 'large',
  ...props
}: Props<T>) => {
  const [searchValue, setSearchValue] = useState<string>('');

  return (
    <Select
      {...props}
      style={{ minWidth: 160 }}
      size={size}
      allowClear
      showSearch
      onClear={() => {
        onClear && onClear();
        setParams((prev: T) => ({ ...prev, [name]: undefined }));
      }}
      onSelect={(value: string) => setParams((prev: T) => ({ ...prev, [name]: value }))}
      onSearch={value => setSearchValue(value)}
      searchValue={searchValue}
      filterOption={(input: string) =>
        toLowerCaseNonAccentVietnamese(searchValue).indexOf(toLowerCaseNonAccentVietnamese(input)) >= 0
      }
    >
      {children ||
        options
          ?.filter(
            (item: OptionType) =>
              toLowerCaseNonAccentVietnamese(item?.label).indexOf(toLowerCaseNonAccentVietnamese(searchValue)) >= 0,
          )
          .map((item: OptionType) => (
            <Option key={item.value} value={item.value}>
              {item.label}
            </Option>
          ))}
    </Select>
  );
};

export default React.memo(CustomSelect) as typeof CustomSelect;
