import type { InputProps } from 'antd';
import type { Dispatch, SetStateAction } from 'react';

import { Input } from 'antd';
import React, { useEffect, useState } from 'react';

import { useDebounce } from '@/hooks/useDebounce';

interface Props<T> extends InputProps {
  query: keyof T;
  setParams: Dispatch<SetStateAction<T>>;
}

const CustomSearch = <T extends object = object>({ query, setParams, ...props }: Props<T>) => {
  const [search, setSearch] = useState<string | null>(null);
  const searchDebounce = useDebounce(search, 300);

  useEffect(() => {
    setParams((prev: T) => ({ ...prev, [query]: searchDebounce || undefined }));
  }, [searchDebounce]);

  return <Input.Search {...props} size="large" allowClear onChange={e => setSearch(e.target.value)} />;
};

export default React.memo(CustomSearch) as typeof CustomSearch;
