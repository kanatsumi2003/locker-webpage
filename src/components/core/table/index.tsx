import type { TableProps } from 'antd';

import './index.less';

import { css } from '@emotion/react';
import { Table } from 'antd';
import { useDispatch, useSelector } from 'react-redux';

import { setSelectedRecord, setSelectedRowKeys } from '@/stores/table.store';

import TableColumn from '../table-column';

interface TableCustomProps<T extends object> extends TableProps<T> {
  height: string;
  checkBox: boolean;
  scroll: Record<'x' | 'y', number | string>;
}

const TableCustom = <T extends object = object>(props: Partial<TableCustomProps<T>>) => {
  const { height, checkBox = false, scroll = { x: 1024, y: '45vh' }, ...rest } = props;
  const { selectedRowKeys } = useSelector(state => state.table);
  const dispatch = useDispatch();

  const rowSelection = {
    selectedRowKeys: selectedRowKeys,

    onChange: (keys: React.Key[], record: any) => {
      dispatch(setSelectedRowKeys(keys));
      dispatch(setSelectedRecord(record));
    },
  };

  return (
    <div style={{ height }} css={styles}>
      <Table<T>
        {...rest}
        scroll={scroll}
        pagination={false}
        {...(checkBox && { rowSelection })}
        rowClassName={'row-table'}
        sortDirections={['ascend', 'descend']}
      />
    </div>
  );
};

TableCustom.defaultProps = {
  size: 'small',
  height: 'auto',
} as TableCustomProps<any>;

TableCustom.Column = TableColumn;
TableCustom.ColumnGroup = Table.ColumnGroup;

export default TableCustom;

const styles = css`
  display: flex;
  flex-direction: column;
  overflow: hidden;

  .ant-table-wrapper,
  .ant-spin-nested-loading,
  .ant-spin-container,
  .ant-table-container {
    height: 100%;
  }
  .ant-spin-container {
    overflow: hidden;
    display: flex;
    flex-direction: column;

    .ant-table {
      flex: 1;
      overflow: hidden;
      border-bottom: 1px solid #eee;

      .ant-table-container {
        display: flex;
        flex-direction: column;
        .ant-table-body {
          flex: 1;
          table {
            height: 100%;
          }
        }
      }
    }

    .ant-pagination {
      padding: 0 10px;
    }
  }
`;
