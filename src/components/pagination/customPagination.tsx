import type { PaginationProps } from 'antd';
import type { Dispatch, SetStateAction } from 'react';

import { Pagination } from 'antd';
import React from 'react';

interface Props<T> extends PaginationProps {
  pageNumber?: number;
  pageSize?: number;
  total?: number;
  setParams: Dispatch<SetStateAction<T>>;
}

const CustomPagination = <T extends object = object>({
  pageNumber = 1,
  pageSize = 20,
  total = 20,
  setParams,
  ...props
}: Props<T>) => {
  const showTotal: PaginationProps['showTotal'] = total => `Total ${total} items`;

  return (
    <Pagination
      style={{ float: 'right', marginTop: '1rem' }}
      size="default"
      pageSizeOptions={['10', '20', '50', '100', '200']}
      current={pageNumber}
      pageSize={pageSize}
      total={total}
      showTotal={showTotal}
      showSizeChanger
      showQuickJumper
      hideOnSinglePage
      onChange={(page, pageSize) => setParams((prev: T) => ({ ...prev, ...{ pageNumber: page, pageSize } }))}
      responsive
      {...props}
    />
  );
};

export default React.memo(CustomPagination) as typeof CustomPagination;
