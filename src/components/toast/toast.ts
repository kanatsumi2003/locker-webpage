import type { IconType } from 'antd/es/notification/interface';

import { notification } from 'antd';

type NotificationType = {
  type: IconType;
  message?: string;
  description?: string;
  duration?: number | null;
};

export const Toast = (values: NotificationType) => {
  const { type, message, description } = values;

  return notification[type]({
    message,
    description,
  });
};
