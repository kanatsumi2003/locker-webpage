import { Statistic } from 'antd';
import CountUp from 'react-countup';

type Props = {
  value?: number;
  title?: string;
};

const countUpFormatter = (value: number | string) => <CountUp end={Number(value)} separator="," />;

const StatisticCustom = ({ value, title }: Props) => {
  return <Statistic value={value} title={title} formatter={countUpFormatter} />;
};

export default StatisticCustom;
