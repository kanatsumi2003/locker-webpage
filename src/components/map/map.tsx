import type { DragEndEvent, LatLngTuple, Map } from 'leaflet';
import type { ReactElement } from 'react';

import 'leaflet/dist/leaflet.css';

import L from 'leaflet';
import { useEffect, useState } from 'react';
import { renderToString } from 'react-dom/server';
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet';

interface ILocation {
  location: LatLngTuple;
}

interface IMarker extends ILocation {
  popup: React.ReactNode;
  icon?: ReactElement;
}

interface Props {
  markers?: IMarker[];
  center?: ILocation;
  scrollWheelZoom?: boolean;
  markerDraggable?: boolean;
  onMarkerDragEnd?: (e: DragEndEvent) => void;
}

const CustomMap = ({ markers = [], center, scrollWheelZoom, markerDraggable = false, onMarkerDragEnd }: Props) => {
  const [mapInstance, setMapInstance] = useState<Map>();

  useEffect(() => {
    mapInstance && center && mapInstance.flyTo(center.location);

    // timeout to resize map
    const timeout = setTimeout(function () {
      mapInstance && mapInstance.invalidateSize();
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [center]);

  return (
    <MapContainer
      ref={e => e && setMapInstance(e)}
      center={center ? center.location : [0, 0]}
      zoom={12}
      scrollWheelZoom={scrollWheelZoom}
      style={{ width: '100%', height: '500px' }}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {markers.map((marker, index) => (
        <Marker
          position={marker.location}
          key={index}
          {...(marker.icon && {
            icon: L.divIcon({
              html: renderToString(marker.icon),
              className: 'mapMarkerIcon',
            }),
          })}
          draggable={markerDraggable}
          eventHandlers={{
            dragend: onMarkerDragEnd,
          }}
        >
          {marker.popup && <Popup>{marker.popup}</Popup>}
        </Marker>
      ))}
    </MapContainer>
  );
};

export default CustomMap;
