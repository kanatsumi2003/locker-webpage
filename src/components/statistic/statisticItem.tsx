import './index.less';

import { Card, Col, Row } from 'antd';

import { hexToRGB } from '@/utils/format';

import StatisticCustom from './statisticCustom';

type Props = {
  value?: number;
  title?: string;
  color: string;
  icon: React.ReactNode;
};

const StatisticItem = ({ value, title, color, icon }: Props) => {
  return (
    <Card bordered={false} style={{ minWidth: '200px', height: '100%' }} className="card-statistic">
      <Row align={'middle'} justify={'center'} style={{ height: '100%' }}>
        <Col flex={10} style={{ display: 'flex' }}>
          <div
            style={{
              width: '52px',
              height: '52px',
              margin: 'auto',
              backgroundColor: hexToRGB(color, 0.2),
              display: 'flex',
              borderRadius: '100%',
              color: color,
              fontSize: '24px',
              justifyContent: 'center',
            }}
          >
            {icon}
          </div>
        </Col>
        <Col flex={14}>
          <StatisticCustom value={value} title={title} />
        </Col>
      </Row>
    </Card>
  );
};

export default StatisticItem;
