import { Statistic, Typography } from 'antd';
import CountUp from 'react-countup';

import { formatDecimalPrecision } from '@/utils/decimalPrecision';

type Props = {
  value?: number;
  title?: string;
};

const countUpFormatter = (value: number | string) => (
  <Typography.Title level={2}>
    <CountUp end={Number(value)} separator="," />
  </Typography.Title>
);

const StatisticCustom = ({ value, title }: Props) => {
  return (
    <Statistic
      title={title}
      value={formatDecimalPrecision(value ?? 0) ?? 0}
      formatter={countUpFormatter}
      style={{ textAlign: 'center' }}
    />
  );
};

export default StatisticCustom;
