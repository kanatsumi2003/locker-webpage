import type { FormInstance, UploadFile, UploadProps } from 'antd';
import type { UploadChangeParam } from 'antd/es/upload';

import { InboxOutlined } from '@ant-design/icons';
import { message, Upload } from 'antd';
import React, { useEffect } from 'react';

const { Dragger } = Upload;

const uploadProps: UploadProps = {
  name: 'file',
  action: `${import.meta.env.VITE_API_FILE}`,
  onDrop(e) {
    console.log('Dropped files', e.dataTransfer.files);
  },
};

interface Props extends UploadProps {
  multiple: boolean;
  maxCount: number;
  setFileMap: React.Dispatch<React.SetStateAction<Map<number, UploadFile[]>>>;
  fileMap: Map<number, UploadFile[]>;
  fieldKey: number;
  form: FormInstance<any>;
  nameList: string;
  name: string;
}

const DraggerUpload = ({
  multiple = false,
  maxCount = 1,
  fieldKey,
  setFileMap,
  fileMap,
  form,
  nameList,
  name,
  ...props
}: Partial<Props>) => {
  const onChange = (info: UploadChangeParam<UploadFile<any>>, fieldKey?: number) => {
    const { status } = info.file;

    if (status === 'done') {
      if (fileMap && setFileMap && form && typeof fieldKey === 'number') {
        const newFileMap = new Map(fileMap).set(fieldKey, info.fileList);

        setFileMap(newFileMap);

        if (nameList && name) {
          const updatedFields = form.getFieldsValue([nameList]);

          if (maxCount === 1) {
            updatedFields[nameList][fieldKey][name] = info.fileList[0].response.url;
          } else {
            updatedFields[nameList][fieldKey][name] = info.fileList.map((item: UploadFile) => item.response.url);
          }

          form.setFieldsValue(updatedFields);
        } else if (name) {
          form.setFieldValue(
            name,
            maxCount === 1 ? info.fileList[0].response.url : info.fileList.map((item: UploadFile) => item.response.url),
          );
        }

        name && form.validateFields([name]);
      }

      message.success(`${info.file.name} file uploaded successfully.`);
    } else if (status === 'error') {
      message.error(`${info.file.name} file upload failed.`);
    }
  };

  return (
    <Dragger
      {...uploadProps}
      {...props}
      multiple={multiple}
      maxCount={maxCount}
      {...(typeof fieldKey === 'number' && {
        onChange: info => onChange(info, fieldKey),
        defaultFileList: fileMap?.get(fieldKey),
      })}
    >
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">Click or drag file to this area to upload</p>
    </Dragger>
  );
};

export default DraggerUpload;
