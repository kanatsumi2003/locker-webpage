import type { SelectProps } from 'antd';
import type { FC } from 'react';

import { Select } from 'antd';
import { useCallback, useEffect, useState } from 'react';

import { toLowerCaseNonAccentVietnamese } from '@/utils/formatSearch';

const { Option } = Select;

interface InfiniteSelectProps extends SelectProps {
  onLoad: (params?: any) => void;
  data: SelectOption[] | undefined;
  filter?: unknown;
}

interface SelectQuery {
  pageNumber: number;
  pageSize?: number;
  search?: string;
  filter?: unknown;
}
interface SelectOption {
  value?: any;
  key?: any;
  label?: any;
  children?: string;
}

const InfiniteSelect: FC<InfiniteSelectProps> = ({ onLoad, data, filter, ...rest }) => {
  const [loading, setLoading] = useState(false);
  const [selectQuery, setSelectQuery] = useState<SelectQuery>({
    pageNumber: 1,
    pageSize: 10,
    ...(typeof filter === 'object' && filter),
  });

  useEffect(() => {
    const queryTimeout = setTimeout(() => {
      setLoading(true);

      onLoad(selectQuery);

      setLoading(false);
    }, 300);

    return () => clearTimeout(queryTimeout);
  }, [selectQuery.pageNumber, selectQuery.search]);

  const onSearch = useCallback((value: string) => {
    setSelectQuery(prev => ({
      ...prev,
      pageNumber: 1,
      search: value,
      ...(typeof filter === 'object' && filter),
    }));
  }, []);

  const onScroll = async (event: any) => {
    const target = event.target;

    if (!loading && target.scrollTop + target.offsetHeight === target.scrollHeight) {
      setSelectQuery(prev => ({
        ...prev,
        page: prev.pageNumber + 1,
      }));
    }
  };

  return (
    <Select
      size="large"
      style={{ minWidth: '10rem' }}
      {...rest}
      loading={loading}
      onPopupScroll={onScroll}
      onSearch={onSearch}
      searchValue={selectQuery.search}
      filterOption={(input: string, option: any) => {
        return toLowerCaseNonAccentVietnamese(option?.children).indexOf(toLowerCaseNonAccentVietnamese(input)) >= 0;
      }}
    >
      {data?.map(el => (
        <Option key={el.value} value={el.value}>
          {el.label}
        </Option>
      ))}
      {/* {loading && <Option>Loading...</Option>} */}
    </Select>
  );
};

export default InfiniteSelect;
