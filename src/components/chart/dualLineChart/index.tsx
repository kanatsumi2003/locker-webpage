import type { Box } from '@nivo/core';
import type { Serie } from '@nivo/line';

import { formatDecimalPrecision } from '@/utils/decimalPrecision';

import { BaseLineChart } from '../lineChart';

interface DualLineChartData extends Serie {
  legend?: string;
}

interface Props {
  data: [DualLineChartData, DualLineChartData];
  color: [string, string];
  height?: number;
  margin?: Box;
  enableArea?: boolean;
}

export const DualLineChart = ({
  data,
  color,
  height = 400,
  margin = { top: 20, right: 120, bottom: 40, left: 40 },
  enableArea = false,
}: Props) => {
  return (
    <div style={{ height: height, minWidth: 500, position: 'relative' }}>
      <div style={{ position: 'absolute', top: 0, height: height, width: '100%' }}>
        <BaseLineChart
          enableArea={enableArea}
          data={[data[0]]}
          color={[color[0]]}
          axisRight={{
            tickSize: 0,
            tickPadding: 10,
            tickRotation: 0,
            legend: data[0].legend,
            legendOffset: 60,
            legendPosition: 'middle',
            format: '>-,',
          }}
          yScale={{ type: 'linear', max: 100, min: 0 }}
          margin={margin}
          disableLegend
        />
      </div>
      <div style={{ position: 'absolute', top: 0, height: height, width: '100%' }}>
        <BaseLineChart
          enableArea={enableArea}
          data={Array.of(data[0], data[1])}
          axisBottom={null}
          axisLeft={{
            tickSize: 0,
            tickPadding: 10,
            tickRotation: 0,
            legendOffset: -80,
            legend: data[1].legend,
            legendPosition: 'middle',
            format: '>-,',
          }}
          yScale={{ type: 'linear', min: 0 }}
          color={['rgba(255, 255, 255, 0)', color[1]]}
          disableGrid
          margin={margin}
          /* Add this for tooltip */
          enableSlices="x"
          useMesh={true}
          sliceTooltip={({ slice }) => {
            return (
              <div
                style={{
                  background: 'white',
                  padding: '9px 12px',
                  border: '1px solid #ccc',
                }}
              >
                <div>{slice.points[0].data.x}</div>
                {slice.points.map((point, index) => (
                  <div
                    key={point.id}
                    style={{
                      color: color[index],
                    }}
                  >
                    <strong>{point.serieId}</strong>: {formatDecimalPrecision(Number(point.data.y) ?? 0) ?? 0}
                  </div>
                ))}
              </div>
            );
          }}
          legendsData={[
            {
              id: data[0].id,
              label: data[0].id,
              color: color[0],
            },
            {
              id: data[1].id,
              label: data[1].id,
              color: color[1],
            },
          ]}
        />
      </div>
    </div>
  );
};
