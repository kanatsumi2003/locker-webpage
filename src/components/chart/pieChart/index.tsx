import type { Box } from '@nivo/core';
import type { LegendProps } from '@nivo/legends';

import { ResponsivePie } from '@nivo/pie';
import { useSelector } from 'react-redux';

interface IPieDataType {
  value: number;
  color: string;
  label: string;
}

interface Props {
  onRangeChange?: () => void;
  data?: IPieDataType[];
  disableLegend?: boolean;
  disableArcLinkLabels?: boolean;
  legendsDirection?: 'row' | 'column';
  legendsItemDirection?: 'left-to-right' | 'right-to-left' | 'top-to-bottom' | 'bottom-to-top';
  margin?: Box;
  legendsTranslateY?: number;
  legendsTranslateX?: number;
  legendsItemWidth?: number;
  legendsItemHeight?: number;
  legendsItemSpacing?: number;
  legendsBreakPoint?: number;
}

const BasePieChart = ({
  data = [],
  disableLegend,
  disableArcLinkLabels,
  legendsDirection = 'row',
  legendsItemDirection = 'top-to-bottom',
  legendsTranslateY = 56,
  legendsTranslateX = 0,
  legendsItemWidth = 84,
  legendsItemHeight = 28,
  legendsItemSpacing = 0,
  legendsBreakPoint = 4,
  margin = { top: 40, right: 80, bottom: 80, left: 80 },
}: Props) => {
  const { theme } = useSelector(state => state.global);

  const legendProps: LegendProps = {
    anchor: 'top',
    direction: legendsDirection,
    justify: true,
    itemsSpacing: legendsItemSpacing,
    itemWidth: legendsItemWidth,
    itemHeight: legendsItemHeight,
    itemDirection: legendsItemDirection,
    itemOpacity: 1,
    symbolSize: 12,
    symbolShape: 'circle',
    effects: [
      {
        on: 'hover',
        style: {
          itemTextColor: '#000',
        },
      },
    ],
  };

  return (
    <ResponsivePie
      data={data}
      margin={margin}
      innerRadius={0.5}
      padAngle={1}
      cornerRadius={3}
      activeOuterRadiusOffset={8}
      borderWidth={1}
      enableArcLinkLabels={!disableArcLinkLabels}
      arcLinkLabelsSkipAngle={10}
      theme={{ textColor: theme === 'light' ? '#999' : '#FFF', tooltip: { basic: { color: '#000' } } }}
      colors={item => {
        return item.data?.color;
      }}
      arcLinkLabelsThickness={2}
      arcLinkLabelsColor={{ from: 'color' }}
      arcLabelsSkipAngle={10}
      sortByValue={true}
      arcLabelsTextColor={theme === 'light' ? '#000' : '#000'}
      arcLinkLabelsTextColor={theme === 'light' ? '#999' : '#FFF'}
      borderColor={{ from: 'color' }}
      legends={
        data && data.length <= legendsBreakPoint
          ? [
              {
                ...legendProps,
                translateY: legendsTranslateY,
                translateX: legendsTranslateX,
              },
            ]
          : [
              {
                ...legendProps,
                translateY: legendsTranslateY,
                translateX:
                  legendsDirection === 'row'
                    ? legendsTranslateX
                    : legendsTranslateX - legendsItemWidth / 2 - legendsItemSpacing,
                data: data.slice(0, Math.floor(data.length / 2)).map((cur, index) => ({
                  id: cur.label,
                  label: cur.label,
                  color: cur.color,
                })),
              },
              {
                ...legendProps,
                translateY: legendsDirection === 'column' ? legendsTranslateY : legendsTranslateY + 50,
                translateX:
                  legendsDirection === 'row'
                    ? legendsTranslateX
                    : legendsTranslateX + legendsItemWidth / 2 + legendsItemSpacing,
                data: data.slice(Math.floor(data.length / 2)).map((cur, index) => ({
                  id: cur.label,
                  label: cur.label,
                  color: cur.color,
                })),
              },
            ]
      }
    />
  );
};

export default BasePieChart;
