import type { BarDatum } from '@nivo/bar';
import type { Box } from '@nivo/core';

import { ResponsiveBar } from '@nivo/bar';
import { useSelector } from 'react-redux';

interface Props {
  data?: (BarDatum & { color?: string })[];
  indexBy: string;
  keys: string[];
  xAxisTitle?: string;
  yAxisTitle?: string;
  disableGrid?: boolean;
  disableLegend?: boolean;
  margin?: Box;
  colorBy?: 'id' | 'indexValue';
  tickRotation?: number;
  gridYValues?: number;
  valueScale?: 'symlog' | 'linear';
  xAxisFormat?: (value: number) => string;
}

export const BaseBarChart = ({
  data = [],
  indexBy,
  keys,
  xAxisTitle,
  yAxisTitle,
  disableGrid,
  colorBy = 'id',
  xAxisFormat,
  disableLegend,
  tickRotation = 0,
  gridYValues = 5,
  valueScale = 'symlog',
  margin = { top: 50, right: 130, bottom: 50, left: 60 },
}: Props) => {
  const { theme } = useSelector(state => state.global);

  return (
    <ResponsiveBar
      data={data}
      keys={keys}
      indexBy={indexBy}
      padding={0.4}
      margin={margin}
      groupMode="grouped"
      valueScale={{ type: valueScale }}
      indexScale={{ type: 'band', round: true }}
      axisTop={null}
      axisBottom={{
        tickSize: 0,
        tickPadding: 10,
        tickRotation: tickRotation,
        legend: xAxisTitle,
        format: xAxisFormat,
        legendPosition: 'middle',
        legendOffset: 40,
      }}
      axisLeft={{
        format: value => `${value > 1000 ? value / 1000 + 'K' : value}`,
        tickSize: 0,
        tickPadding: 4,
        legendPosition: 'middle',
        legendOffset: 20,
        legend: yAxisTitle,
        tickValues: gridYValues,
      }}
      theme={{
        textColor: theme === 'light' ? '#999' : '#FFF',
        tooltip: { basic: { color: '#000' } },
      }}
      gridYValues={gridYValues}
      labelTextColor={theme === 'light' ? '#000' : '#000'}
      enableGridY={!disableGrid && data.length !== 0}
      labelSkipWidth={12}
      colorBy={colorBy}
      colors={colorBy === 'indexValue' ? ({ index }) => data[index].color ?? '' : { scheme: 'nivo' }}
      labelSkipHeight={12}
      legends={
        !disableLegend
          ? [
              {
                dataFrom: 'keys',
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 120,
                translateY: 0,
                itemsSpacing: 2,
                itemWidth: 100,
                itemHeight: 20,
                itemDirection: 'left-to-right',
                itemOpacity: 0.85,
                symbolSize: 20,
                effects: [
                  {
                    on: 'hover',
                    style: {
                      itemOpacity: 1,
                    },
                  },
                ],
              },
            ]
          : []
      }
      valueFormat=">-,"
    />
  );
};
