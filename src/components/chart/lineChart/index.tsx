import type { AxisProps } from '@nivo/axes';
import type { OrdinalColorScaleConfig } from '@nivo/colors';
import type { Box } from '@nivo/core';
import type { Datum as LegendsType } from '@nivo/legends';
import type { Serie, SliceTooltip } from '@nivo/line';
import type { ScaleSpec } from '@nivo/scales';

import { ResponsiveLine } from '@nivo/line';
import { useSelector } from 'react-redux';

interface Props {
  data?: Serie[];
  xAxisTitle?: string;
  yAxisTitle?: string;
  disableGrid?: boolean;
  disableLegend?: boolean;
  margin?: Box;
  tickRotation?: number;
  axisLeft?: AxisProps;
  axisBottom?: AxisProps | null;
  axisRight?: AxisProps;
  color?: OrdinalColorScaleConfig;
  legendTranslateY?: number;
  yScale?: ScaleSpec;
  useMesh?: boolean;
  enableSlices?: 'x' | 'y' | false;
  sliceTooltip?: SliceTooltip;
  legendsData?: LegendsType[];
  enableArea?: boolean;
}

export const BaseLineChart = ({
  data = [],
  xAxisTitle,
  yAxisTitle,
  disableGrid,
  disableLegend,
  axisRight,
  enableSlices,
  sliceTooltip,
  useMesh = true,
  legendTranslateY = 0,
  color,
  yScale = {
    type: 'linear',
    min: 'auto',
    max: 'auto',
    stacked: false,
    reverse: false,
  },
  axisBottom = {
    tickSize: 0,
    tickPadding: 10,
    tickRotation: 0,
    legend: xAxisTitle,
    legendOffset: 36,
    legendPosition: 'middle',
  },
  legendsData,
  axisLeft,
  margin = { top: 50, right: 130, bottom: 50, left: 60 },
  enableArea = false,
}: Props) => {
  const { theme } = useSelector(state => state.global);

  return (
    <ResponsiveLine
      enableArea={enableArea}
      data={data}
      margin={margin}
      xScale={{ type: 'point' }}
      yScale={yScale}
      colors={color}
      axisTop={null}
      axisRight={axisRight ?? null}
      axisBottom={axisBottom}
      theme={{
        textColor: theme === 'light' ? '#999' : '#FFF',
        tooltip: { basic: { color: '#000' } },
      }}
      enableGridX={!disableGrid}
      enableGridY={!disableGrid}
      axisLeft={axisLeft ?? null}
      pointSize={10}
      pointColor={{ theme: 'background' }}
      pointBorderWidth={2}
      pointBorderColor={{ from: 'serieColor' }}
      pointLabelYOffset={-12}
      useMesh={useMesh}
      sliceTooltip={sliceTooltip}
      enableSlices={enableSlices}
      legends={
        !disableLegend
          ? [
              {
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 120,
                translateY: legendTranslateY,
                itemsSpacing: 0,
                itemDirection: 'left-to-right',
                itemWidth: 80,
                itemHeight: 20,
                itemOpacity: 0.75,
                symbolSize: 12,
                symbolShape: 'circle',
                symbolBorderColor: 'rgba(0, 0, 0, .5)',
                data: legendsData,
                effects: [
                  {
                    on: 'hover',
                    style: {
                      itemOpacity: 1,
                    },
                  },
                ],
              },
            ]
          : []
      }
      yFormat=">-,"
    />
  );
};
