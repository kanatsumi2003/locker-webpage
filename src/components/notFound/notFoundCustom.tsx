import { Skeleton } from 'antd';

import NotFoundPage from '@/pages/404';

export const NotFoundCustom = ({ loading }: { loading?: boolean }) => {
  return (
    <Skeleton loading={loading} active>
      <NotFoundPage />
    </Skeleton>
  );
};
