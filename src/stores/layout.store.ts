import type { DrawerState } from '@/interface/layout/drawer.interface';
import type { PayloadAction } from '@reduxjs/toolkit';

import { createSlice } from '@reduxjs/toolkit';

const initialState: DrawerState = {
  title: '',
  visible: false,
  flag: '',
};

const drawerSlice = createSlice({
  name: 'drawer',
  initialState,
  reducers: {
    openDrawer(state, action: PayloadAction<Partial<DrawerState>>) {
      state.title = action.payload.title;
      state.flag = action.payload.flag;
      state.visible = true;
    },
    closeDrawer(state) {
      state.title = '';
      state.flag = '';
      state.visible = false;
    },
  },
});

export const { openDrawer, closeDrawer } = drawerSlice.actions;

export default drawerSlice.reducer;
