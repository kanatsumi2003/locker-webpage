import type { IRangePicker } from '@/interface/layout/index.interface';
import type { ILockerItem } from '@/interface/locker/locker';
import type { PayloadAction } from '@reduxjs/toolkit';

import { createSlice } from '@reduxjs/toolkit';

interface LockerState {
  dataLocker: ILockerItem | null;
  rangePicker?: IRangePicker;
}

const initialState: LockerState = {
  dataLocker: null,
  rangePicker: undefined,
};

const lockerSlice = createSlice({
  name: 'locker',
  initialState,
  reducers: {
    setDataLocker(state, action: PayloadAction<ILockerItem>) {
      state.dataLocker = action.payload;
    },
    setRangePicker(state, action: PayloadAction<IRangePicker>) {
      state.rangePicker = action.payload;
    },
  },
});

export const { setDataLocker, setRangePicker } = lockerSlice.actions;

export default lockerSlice.reducer;
