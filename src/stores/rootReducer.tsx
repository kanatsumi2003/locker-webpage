import { combineReducers } from '@reduxjs/toolkit';

import { addressApi } from '@/services/addressService';
import { authApi } from '@/services/authService';
import { boxApi } from '@/services/boxService';
import { dashboardApi } from '@/services/dashboardService';
import { hardwareApi } from '@/services/hardwareService';
import { lockerApi } from '@/services/lockerService';
import { orderApi } from '@/services/orderService';
import { serviceApi } from '@/services/serviceService';
import { staffApi } from '@/services/staffService';
import { storeApi } from '@/services/storeService';

import globalReducer from './global.store';
import drawerReducer from './layout.store';
import lockerReducer from './locker.store';
import tableReducer from './table.store';
import tagsViewReducer from './tags-view.store';
import userReducer from './user.store';

const rootReducer = combineReducers({
  user: userReducer,
  tagsView: tagsViewReducer,
  global: globalReducer,
  table: tableReducer,
  drawer: drawerReducer,
  locker: lockerReducer,
  [lockerApi.reducerPath]: lockerApi.reducer,
  [hardwareApi.reducerPath]: hardwareApi.reducer,
  [serviceApi.reducerPath]: serviceApi.reducer,
  [boxApi.reducerPath]: boxApi.reducer,
  [orderApi.reducerPath]: orderApi.reducer,
  [dashboardApi.reducerPath]: dashboardApi.reducer,
  [addressApi.reducerPath]: addressApi.reducer,
  [authApi.reducerPath]: authApi.reducer,
  [storeApi.reducerPath]: storeApi.reducer,
  [staffApi.reducerPath]: staffApi.reducer,
});

export default rootReducer;
