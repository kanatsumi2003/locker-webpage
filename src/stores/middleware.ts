import { addressApi } from '@/services/addressService';
import { authApi } from '@/services/authService';
import { boxApi } from '@/services/boxService';
import { dashboardApi } from '@/services/dashboardService';
import { hardwareApi } from '@/services/hardwareService';
import { lockerApi } from '@/services/lockerService';
import { orderApi } from '@/services/orderService';
import { serviceApi } from '@/services/serviceService';
import { staffApi } from '@/services/staffService';
import { storeApi } from '@/services/storeService';

export const middleware = [
  lockerApi.middleware,
  boxApi.middleware,
  serviceApi.middleware,
  hardwareApi.middleware,
  orderApi.middleware,
  dashboardApi.middleware,
  addressApi.middleware,
  authApi.middleware,
  storeApi.middleware,
  staffApi.middleware,
];
