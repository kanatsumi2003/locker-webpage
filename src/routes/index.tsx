import type { FC } from 'react';
import type { RouteObject } from 'react-router';

import { lazy } from 'react';
import { Navigate } from 'react-router';
import { useLocation, useNavigate, useRoutes } from 'react-router-dom';

import { PATH } from '@/constants/common';
import Dashboard from '@/pages/dashboard';
import LayoutPage from '@/pages/layout';
import LoginPage from '@/pages/login';
import { historyNavigation } from '@/utils/historyNavigation';

import WrapperRouteComponent from './config';

const NotFound = lazy(() => import(/* webpackChunkName: "404'"*/ '@/pages/404'));
const Documentation = lazy(() => import(/* webpackChunkName: "404'"*/ '@/pages/doucumentation'));
const Guide = lazy(() => import(/* webpackChunkName: "guide'"*/ '@/pages/guide'));
const RoutePermission = lazy(() => import(/* webpackChunkName: "route-permission"*/ '@/pages/permission/route'));
const FormPage = lazy(() => import(/* webpackChunkName: "form'"*/ '@/pages/components/form'));
const TablePage = lazy(() => import(/* webpackChunkName: "table'"*/ '@/pages/components/table'));
const SearchPage = lazy(() => import(/* webpackChunkName: "search'"*/ '@/pages/components/search'));
const TabsPage = lazy(() => import(/* webpackChunkName: "tabs'"*/ '@/pages/components/tabs'));
const AsidePage = lazy(() => import(/* webpackChunkName: "aside'"*/ '@/pages/components/aside'));
const RadioCardsPage = lazy(() => import(/* webpackChunkName: "radio-cards'"*/ '@/pages/components/radio-cards'));
const BusinessBasicPage = lazy(() => import(/* webpackChunkName: "basic-page" */ '@/pages/business/basic'));
const BusinessWithSearchPage = lazy(() => import(/* webpackChunkName: "with-search" */ '@/pages/business/with-search'));
const BusinessWithAsidePage = lazy(() => import(/* webpackChunkName: "with-aside" */ '@/pages/business/with-aside'));
const BusinessWithRadioCardsPage = lazy(
  () => import(/* webpackChunkName: "with-aside" */ '@/pages/business/with-radio-cards'),
);
const BusinessWithTabsPage = lazy(() => import(/* webpackChunkName: "with-tabs" */ '@/pages/business/with-tabs'));
const LockerPage = lazy(() => import('@/pages/locker'));
const LockerDetailPage = lazy(() => import('@/pages/lockerDetail'));
const OrderPage = lazy(() => import('@/pages/order'));
const OrderDetailPage = lazy(() => import('@/pages/orderDetail'));
const StorePage = lazy(() => import('@/pages/store'));
const StoreDetailPage = lazy(() => import('@/pages/storeDetail'));
const ServicePage = lazy(() => import('@/pages/service'));
const ServiceDetailPage = lazy(() => import('@/pages/serviceDetail'));
const StaffPage = lazy(() => import('@/pages/staff'));
const StaffDetailPage = lazy(() => import('@/pages/staffDetail'));

const routeList: RouteObject[] = [
  {
    path: PATH.LOGIN,
    element: <WrapperRouteComponent element={<LoginPage />} titleId="title.login" />,
  },
  {
    path: PATH.HOME,
    element: <WrapperRouteComponent element={<LayoutPage />} titleId="" />,
    children: [
      {
        path: '',
        element: <Navigate to="dashboard" />,
      },
      {
        path: PATH.DASHBOARD,
        element: <WrapperRouteComponent element={<Dashboard />} titleId="title.dashboard" auth />,
      },
      {
        path: 'dashboard/analysis',
        element: <WrapperRouteComponent element={<Dashboard />} titleId="title.dashboard" auth />,
      },
      {
        path: PATH.STORE,
        element: <WrapperRouteComponent element={<StorePage />} titleId="title.store" auth />,
      },
      {
        path: PATH.STORE_DETAIL,
        element: <WrapperRouteComponent element={<StoreDetailPage />} titleId="title.store.detail" auth />,
      },
      {
        path: PATH.STAFF,
        element: <WrapperRouteComponent element={<StaffPage />} titleId="title.staff" auth />,
      },
      {
        path: PATH.STAFF_DETAIL,
        element: <WrapperRouteComponent element={<StaffDetailPage />} titleId="title.staff.detail" auth />,
      },
      {
        path: PATH.LOCKER,
        element: <WrapperRouteComponent element={<LockerPage />} titleId="title.locker" auth />,
      },
      {
        path: PATH.LOCKER_DETAIL,
        element: <WrapperRouteComponent element={<LockerDetailPage />} titleId="title.locker.detail" auth />,
      },
      {
        path: PATH.SERVICE,
        element: <WrapperRouteComponent element={<ServicePage />} titleId="title.order" auth />,
      },
      {
        path: PATH.SERVICE_DETAIL,
        element: <WrapperRouteComponent element={<ServiceDetailPage />} titleId="title.service.detail" auth />,
      },
      {
        path: PATH.ORDER,
        element: <WrapperRouteComponent element={<OrderPage />} titleId="title.order" auth />,
      },
      {
        path: PATH.ORDER_DETAIL,
        element: <WrapperRouteComponent element={<OrderDetailPage />} titleId="title.order.detail" auth />,
      },
      {
        path: 'documentation',
        element: <WrapperRouteComponent element={<Documentation />} titleId="title.documentation" />,
      },
      {
        path: 'guide',
        element: <WrapperRouteComponent element={<Guide />} titleId="title.guide" />,
      },
      {
        path: 'permission/route',
        element: <WrapperRouteComponent element={<RoutePermission />} titleId="title.permission.route" auth />,
      },
      {
        path: 'component/form',
        element: <WrapperRouteComponent element={<FormPage />} titleId="title.account" />,
      },
      {
        path: 'component/table',
        element: <WrapperRouteComponent element={<TablePage />} titleId="title.account" />,
      },
      {
        path: 'component/search',
        element: <WrapperRouteComponent element={<SearchPage />} titleId="title.account" />,
      },
      {
        path: 'component/tabs',
        element: <WrapperRouteComponent element={<TabsPage />} titleId="title.account" />,
      },
      {
        path: 'component/aside',
        element: <WrapperRouteComponent element={<AsidePage />} titleId="title.account" />,
      },
      {
        path: 'component/radio-cards',
        element: <WrapperRouteComponent element={<RadioCardsPage />} titleId="title.account" />,
      },
      {
        path: 'business/basic',
        element: <WrapperRouteComponent element={<BusinessBasicPage />} titleId="title.account" />,
      },
      {
        path: 'business/with-search',
        element: <WrapperRouteComponent element={<BusinessWithSearchPage />} titleId="title.account" />,
      },
      {
        path: 'business/with-aside',
        element: <WrapperRouteComponent element={<BusinessWithAsidePage />} titleId="title.account" />,
      },
      {
        path: 'business/with-radio-cards',
        element: <WrapperRouteComponent element={<BusinessWithRadioCardsPage />} titleId="title.account" />,
      },
      {
        path: 'business/with-tabs',
        element: <WrapperRouteComponent element={<BusinessWithTabsPage />} titleId="title.account" />,
      },
      {
        path: '*',
        element: <WrapperRouteComponent element={<NotFound />} titleId="title.notFount" />,
      },
    ],
  },
];

const RenderRouter: FC = () => {
  const element = useRoutes(routeList);

  historyNavigation.navigate = useNavigate();
  historyNavigation.location = useLocation();

  return element;
};

export default RenderRouter;
