const color = {
  primary: '#4096ff',
  textPrimary: '#d9ecfe',
};

export const token = {
  color,
};
