import type { IRangePicker } from '@/interface/layout/index.interface';
import type { ORDER_STATUS, ORDER_TYPE } from '@/interface/order/order';
import type { FC } from 'react';

import { Card, Col, Row, Statistic } from 'antd';
import React from 'react';

import { BaseBarChart } from '@/components/chart/barChart';
import { BaseLineChart } from '@/components/chart/lineChart';
import BasePieChart from '@/components/chart/pieChart';
import { useDashboardOrdersQuery } from '@/services/dashboardService';
import { mapOrderStatusColor, mapOrderTypeColor } from '@/utils/colorAdapter';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import { formatCurrency } from '@/utils/format';

const StatisticItemLayout: FC<{ title: string; children: React.ReactNode; value: React.ReactNode }> = ({
  title,
  children,
  value,
}) => {
  return (
    <Card style={{ height: '100%' }} bodyStyle={{ padding: 16 }}>
      <Statistic title={title} valueRender={() => <>{value}</>}></Statistic>
      {children}
    </Card>
  );
};

const StatisticBarChart: FC<{
  title: string;
  metricKey: string;
  metric: string;
  data?: { type: ORDER_TYPE; metric: number }[];
}> = ({ title, metricKey, metric, data = [] }) => {
  return (
    <StatisticItemLayout
      title={title}
      value={
        <div
          style={{
            fontSize: '25px',
            fontWeight: 500,
          }}
        >
          {metric}
        </div>
      }
    >
      <div>
        <div style={{ height: '120px', marginTop: 24 }}>
          <BaseBarChart
            disableLegend
            margin={{ bottom: 25, left: 50, top: 5 }}
            data={data?.map(d => ({
              [metricKey]: d.metric,
              type: `${d.type}`,
              color: mapOrderTypeColor(d.type),
            }))}
            valueScale="linear"
            keys={[metricKey]}
            indexBy="type"
            colorBy="indexValue"
            gridYValues={3}
          />
        </div>
      </div>
    </StatisticItemLayout>
  );
};

const StatistPieChart = <T extends ORDER_STATUS | ORDER_TYPE>({
  title,
  data = [],
  colorMapper,
}: {
  title: string;
  data?: { type: T; metric: number }[];
  colorMapper: (value: T) => string;
}) => {
  return (
    <StatisticItemLayout title={title} value={<></>}>
      <div style={{ height: '252px', width: '100%', marginTop: 20 }}>
        <BasePieChart
          legendsTranslateY={160}
          legendsTranslateX={0}
          disableArcLinkLabels
          legendsDirection={'column'}
          margin={{ bottom: 118, top: 10 }}
          legendsItemDirection={'left-to-right'}
          data={data?.map(d => ({
            id: d.type,
            label: `${d.type}`,
            value: d.metric,
            color: colorMapper(d.type),
          }))}
          legendsItemWidth={72}
          legendsItemSpacing={8}
          legendsItemHeight={14}
          legendsBreakPoint={4}
        />
      </div>
    </StatisticItemLayout>
  );
};

const DashboardStatistic: FC<{ rangePicker?: IRangePicker; title: string; storeId?: number; lockerId?: number }> = ({
  title,
  storeId,
  lockerId,
  rangePicker,
}) => {
  const { data, isLoading } = useDashboardOrdersQuery({ ...rangePicker, storeId, lockerId });

  return (
    <Card className="stores" title={title} loading={isLoading || !data} style={{ height: '100%' }}>
      <Row gutter={[12, 12]}>
        <Col md={{ span: 12 }} span={24}>
          <StatisticBarChart
            title="Đơn hàng thành công"
            metricKey="Đơn hàng"
            metric={`${data?.overview.completed} đơn hàng`}
            data={data?.overview.orderTypes.map(d => ({
              metric: d.count,
              type: d.type,
            }))}
          />
        </Col>
        <Col md={{ span: 12 }} span={24}>
          <StatisticBarChart
            title="Doanh thu"
            metricKey="Doanh thu"
            metric={(formatDecimalPrecision(data?.overview.revenue ?? 0) ?? 0) + ' đ'}
            data={data?.overview.orderTypes.map(d => ({
              metric: d.revenue,
              type: d.type,
            }))}
          />
        </Col>
        <Col md={{ span: 12 }} span={24}>
          <StatistPieChart<ORDER_TYPE>
            title="Theo loại"
            data={data?.orderTypes.map(d => ({
              metric: d.count,
              type: d.type,
            }))}
            colorMapper={value => mapOrderTypeColor(value)}
          />
        </Col>
        <Col md={{ span: 12 }} span={24}>
          <StatistPieChart<ORDER_STATUS>
            title="Theo trạng thái"
            data={data?.orderStatuses.map(d => ({
              metric: d.count,
              type: d.status,
            }))}
            colorMapper={value => mapOrderStatusColor(value)}
          />
        </Col>
      </Row>
    </Card>
  );
};

export default DashboardStatistic;
