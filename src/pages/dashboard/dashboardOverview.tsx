import type { IRangePicker } from '@/interface/layout/index.interface';

import { DatabaseOutlined, ShopOutlined, TagOutlined, UserOutlined, UserSwitchOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';

import StatisticItem from '@/components/statistic/statisticItem';
import { useDashboardOverviewQuery } from '@/services/dashboardService';

const DashboardOverview = ({ rangePicker }: { rangePicker?: IRangePicker }) => {
  const { data } = useDashboardOverviewQuery(rangePicker);

  return (
    <Row gutter={[12, 12]}>
      <Col flex={'1 0 20%'}>
        <StatisticItem title={'Cửa hàng'} value={data?.storeCount} icon={<ShopOutlined />} color="#73d13d" />
      </Col>
      <Col flex={'1 0 20%'}>
        <StatisticItem title={'Locker'} value={data?.lockerCount} icon={<DatabaseOutlined />} color="#ff4d4f" />
      </Col>
      <Col flex={'1 0 20%'}>
        <StatisticItem title={'Nhân viên'} value={data?.staffCount} icon={<UserSwitchOutlined />} color="#597ef7" />
      </Col>
      <Col flex={'1 0 20%'}>
        <StatisticItem title={'Khách hàng'} value={data?.customerCount} icon={<UserOutlined />} color="#36cfc9" />
      </Col>
      <Col flex={'1 0 20%'}>
        <StatisticItem title={'Dịch vụ'} value={data?.serviceCount} icon={<TagOutlined />} color="#ffc53d" />
      </Col>
    </Row>
  );
};

export default DashboardOverview;
