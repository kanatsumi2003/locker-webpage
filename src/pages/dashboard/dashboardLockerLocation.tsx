import type { LOCKER_STATUS } from '@/interface/locker/locker';
import type { IDashboardParams } from '@/interface/order/order';
import type { FC } from 'react';

import './index.less';

import { AppstoreOutlined, ArrowsAltOutlined, TableOutlined } from '@ant-design/icons';
import { Button, Card, Col, Empty, Image, Modal, Row, Space, Tag, Typography } from 'antd';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import CustomMap from '@/components/map/map';
import { PATH } from '@/constants/common';
import { useDashboardLockerLocationsQuery } from '@/services/dashboardService';
import { mapLockerStatusColor } from '@/utils/colorAdapter';

import LocationFilter from '../components/locationFilter';

const { Title } = Typography;

const generateLockerStatusIcons = (status: LOCKER_STATUS) => {
  return {
    backgroundColor: mapLockerStatusColor(status),
  };
};

interface PopupMarkerProps {
  id: number;
  name: string;
  locationString: string;
  status: LOCKER_STATUS;
  image: string;
}

const PopupMarker = ({ id, name, status, locationString, image }: PopupMarkerProps) => {
  return (
    <>
      <Row gutter={8}>
        <Col span={6}>
          <Image src={image} style={{ objectFit: 'cover' }} />
        </Col>
        <Col span={18}>
          <div style={{ marginBottom: '5px' }}>
            <Link to={`${PATH.LOCKER}/${id}`} style={{ display: 'block', marginBottom: '5px' }}>
              <span style={{ fontWeight: '600', marginRight: '4px' }}>{name}</span>
            </Link>{' '}
            <Tag color={mapLockerStatusColor(status)}>{status}</Tag>
          </div>
          {locationString}
        </Col>
      </Row>
    </>
  );
};

const LockerLocation: FC<{ storeId?: number }> = ({ storeId }) => {
  const [params, setParams] = useState<IDashboardParams>({});
  const { data, isLoading } = useDashboardLockerLocationsQuery({ ...params, storeId });

  return (
    <>
      <LocationFilter setParams={setParams} size="middle" />
      {data && data.length > 0 ? (
        <CustomMap
          center={
            data && data.length > 0
              ? {
                  location: [data?.at(0)?.location.longitude ?? 0, data?.at(0)?.location.latitude ?? 0],
                }
              : undefined
          }
          scrollWheelZoom
          markers={data
            ?.filter(d => d.location.latitude && d.location.latitude)
            .map(d => ({
              location: [d.location.longitude ?? 0, d.location.latitude ?? 0],
              popup: (
                <PopupMarker
                  key={d.id}
                  id={d.id}
                  name={d.name}
                  status={d.status}
                  image={d.image}
                  locationString={`${d.location.address}, ${d.location.ward.name}, ${d.location.district.name}, ${d.location.province.name}`}
                />
              ),
              icon: (
                <span style={generateLockerStatusIcons(d.status)} className="markerLocker">
                  <AppstoreOutlined className="markerLockerIcon" />
                </span>
              ),
            }))}
        />
      ) : (
        <Empty style={{ marginTop: 50 }} />
      )}
    </>
  );
};

const DashboardLockerLocation: FC<{ title: string; storeId?: number }> = ({ title, storeId }) => {
  const [fullScreen, setFullScreen] = useState(false);

  return (
    <Card
      title={title}
      // loading={isLoading || !data}
      style={{ height: '100%', overflowX: 'hidden' }}
      extra={<Button icon={<ArrowsAltOutlined />} onClick={() => setFullScreen(true)}></Button>}
    >
      <LockerLocation storeId={storeId} />
      <Modal
        mask
        centered
        open={fullScreen}
        onOk={() => setFullScreen(false)}
        onCancel={() => setFullScreen(false)}
        destroyOnClose={true}
        width={1200}
        footer={null}
      >
        <LockerLocation storeId={storeId} />
      </Modal>
    </Card>
  );
};

export default DashboardLockerLocation;
