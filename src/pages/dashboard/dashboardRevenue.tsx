import type { FC } from 'react';

import './index.less';

import { Card, DatePicker } from 'antd';
import dayjs from 'dayjs';
import { useState } from 'react';

import { DualLineChart } from '@/components/chart/dualLineChart';
import { useDashboardRevenueQuery } from '@/services/dashboardService';

const DashboardRevenue: FC<{ title: string; storeId?: number }> = ({ title, storeId }) => {
  const [date, setDate] = useState<dayjs.Dayjs | undefined>(dayjs());
  const { data, isLoading } = useDashboardRevenueQuery({ storeId, year: date?.year() });

  return (
    <Card
      title={title}
      style={{ height: '100%' }}
      loading={isLoading || !data}
      bodyStyle={{ overflowX: 'auto', paddingTop: 20 }}
      extra={
        <DatePicker
          disabledDate={date => date.isAfter(dayjs()) || date.isBefore(dayjs().add(-5, 'year'))}
          defaultValue={date}
          onChange={date => setDate(date ?? undefined)}
          picker="year"
        />
      }
    >
      <DualLineChart
        enableArea={true}
        height={420}
        color={['#f87171', '#91caff']}
        data={[
          {
            id: 'Đơn hàng',
            legend: 'Đơn hàng',
            data:
              data?.map(d => ({
                x: `T${d.month}`,
                y: d.orderCount,
              })) ?? [],
          },
          {
            id: 'Doanh thu',
            legend: 'VND',
            data:
              data?.map(d => ({
                x: `T${d.month}`,
                y: d.revenue,
              })) ?? [],
          },
        ]}
        margin={{ top: 20, right: 120, bottom: 40, left: 100 }}
      />
    </Card>
  );
};

export default DashboardRevenue;
