import type { FC } from 'react';

import './index.less';

import { Badge, Select } from 'antd';
import { useState } from 'react';

const { Option } = Select;

const data = new Array(12).fill(null).map((_, index) => ({
  name: `${index + 1}`,
  totalOrders: Math.floor(Math.random() * 100 + 1),
  totalRevenue: Math.floor(Math.random() * 1000 + 1),
}));

const CustomTooltip: FC<any> = ({ active, payload, label }) => {
  if (active && payload) {
    const value1 = payload[0]?.value;
    const value2 = payload[1]?.value;

    return (
      <div className="customTooltip">
        <span className="customTooltip-title">{label}</span>
        <ul className="customTooltip-content">
          <li key="orders">
            <Badge color="#3F90F7" />
            <span className="customTooltip-label">Orders: </span> &nbsp; {value1}
          </li>
          <li key="revenue">
            <Badge color="#61BE82" />
            <span className="customTooltip-label">Revenue: </span> &nbsp; {value2}
          </li>
        </ul>
      </div>
    );
  }

  return null;
};

const TimeLine: FC<{ loading: boolean }> = ({ loading }) => {
  const [year, setYear] = useState<number>(2023);

  const handleYearChange = (value: number) => {
    setYear(value);
  };

  return <div>{/* <BarChartDashboard chartTitle="MONTHLY STATISTIC" /> */}</div>;
};

export default TimeLine;
