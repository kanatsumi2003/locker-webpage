import type { IDashboardStoreItem } from '@/interface/dashboard/dashboard';
import type { IRangePicker } from '@/interface/layout/index.interface';
import type { IDashboardStoreParams } from '@/interface/order/order';
import type { STORE_STATUS } from '@/interface/store/store';
import type { ColumnsType, TablePaginationConfig, TableProps } from 'antd/es/table';
import type { FilterValue } from 'antd/es/table/interface';
import type { FC } from 'react';

import { Button, Card, Image } from 'antd';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import TableCustom from '@/components/core/table';
import CustomSearch from '@/components/input/customSearch';
import CustomPagination from '@/components/pagination/customPagination';
import { PATH } from '@/constants/common';
import { useDashboardStoresQuery } from '@/services/dashboardService';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import { dropdownRenderStoreStatus } from '@/utils/dropdownRender';

import FilterStoreDashboard from './filterStoreDashboard';

const DashboardStores: FC<{ rangePicker?: IRangePicker; title: string }> = ({ title, rangePicker }) => {
  const [params, setParams] = useState<IDashboardStoreParams>({});
  const { data, isLoading } = useDashboardStoresQuery({ ...rangePicker, ...params });
  const navigate = useNavigate();

  const columns: ColumnsType<IDashboardStoreItem> = [
    {
      title: 'Hình ảnh',
      dataIndex: 'image',
      key: 'image',
      render: value => <Image src={value} style={{ width: 150 }} />,
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
      sorter: true,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status: STORE_STATUS) => dropdownRenderStoreStatus(status),
    },
    {
      title: 'SL Nhân viên',
      dataIndex: 'staffCount',
      key: 'staffCount',
      align: 'right',
      sorter: true,
    },

    {
      title: 'SL Locker',
      dataIndex: 'lockerCount',
      key: 'lockerCount',
      align: 'right',
      sorter: true,
    },
    {
      title: 'SL Đơn hàng',
      dataIndex: 'orderCount',
      key: 'orderCount',
      align: 'right',
      sorter: true,
    },
    {
      title: 'Doanh thu (đ)',
      dataIndex: 'revenue',
      key: 'revenue',
      align: 'right',
      sorter: true,
      render: (value, record, index) => <>{formatDecimalPrecision(value) ?? 0}</>,
    },
    {
      title: 'Hành động',
      key: 'action',
      align: 'center',
      fixed: 'right',
      render: (_: unknown, record: IDashboardStoreItem) => (
        <Button onClick={() => navigate(`${PATH.STORE}/${record?.id}`)}>Details</Button>
      ),
    },
  ];

  const handleTableChange = (
    pagination: TablePaginationConfig,
    filters: Record<string, FilterValue | null>,
    sorter: any,
  ) => {
    console.log(sorter);
    setParams({
      ...params,
      sortDir: sorter?.order ? (sorter.order === 'ascend' ? 'Asc' : 'Desc') : undefined,
      sortColumn: sorter?.order ? sorter.columnKey?.toString() : undefined,
    });
  };

  return (
    <Card className="stores" title={title} loading={isLoading || !data} bodyStyle={{ minHeight: 440 }}>
      <FilterStoreDashboard setParams={setParams} />
      <TableCustom
        onChange={handleTableChange}
        bordered
        loading={isLoading}
        rowKey={(record: IDashboardStoreItem) => record.id}
        dataSource={data?.items}
        columns={columns}
        scroll={{ x: 660, y: '600px' }}
      />
      <CustomPagination<IDashboardStoreParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setParams}
      />
    </Card>
  );
};

export default DashboardStores;
