import type { FC } from 'react';

import { Card } from 'antd';

import BasePieChart from '@/components/chart/pieChart';
import { useDashboardLockersQuery } from '@/services/dashboardService';
import { mapLockerStatusColor } from '@/utils/colorAdapter';

const DashboardLockerStatus: FC<{ title: string; storeId?: number }> = ({ title, storeId }) => {
  const { data, isLoading } = useDashboardLockersQuery({ storeId });

  return (
    <Card className="stores" title={title} loading={isLoading || !data} style={{ height: '100%' }}>
      <div style={{ height: 480 }}>
        <BasePieChart
          legendsTranslateY={320}
          margin={{ top: 40, right: 80, bottom: 180, left: 80 }}
          data={data?.map(d => ({
            id: d.status,
            label: d.status,
            value: d.count,
            color: mapLockerStatusColor(d.status),
          }))}
        />
      </div>
    </Card>
  );
};

export default DashboardLockerStatus;
