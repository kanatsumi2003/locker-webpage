import type { IRangePicker } from '@/interface/layout/index.interface';
import type { FC } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Form, Row } from 'antd';
import { useState } from 'react';

import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';

import DateRangePicker from '../components/dateRangePicker';
import DashboardStoreLocation from './dashboardLockerLocation';
import DashboardLockerStatus from './dashboardLockers';
import DashboardOverview from './dashboardOverview';
import DashboardRevenue from './dashboardRevenue';
import DashboardStatistic from './dashboardStatistic';
import DashboardStores from './dashboardStores';

const DashBoardPage: FC = () => {
  const [rangePicker, setRangePicker] = useState<IRangePicker>({});

  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setRangePicker({
      from: undefined,
      to: undefined,
    });
  };

  return (
    <div>
      <Divider orientation="left" orientationMargin={0}>
        TỔNG QUAN
      </Divider>

      <Form form={form} className="filter-group" layout="vertical">
        <ScrollWrapper>
          <Form.Item name={['from', 'to']}>
            <DateRangePicker setParams={setRangePicker} />
          </Form.Item>

          <Form.Item>
            <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large">
              Reset
            </Button>
          </Form.Item>
        </ScrollWrapper>
      </Form>

      <Row gutter={[12, 12]} style={{ marginTop: 12 }}>
        <Col span={24}>
          <DashboardOverview rangePicker={rangePicker} />
        </Col>
        <Col xl={{ span: 10 }} span={24}>
          <DashboardStatistic rangePicker={rangePicker} title={'Thống kê đơn hàng'} />
        </Col>
        <Col xl={{ span: 14 }} span={24}>
          <DashboardRevenue title={'Báo cáo doanh thu'} />
        </Col>
      </Row>

      <Divider style={{ marginTop: '30px' }} orientation="left" orientationMargin={0}>
        THỐNG KÊ CỬA HÀNG
      </Divider>
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <DashboardStores title={'Thống kê cửa hàng'} />
        </Col>
      </Row>

      <Divider style={{ marginTop: '30px' }} orientation="left" orientationMargin={0}>
        THỐNG KÊ LOCKER
      </Divider>
      <Row gutter={[12, 12]}>
        <Col xl={{ span: 10 }} span={24}>
          <DashboardLockerStatus title={'Trạng thái locker'} />
        </Col>
        <Col xl={{ span: 14 }} span={24}>
          <DashboardStoreLocation title={'Vị trí Locker'} />
        </Col>
      </Row>
    </div>
  );
};

export default DashBoardPage;
