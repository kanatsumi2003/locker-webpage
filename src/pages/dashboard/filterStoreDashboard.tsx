import type { IDashboardStoreParams } from '@/interface/order/order';
import type { Dispatch, FC, SetStateAction } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form, Select } from 'antd';

import CustomSearch from '@/components/input/customSearch';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';

import DateRangePicker from '../components/dateRangePicker';

interface Props {
  setParams: Dispatch<SetStateAction<IDashboardStoreParams>>;
}

const FilterStoreDashboard: React.FC<Props> = ({ setParams }) => {
  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setParams({ pageNumber: 1, pageSize: 10 });
  };

  const onValuesChange = (changedValues: IDashboardStoreParams) => {
    setParams((prev: IDashboardStoreParams) => ({ ...prev, ...changedValues }));
  };

  return (
    <Form form={form} name="filter-order" className="filter-group" layout="vertical" onValuesChange={onValuesChange}>
      {/* SEARCH ORDER */}
      <Form.Item name="query" label="Tìm kiếm">
        <CustomSearch<IDashboardStoreParams> placeholder="Search..." query="search" setParams={setParams} />
      </Form.Item>
      <ScrollWrapper>
        <Form.Item name={['from', 'to']} label="Thời gian">
          <DateRangePicker setParams={setParams} />
        </Form.Item>

        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large" className="reset-btn">
          Reset
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default FilterStoreDashboard;
