import type { IOrderParams } from '@/interface/order/order';
import type { Dispatch, SetStateAction } from 'react';

import '../../../styles/index.less';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form, Select } from 'antd';
import React from 'react';

import CustomSearch from '@/components/input/customSearch';
import CustomSelect from '@/components/input/customSelect';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';
import { ORDER_STATUS, ORDER_TYPE } from '@/interface/order/order';
import DateRangePicker from '@/pages/components/dateRangePicker';
import StaffSelect from '@/pages/staff/staffSelect';
import StoreSelect from '@/pages/store/storeSelect';
import { dropdownRenderOrderStatus, dropdownRenderOrderType } from '@/utils/dropdownRender';

import LockerSelect from '../lockerSelect';

const { Option } = Select;

interface Props {
  storeId?: number;
  staffId?: number;
  lockerId?: number;
  setParams: Dispatch<SetStateAction<IOrderParams>>;
}

const FilterOrder = ({ storeId, setParams, staffId, lockerId }: Props) => {
  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setParams({ pageNumber: 1, pageSize: 10 });
  };

  const onValuesChange = (changedValues: IOrderParams) => {
    setParams((prev: IOrderParams) => ({ ...prev, ...changedValues }));
  };

  return (
    <Form form={form} name="filter-order" className="filter-group" layout="vertical" onValuesChange={onValuesChange}>
      {/* SEARCH ORDER */}
      <Form.Item label="Search" name="query">
        <CustomSearch<IOrderParams> placeholder="Search..." query="query" setParams={setParams} />
      </Form.Item>

      <ScrollWrapper>
        {/* FILTER ORDER BY STORE */}
        {!storeId && (
          <Form.Item label="Store" name="storeId">
            <StoreSelect
              showSearch
              allowClear
              placeholder="Store"
              onChange={value => setParams((prev: IOrderParams) => ({ ...prev, ...{ storeId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER LOCKER BY STAFF */}
        {!staffId && (
          <Form.Item label="Staff" name="staffId">
            <StaffSelect
              showSearch
              allowClear
              placeholder="Staff"
              onChange={value => setParams((prev: IOrderParams) => ({ ...prev, ...{ staffId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER ORDER BY LOCKER */}
        {!lockerId && (
          <Form.Item label="Locker" name="lockerId">
            <LockerSelect
              showSearch
              allowClear
              placeholder="Locker"
              onChange={value => setParams((prev: IOrderParams) => ({ ...prev, ...{ lockerId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER ORDER BY TYPE */}
        <Form.Item label="Type" name="type">
          <CustomSelect<IOrderParams> setParams={setParams} name="type" placeholder="Type">
            {Object.values(ORDER_TYPE).map((type: ORDER_TYPE) => (
              <Option value={type} key={type}>
                {dropdownRenderOrderType(type)}
              </Option>
            ))}
          </CustomSelect>
        </Form.Item>

        {/* FILTER ORDER BY STATUS */}
        <Form.Item label="Status" name="status">
          <CustomSelect<IOrderParams> setParams={setParams} name="status" placeholder="Status">
            {Object.values(ORDER_STATUS).map((status: ORDER_STATUS) => (
              <Option value={status} key={status}>
                {dropdownRenderOrderStatus(status)}
              </Option>
            ))}
          </CustomSelect>
        </Form.Item>

        {/* FILTER ORDER BY CREATED AT */}
        <Form.Item label="Created Date" name={['from', 'to']}>
          <DateRangePicker setParams={setParams} />
        </Form.Item>

        {/* BUTTON RESET FILTER */}
        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large" className="reset-btn">
          Reset Filters
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default FilterOrder;
