import type { IProfile } from '@/interface/auth/profile';
import type { IBoxItem } from '@/interface/box/box';
import type { ILockerItem } from '@/interface/locker/locker';
import type { IOrderItem, IOrderParams, ORDER_STATUS, ORDER_TYPE } from '@/interface/order/order';
import type { IStaffItem } from '@/interface/staff/staff';
import type { ColumnsType, TableProps } from 'antd/es/table';

import { Avatar, Button, Space, Tooltip, Typography } from 'antd';
import dayjs from 'dayjs';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useNavigate, useParams } from 'react-router-dom';

import PersonPlaceholder from '@/assets/placeholder/person-placeholder.png';
import TableCustom from '@/components/core/table';
import CustomPagination from '@/components/pagination/customPagination';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { useOrdersQuery } from '@/services/orderService';
import { openDrawer } from '@/stores/layout.store';
import { setSelectedRowKeys } from '@/stores/table.store';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import { dropdownRenderOrderStatus, dropdownRenderOrderType } from '@/utils/dropdownRender';

import FilterOrder from './filterOrder';

const { Paragraph } = Typography;

interface Props {
  storeId?: number;
  staffId?: number;
  lockerId?: number;
  serviceId?: number;
}

const TableOrder: React.FC<Props> = ({ storeId, staffId, lockerId, serviceId }) => {
  const { id } = useParams();

  const dispatch = useDispatch();
  const navigate = useNavigate();

  // STATE
  const [orderParams, setOrderParams] = useState<IOrderParams>({
    pageNumber: 1,
    pageSize: 20,
    storeId,
    staffId,
    lockerId,
    serviceId,
  });

  // QUERY
  const { data, isLoading } = useOrdersQuery({
    ...orderParams,
  });

  const onChange: TableProps<IOrderItem>['onChange'] = ({ sorter }: any) => {
    setOrderParams({
      ...orderParams,
      sortColumn: sorter?.columnKey,
      sortDir: sorter?.order === 'ascend' ? 'Asc' : 'Desc',
    });
  };

  const handleViewDetail = (record: IOrderItem) => {
    dispatch(openDrawer({ title: 'Order Details', flag: 'details' }));
    dispatch(setSelectedRowKeys([record?.id]));
  };

  const tableColumns: ColumnsType<IOrderItem> = [
    {
      title: 'Store',
      dataIndex: 'locker',
      key: 'store',
      ellipsis: true,
      render: (locker: ILockerItem) => (
        <Tooltip placement="bottom" title={locker?.store?.name}>
          <Link to={`/stores/${locker?.store?.id}`}>{locker?.store?.name}</Link>
        </Tooltip>
      ),
    },
    {
      title: 'Locker',
      dataIndex: 'locker',
      key: 'locker',
      ellipsis: true,
      render: (locker: ILockerItem) => (
        <Tooltip placement="bottom" title={locker?.name}>
          <Link to={`/lockers/${locker?.id}`}>{locker?.name}</Link>
        </Tooltip>
      ),
    },
    {
      title: 'Pin Code',
      dataIndex: 'pinCode',
      key: 'pinCode',
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
      align: 'center',
      width: 120,
      render: (type: ORDER_TYPE) => dropdownRenderOrderType(type),
    },
    {
      title: 'Customer',
      key: 'customer',
      children: [
        {
          title: 'Order Phone',
          dataIndex: 'sender',
          key: 'orderPhone',
          align: 'center',
          width: 150,
          render: (sender: IProfile) =>
            sender?.phoneNumber && (
              <Tooltip
                placement="bottom"
                title={
                  <Space direction="vertical">
                    <Space>
                      <Avatar src={sender?.avatar || PersonPlaceholder} alt={sender?.avatar} />
                      Name: {sender?.fullName}
                    </Space>
                    {/* <Pho */}
                  </Space>
                }
              >
                {sender?.phoneNumber && <Paragraph copyable>{sender?.phoneNumber}</Paragraph>}
              </Tooltip>
            ),
        },
        {
          title: 'Receive Phone',
          dataIndex: 'receiver',
          key: 'receivePhone',
          align: 'center',
          width: 150,
          render: (receiver: IProfile) => (
            <Tooltip
              placement="bottom"
              title={
                <Space direction="vertical">
                  <Space>
                    <Avatar src={receiver?.avatar || PersonPlaceholder} alt={receiver?.avatar} />
                    Name: {receiver?.fullName}
                  </Space>
                  {/* <Pho */}
                </Space>
              }
            >
              {receiver?.phoneNumber && <Paragraph copyable>{receiver?.phoneNumber}</Paragraph>}
            </Tooltip>
          ),
        },
      ],
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      width: 120,
      render: (status: ORDER_STATUS) => dropdownRenderOrderStatus(status),
    },
    {
      title: 'Box',
      key: 'box',
      children: [
        {
          title: 'Send',
          dataIndex: 'sendBox',
          key: 'sendBox',
          align: 'right',
          render: (sendBox: IBoxItem) => sendBox?.number,
        },
        {
          title: 'Receive',
          dataIndex: 'receiveBox',
          key: 'receiveBox',
          align: 'right',
          render: (receiveBox: IBoxItem) => receiveBox?.number,
        },
      ],
    },
    {
      title: 'Staff',
      dataIndex: 'staff',
      key: 'staff',
      ellipsis: true,
      align: 'center',
      width: 120,
      render: (staff: IStaffItem) => (
        <Tooltip
          placement="bottom"
          title={
            <Space direction="vertical">
              <Space>
                <Avatar src={staff?.avatar || PersonPlaceholder} alt={staff?.avatar} />
                Name: {staff?.fullName}
              </Space>
            </Space>
          }
        >
          {staff?.phoneNumber && (
            <Paragraph copyable>
              <Link to={`/staffs/${staff?.id}`}>{staff?.phoneNumber}</Link>
            </Paragraph>
          )}
        </Tooltip>
      ),
    },
    {
      title: 'Fee',
      key: 'fee',
      children: [
        {
          title: 'Price',
          key: 'price',
          dataIndex: 'price',
          align: 'right',
          sorter: {
            compare: () => {
              setOrderParams({
                ...orderParams,
                sortColumn: 'price',
                sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
              });

              return 1;
            },
          },
          render: (price: number) => price && formatDecimalPrecision(price),
        },
        {
          title: 'Extra Count',
          key: 'extraCount',
          dataIndex: 'extraCount',
          align: 'right',
          sorter: {
            compare: () => {
              setOrderParams({
                ...orderParams,
                sortColumn: 'extraCount',
                sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
              });

              return 1;
            },
          },
          render: (extraCount: number) => extraCount && formatDecimalPrecision(extraCount),
        },
        {
          title: 'Extra Fee',
          key: 'extraFee',
          dataIndex: 'extraFee',
          align: 'right',
          sorter: {
            compare: () => {
              setOrderParams({
                ...orderParams,
                sortColumn: 'extraFee',
                sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
              });

              return 1;
            },
          },
          render: (extraFee: number) => extraFee && formatDecimalPrecision(extraFee),
        },
        {
          title: 'Discount',
          key: 'discount',
          dataIndex: 'discount',
          align: 'right',
          sorter: {
            compare: () => {
              setOrderParams({
                ...orderParams,
                sortColumn: 'discount',
                sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
              });

              return 1;
            },
          },
          render: (discount: number) => discount && formatDecimalPrecision(discount),
        },
        {
          title: 'Total',
          key: 'totalPrice',
          dataIndex: 'totalPrice',
          sorter: {
            compare: () => {
              setOrderParams({
                ...orderParams,
                sortColumn: 'totalPrice',
                sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
              });

              return 1;
            },
          },
          render: (totalPrice: number) => totalPrice && formatDecimalPrecision(totalPrice),
        },
      ],
    },
    {
      title: 'Created At',
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      sorter: {
        compare: () => {
          setOrderParams({
            ...orderParams,
            sortColumn: 'createdAt',
            sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
          });

          return 1;
        },
      },
      render: (createdAt: string) => dayjs(createdAt).format(FULL_TIME_FORMAT),
    },
    {
      title: 'Updated At',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      align: 'center',
      sorter: {
        compare: () => {
          setOrderParams({
            ...orderParams,
            sortColumn: 'updatedAt',
            sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
          });

          return 1;
        },
      },
      render: (createdAt: string) => dayjs(createdAt).format(FULL_TIME_FORMAT),
    },

    {
      title: 'Receive At',
      dataIndex: 'receiveAt',
      key: 'receiveAt',
      align: 'center',
      sorter: {
        compare: () => {
          setOrderParams({
            ...orderParams,
            sortColumn: 'receiveAt',
            sortDir: orderParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
          });

          return 1;
        },
      },
      render: (receiveAt: string) => receiveAt && dayjs(receiveAt).format(FULL_TIME_FORMAT),
    },
    {
      title: 'Action',
      align: 'center',
      fixed: 'right',
      render: (_, record: IOrderItem) => <Button onClick={() => navigate(`/orders/${record?.id}`)}>Detail</Button>,
    },
  ];

  return (
    <Space direction="vertical">
      <FilterOrder setParams={setOrderParams} storeId={storeId} staffId={staffId} lockerId={lockerId} />

      <TableCustom
        onChange={onChange}
        sortDirections={['ascend', 'descend']}
        showSorterTooltip={false}
        loading={isLoading}
        bordered
        rowKey={(record: IOrderItem) => record.id}
        dataSource={data?.items}
        columns={tableColumns}
        scroll={{ x: 2248, y: '45vh' }}
      />

      <CustomPagination<IOrderParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setOrderParams}
      />
    </Space>
  );
};

export default TableOrder;
