import type { IRangePicker } from '@/interface/layout/index.interface';
import type { ILockerTimelineItem, ILockerTimelineParams, LOCKER_STATUS } from '@/interface/locker/locker';
import type { ColumnsType, TableProps } from 'antd/es/table';

import { Card } from 'antd';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import TableCustom from '@/components/core/table';
import CustomPagination from '@/components/pagination/customPagination';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { LOCKER_EVENT } from '@/interface/locker/locker';
import { useLockerTimelinesQuery } from '@/services/lockerService';
import { dropdownRenderLockerStatus } from '@/utils/dropdownRender';
import { mappingEventValue } from '@/utils/insertMiddleString';

const LockerTimeline = ({
  title,
  lockerId,
  rangePicker,
}: {
  title: string;
  lockerId: number;
  rangePicker?: IRangePicker;
}) => {
  const [lockerTimelineParams, setLockerTimelineParams] = useState<ILockerTimelineParams>({
    pageNumber: 1,
    pageSize: 5,
  });

  // QUERY
  const { data, isLoading } = useLockerTimelinesQuery({ id: lockerId, ...rangePicker, ...lockerTimelineParams });

  const columns: ColumnsType<ILockerTimelineItem> = [
    {
      title: 'Time',
      dataIndex: 'createdAt',
      key: 'createdAt',
      render: (createdAt: string) => dayjs(createdAt).format(FULL_TIME_FORMAT),
      sorter: true,
    },
    {
      title: 'Event',
      dataIndex: 'event',
      key: 'event',
      render: (event: LOCKER_EVENT) => mappingEventValue(event),
      filters: Object.values(LOCKER_EVENT).map((event: LOCKER_EVENT) => {
        return { text: mappingEventValue(event), value: event, key: event };
      }),
      filterMultiple: false,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status: LOCKER_STATUS) => dropdownRenderLockerStatus(status),
    },
    {
      title: 'Error Code',
      dataIndex: 'errorCode',
      key: 'errorCode',
      align: 'right',
    },
  ];

  const onChange: TableProps<ILockerTimelineItem>['onChange'] = (pagination, filters, sorter: any, extra) => {
    setLockerTimelineParams({
      ...(filters?.event && filters?.event[0] && { event: filters?.event[0] as LOCKER_EVENT }),
      pageNumber: pagination?.current || 1,
      pageSize: pagination?.pageSize || 5,
      sortColumn: sorter?.columnKey,
      sortDir: sorter?.order === 'ascend' ? 'Asc' : 'Desc',
    });
  };

  return (
    <Card style={{ height: '100%', minHeight: 400 }} title={title}>
      <TableCustom
        columns={columns}
        dataSource={data?.items}
        loading={isLoading}
        bordered
        sortDirections={['ascend', 'descend']}
        showSorterTooltip={false}
        onChange={onChange}
        rowKey={(record: ILockerTimelineItem) => record.time}
        checkBox={false}
        scroll={{ x: 500, y: '100%' }}
      />

      <CustomPagination<ILockerTimelineParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setLockerTimelineParams}
        size="small"
      />
    </Card>
  );
};

export default LockerTimeline;
