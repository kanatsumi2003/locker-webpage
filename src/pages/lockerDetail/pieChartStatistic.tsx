import { Badge, Card, Col, List, Row } from 'antd';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Cell, Pie, PieChart, ResponsiveContainer, Tooltip } from 'recharts';

type DataType = 'all' | 'online' | 'offline';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042', '#E36E7E', '#8F66DE'];

interface Values {
  name: {
    zh_CN: string;
    en_US: string;
  };
  value: number;
}

interface Data {
  all: Values[];
  online: Values[];
  offline: Values[];
}

const data: Data = {
  all: [
    { name: { zh_CN: '家用电器', en_US: 'appliances' }, value: 4544 },
    { name: { zh_CN: '食用酒水', en_US: 'drinks' }, value: 3321 },
    { name: { zh_CN: '个护健康', en_US: 'health' }, value: 3113 },
    { name: { zh_CN: '服饰箱包', en_US: 'clothing' }, value: 2341 },
    { name: { zh_CN: '母婴产品', en_US: 'baby' }, value: 1231 },
    { name: { zh_CN: '其他', en_US: 'others' }, value: 132 },
  ],
  online: [
    { name: { zh_CN: '家用电器', en_US: 'appliances' }, value: 244 },
    { name: { zh_CN: '食用酒水', en_US: 'drinks' }, value: 231 },
    { name: { zh_CN: '个护健康', en_US: 'health' }, value: 311 },
    { name: { zh_CN: '服饰箱包', en_US: 'clothing' }, value: 41 },
    { name: { zh_CN: '母婴产品', en_US: 'baby' }, value: 121 },
    { name: { zh_CN: '其他', en_US: 'others' }, value: 111 },
  ],
  offline: [
    { name: { zh_CN: '家用电器', en_US: 'appliances' }, value: 99 },
    { name: { zh_CN: '食用酒水', en_US: 'drinks' }, value: 188 },
    { name: { zh_CN: '个护健康', en_US: 'health' }, value: 344 },
    { name: { zh_CN: '服饰箱包', en_US: 'clothing' }, value: 255 },
    { name: { zh_CN: '其他', en_US: 'others' }, value: 65 },
  ],
};

interface Props {
  title: string;
}

const PieChartStatistic = ({ title }: Props) => {
  const [dataType, setDataType] = useState<DataType>('all');

  const { locale } = useSelector(state => state.user);

  return (
    <Card className="pie-wrapper" title={title}>
      <Row gutter={[12, 12]}>
        <Col xl={{ span: 10 }} span={24}>
          <ResponsiveContainer height={250}>
            <PieChart>
              <Tooltip
                content={({ active, payload }: any) => {
                  if (active) {
                    const { name, value } = payload[0];
                    const total = data[dataType].map(d => d.value).reduce((a, b) => a + b);
                    const percent = ((value / total) * 100).toFixed(2) + '%';

                    return (
                      <span className="customTooltip">
                        {name[locale]} : {percent}
                      </span>
                    );
                  }

                  return null;
                }}
              />
              <Pie
                strokeOpacity={0}
                data={data[dataType]}
                innerRadius={60}
                outerRadius={80}
                paddingAngle={5}
                dataKey="value"
              >
                {data[dataType].map((_, index) => (
                  <Cell key={`cell-${index}`} fill={COLORS[index]} />
                ))}
              </Pie>
            </PieChart>
          </ResponsiveContainer>
        </Col>
        <Col xl={{ span: 14 }} span={24}>
          <List<Values>
            dataSource={data[dataType]}
            renderItem={(item, index) => {
              const total = data[dataType].map(d => d.value).reduce((a, b) => a + b);
              const percent = ((item.value / total) * 100).toFixed(2) + '%';

              return (
                <List.Item key={item.value}>
                  <Row gutter={12}>
                    <Col span={2}>
                      <Badge color={COLORS[index]} />
                    </Col>
                    <Col span={8}>
                      <span>{item.name[locale]}</span>
                    </Col>
                    <Col span={6}>
                      <span>{percent}</span>
                    </Col>
                    <Col span={8}>
                      <span>{item.value}</span>
                    </Col>
                  </Row>
                </List.Item>
              );
            }}
          />
        </Col>
      </Row>
    </Card>
  );
};

export default PieChartStatistic;
