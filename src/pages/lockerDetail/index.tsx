import type { MyTabsOption } from '@/components/business/tabs';

import { Card, Divider, Skeleton, Space } from 'antd';
import { useParams } from 'react-router-dom';

import { BaseTabs } from '@/components/business/tabs';
import { NotFoundCustom } from '@/components/notFound/notFoundCustom';
import { STATUS_CODE } from '@/constants/common';
import { useLockerQuery } from '@/services/lockerService';

import TableStaff from '../staff/tableStaff';
import TableBox from './box/tableBox';
import Detail from './detail';
import TableHardware from './hardware/tableHardware';
import TableOrder from './order/tableOrder';
import Statistic from './statistic';

const LockerDetail = () => {
  const { id } = useParams();
  // QUERY
  const { data, isLoading, error: errorGetLockerDetail } = useLockerQuery({ id: Number(id) });

  const options: MyTabsOption[] = [
    {
      label: 'Staff',
      tabKey: '1',
      value: 1,
      children: <TableStaff lockerId={Number(id)} />,
    },
    {
      label: 'Order',
      tabKey: '2',
      value: 2,
      children: <TableOrder lockerId={Number(id)} />,
    },

    {
      label: 'Hardware',
      tabKey: '3',
      value: 3,
      children: <TableHardware />,
    },
  ];

  //Return not found

  if (errorGetLockerDetail?.code === STATUS_CODE.BAD_REQUEST || !data) {
    return <NotFoundCustom loading={isLoading} />;
  }

  return (
    <Skeleton loading={isLoading} active>
      <Space direction="vertical" style={{ marginBottom: '1rem' }} size={24}>
        <Detail locker={data} />
        <Card title="Box Status">
          <TableBox />
        </Card>
        <Divider orientationMargin={0} orientation="left">
          THỐNG KÊ
        </Divider>
        <Statistic lockerId={data.id} />
        <BaseTabs options={options} />
      </Space>
    </Skeleton>
  );
};

export default LockerDetail;
