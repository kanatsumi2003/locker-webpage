import type { ILockerItem } from '@/interface/locker/locker';

import { EditOutlined, ShopOutlined, TableOutlined } from '@ant-design/icons';
import { Button, Card, Col, Descriptions, Divider, Image, Modal, Row, Select, Space, Typography } from 'antd';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { Toast } from '@/components/toast/toast';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { LOCKER_STATUS } from '@/interface/locker/locker';
import { useUpdateLockerStatusMutation } from '@/services/lockerService';
import { openDrawer } from '@/stores/layout.store';
import { setDataLocker } from '@/stores/locker.store';
import { setSelectedRecord } from '@/stores/table.store';
import { dropdownRenderLockerStatus, dropdownRenderStoreStatus } from '@/utils/dropdownRender';

const { Title } = Typography;
const { confirm } = Modal;
const { Option } = Select;

const Detail = ({ locker }: { locker: ILockerItem }) => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const [lockerStatus, setLockerStatus] = useState<LOCKER_STATUS>();

  useEffect(() => {
    if (locker) {
      setLockerStatus(locker.status);
      dispatch(setDataLocker(locker));
    }
  }, [locker]);

  // MUTATION
  const [
    updateLockerStatus,
    { isSuccess: isSuccessUpdateLockerStatus, isError: isErrorUpdateLockerStatus, error: errorUpdateLockerStatus },
  ] = useUpdateLockerStatusMutation();

  // CHANGE LOCKER_STATUS OF LOCKER
  const handleChangeStatusLocker = (value: LOCKER_STATUS) => {
    confirm({
      title: 'Confirm',
      content: 'Are you sure to update locker status?',
      onOk: () => {
        updateLockerStatus({ id: Number(id), status: value }).then(() => setLockerStatus(value));
      },
    });
  };

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isErrorUpdateLockerStatus) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: errorUpdateLockerStatus?.message });
      }, 100);
    }

    if (isSuccessUpdateLockerStatus) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Change Locker Status Successfully!' });
      }, 100);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccessUpdateLockerStatus, isErrorUpdateLockerStatus]);

  const handleUpdateLocker = () => {
    dispatch(setSelectedRecord([locker]));
    dispatch(openDrawer({ title: 'Update Locker' }));
  };

  return (
    <Card
      title={
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <Title level={3} style={{ margin: 0 }}>
            Locker Detail
          </Title>
          <Button icon={<EditOutlined />} onClick={handleUpdateLocker} type="primary" />
        </div>
      }
    >
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <Descriptions
            bordered
            title={
              <Space>
                <TableOutlined />
                Locker
              </Space>
            }
            layout="vertical"
            column={{ md: 3, sm: 1, xs: 1 }}
          >
            <Descriptions.Item label="Hình ảnh" span={3}>
              <Image src={locker.image} alt={locker.image} style={{ width: 300, height: 300, margin: 'auto' }} />
            </Descriptions.Item>
            <Descriptions.Item label="Name" span={1}>
              {locker.name}
            </Descriptions.Item>
            <Descriptions.Item label="Code">{locker.code}</Descriptions.Item>
            <Descriptions.Item label="Mac Address">{locker.macAddress || ''}</Descriptions.Item>
            <Descriptions.Item label="IP Address">{locker.ipAddress || ''}</Descriptions.Item>
            <Descriptions.Item label="Status">
              {locker.status === LOCKER_STATUS.INITIALIZED || locker.status === LOCKER_STATUS.DISCONNECTED ? (
                dropdownRenderLockerStatus(locker.status)
              ) : (
                <Select
                  defaultValue={locker.status}
                  value={isSuccessUpdateLockerStatus ? lockerStatus : locker.status}
                  style={{ width: '100%' }}
                  onChange={(value: LOCKER_STATUS) => handleChangeStatusLocker(value)}
                >
                  {locker.status === LOCKER_STATUS.ACTIVE && (
                    <>
                      <Option value={locker.status} disabled>
                        {dropdownRenderLockerStatus(locker.status)}
                      </Option>

                      <Option value={LOCKER_STATUS.INACTIVE}>
                        {dropdownRenderLockerStatus(LOCKER_STATUS.INACTIVE)}
                      </Option>

                      <Option value={LOCKER_STATUS.MAINTAINING}>
                        {dropdownRenderLockerStatus(LOCKER_STATUS.MAINTAINING)}
                      </Option>
                    </>
                  )}

                  {(locker.status === LOCKER_STATUS.MAINTAINING || locker.status === LOCKER_STATUS?.INACTIVE) && (
                    <>
                      <Option value={LOCKER_STATUS.ACTIVE}>{dropdownRenderLockerStatus(LOCKER_STATUS.ACTIVE)}</Option>

                      <Option value={locker.status} disabled>
                        {dropdownRenderLockerStatus(locker.status)}
                      </Option>
                    </>
                  )}
                </Select>
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Province">{locker.location?.province?.name}</Descriptions.Item>
            <Descriptions.Item label="District">{locker.location?.district?.name}</Descriptions.Item>
            <Descriptions.Item label="Ward">{locker.location?.ward?.name}</Descriptions.Item>
            <Descriptions.Item label="Address" span={3}>
              {locker.location?.address}
            </Descriptions.Item>
            <Descriptions.Item label="Description" span={3}>
              {locker.description}
            </Descriptions.Item>
            <Descriptions.Item label="Created At">{dayjs(locker.createdAt).format(FULL_TIME_FORMAT)}</Descriptions.Item>
            <Descriptions.Item label="Updated At">{dayjs(locker.updatedAt).format(FULL_TIME_FORMAT)}</Descriptions.Item>
          </Descriptions>

          <Divider />

          <Descriptions
            title={
              <Space>
                <ShopOutlined />
                Store
              </Space>
            }
            bordered
            column={{ md: 3, sm: 1, xs: 1 }}
            layout="vertical"
          >
            <Descriptions.Item label="Name">{locker.store?.name}</Descriptions.Item>
            <Descriptions.Item label="Contact Phone">{locker.store?.contactPhone}</Descriptions.Item>
            <Descriptions.Item label="Status">
              {locker.store?.status && dropdownRenderStoreStatus(locker.store?.status)}
            </Descriptions.Item>
            <Descriptions.Item label="Province">{locker.store?.location?.province?.name}</Descriptions.Item>
            <Descriptions.Item label="District">{locker.store?.location?.district?.name}</Descriptions.Item>
            <Descriptions.Item label="Ward">{locker.store?.location?.ward?.name}</Descriptions.Item>
            <Descriptions.Item label="Address" span={3}>
              {locker.store?.location?.address}
            </Descriptions.Item>
            <Descriptions.Item label="Created At">
              {dayjs(locker.store?.createdAt).format(FULL_TIME_FORMAT)}
            </Descriptions.Item>
            <Descriptions.Item label="Updated At">
              {dayjs(locker.store?.updatedAt).format(FULL_TIME_FORMAT)}
            </Descriptions.Item>
            <Descriptions.Item label="">{}</Descriptions.Item>
          </Descriptions>
        </Col>
      </Row>
    </Card>
  );
};

export default Detail;
