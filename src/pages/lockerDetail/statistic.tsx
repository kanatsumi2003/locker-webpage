import type { IRangePicker } from '@/interface/layout/index.interface';

import './index.less';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Col, Form, Row, Space } from 'antd';
import { useState } from 'react';

import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';

import DateRangePicker from '../components/dateRangePicker';
import DashboardStatistic from '../dashboard/dashboardStatistic';
import LockerTimeline from './lockerTimeline';
import StatisticCounter from './statisticCounter';

const Statistic = ({ lockerId }: { lockerId: number }) => {
  const [rangePicker, setRangePicker] = useState<IRangePicker>({});
  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setRangePicker({
      from: undefined,
      to: undefined,
    });
  };

  return (
    <Space direction="vertical" size="large">
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <Form form={form} className="filter-group" layout="vertical">
            <ScrollWrapper>
              <Form.Item name={['from', 'to']}>
                <DateRangePicker setParams={setRangePicker} />
              </Form.Item>

              <Form.Item>
                <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large">
                  Reset
                </Button>
              </Form.Item>
            </ScrollWrapper>
          </Form>
        </Col>
        <Col xl={10} span={24}>
          <DashboardStatistic title="Thống kê đơn hàng" lockerId={lockerId} rangePicker={rangePicker} />
        </Col>
        <Col xl={14} span={24}>
          <Row gutter={[12, 12]} style={{ height: '100%' }}>
            <Col span={24}>
              <StatisticCounter lockerId={lockerId} rangePicker={rangePicker} />
            </Col>
            <Col span={24}>
              <LockerTimeline title="Locker Timeline" lockerId={lockerId} rangePicker={rangePicker} />
            </Col>
          </Row>
        </Col>
      </Row>
    </Space>
  );
};

export default Statistic;
