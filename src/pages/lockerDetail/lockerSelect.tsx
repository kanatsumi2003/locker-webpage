import type { ILockerItem, ILockerParams } from '@/interface/locker/locker';
import type { SelectProps } from 'antd';
import type { FC } from 'react';

import React, { useState } from 'react';

import InfiniteSelect from '@/components/infiniteSelect/infiniteSelect';
import { useLockersQuery } from '@/services/lockerService';

const LockerSelect: FC<SelectProps> = ({ ...rest }) => {
  const [lockerParams, setLockerParams] = useState<ILockerParams>({ pageNumber: 1, pageSize: 10 });

  const { data } = useLockersQuery({ ...lockerParams });

  const onLoad = (params: any) => {
    setLockerParams({
      search: params?.search,
      pageNumber: params?.pageNumber,
      pageSize: params?.pageSize,
    });
  };

  const dataSelect = data?.items?.map((item: ILockerItem) => ({
    label: item?.name,
    value: item?.id,
  }));

  return <InfiniteSelect {...rest} onLoad={onLoad} data={dataSelect} />;
};

export default LockerSelect;
