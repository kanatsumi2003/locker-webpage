import type { IHardwareItem } from '@/interface/hardware/hardware';
import type { ColumnsType } from 'antd/es/table';

import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Modal, Space } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import TableCustom from '@/components/core/table';
import { Toast } from '@/components/toast/toast';
import { useDeleteHardwareMutation, useHardwaresQuery } from '@/services/hardwareService';
import { closeDrawer, openDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';

const { confirm } = Modal;

const tableColumns: ColumnsType<IHardwareItem> = [
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Code', dataIndex: 'code', key: 'code' },
  { title: 'Brand', dataIndex: 'brand', key: 'brand' },
  {
    title: 'Price',
    dataIndex: 'price',
    key: 'price',
    align: 'right',
    sorter: (a: IHardwareItem, b: IHardwareItem) => a.price - b.price,
    sortDirections: ['ascend', 'descend'],
    render: (price: number) => formatDecimalPrecision(price),
  },
  { title: 'Description', dataIndex: 'description', key: 'description' },
];

const TableHardware = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const rowKeys = useSelector(state => state.table.selectedRowKeys);

  // QUERY
  const { data, isLoading } = useHardwaresQuery({ id: Number(id) });

  // MUTATION
  const [deleteHardware, { isSuccess, isError }] = useDeleteHardwareMutation();

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: 'Create Hardware Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Create Hardware Successfully!' });
        dispatch(closeDrawer());
        dispatch(refreshRowKeys());
      }, 100);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError]);

  const handleEditHardware = () => {
    dispatch(openDrawer({ title: 'Update Hardware', flag: 'hardware' }));
  };

  const handleCreateHardware = () => {
    dispatch(openDrawer({ title: 'Create Hardware', flag: 'hardware' }));
  };

  const handleDeleteHardware = () => {
    confirm({
      title: 'Confirm',
      content: 'Are you sure to delete this hardware?',
      onOk: () => deleteHardware({ id: Number(id), hardwareId: Number(rowKeys[0]) }),
    });
  };

  return (
    <>
      <Space style={{ margin: '1rem 0' }}>
        <Button type="primary" onClick={handleCreateHardware}>
          <PlusOutlined />
        </Button>
        <Button type="dashed" onClick={handleEditHardware} disabled={!rowKeys.length || rowKeys.length > 1}>
          <EditOutlined />
        </Button>
        <Button danger onClick={handleDeleteHardware} disabled={!rowKeys.length || rowKeys.length > 1}>
          <DeleteOutlined />
        </Button>
      </Space>
      <TableCustom
        bordered
        loading={isLoading}
        rowKey={(record: IHardwareItem) => record.id}
        dataSource={data?.items}
        columns={tableColumns}
      />
    </>
  );
};

export default TableHardware;
