import type { LOCKER_STATUS } from '@/interface/locker/locker';
import type { FEE_TYPE, IServiceItem } from '@/interface/service/service';
import type { ColumnsType } from 'antd/es/table';

import { DeleteOutlined, EditOutlined, PlusOutlined } from '@ant-design/icons';
import { Button, Image, Space } from 'antd';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import TableCustom from '@/components/core/table';
import { useServicesQuery } from '@/services/serviceService';
import { openDrawer } from '@/stores/layout.store';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import { dropdownRenderLockerStatus } from '@/utils/dropdownRender';

const tableColumns: ColumnsType<IServiceItem> = [
  { title: 'Image', dataIndex: 'image', key: 'image', render: image => <Image src={image} /> },
  { title: 'Name', dataIndex: 'name', key: 'name' },
  { title: 'Unit', dataIndex: 'unit', key: 'unit' },
  {
    title: 'Fee',
    dataIndex: 'fee',
    key: 'fee',
    align: 'right',
    render: (fee: number) => formatDecimalPrecision(fee),
  },
  { title: 'Description', dataIndex: 'description', key: 'description' },
  {
    title: 'Status',
    dataIndex: 'status',
    key: 'status',
    align: 'center',
    render: (status: LOCKER_STATUS) => status && dropdownRenderLockerStatus(status),
  },
];

const TableService = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const rowKeys = useSelector(state => state.table.selectedRowKeys);
  const { data, isLoading, isSuccess } = useServicesQuery();

  const handleEditService = () => {
    dispatch(openDrawer({ title: 'Update Service', flag: 'service' }));
  };

  const handleCreateService = () => {
    dispatch(openDrawer({ title: 'Create Service', flag: 'service' }));
  };

  return (
    <>
      <Space style={{ margin: '1rem 0' }}>
        <Button type="primary" onClick={handleCreateService}>
          <PlusOutlined />
        </Button>
        <Button type="dashed" onClick={handleEditService} disabled={!rowKeys.length || rowKeys.length > 1}>
          <EditOutlined />
        </Button>
        <Button danger disabled={!rowKeys.length || rowKeys.length > 1}>
          <DeleteOutlined />
        </Button>
      </Space>
      <TableCustom
        bordered
        loading={isLoading}
        rowKey={(record: IServiceItem) => record.id}
        dataSource={data?.items}
        columns={tableColumns}
      />
    </>
  );
};

export default TableService;
