import type { IBoxItem } from '@/interface/box/box';
import type { MenuProps } from 'antd';

import './index.less';

import { LockOutlined, StopOutlined, UnlockOutlined } from '@ant-design/icons';
import { Dropdown, Empty, Spin, Tag } from 'antd';
import { useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';

import { PATH } from '@/constants/common';
import { useBoxesQuery } from '@/services/boxService';
import { dropdownRenderOrderStatus, dropdownRenderOrderType } from '@/utils/dropdownRender';

const TableBox = () => {
  const { id } = useParams();

  const dataLocker = useSelector(state => state.locker.dataLocker);

  // QUERY
  const { data, isLoading } = useBoxesQuery({ id: Number(id) }, { skip: !id });

  const items: MenuProps['items'] =
    data?.map((box: IBoxItem) => ({
      key: box.id,
      label: box.isActive ? (
        <div className="box-detail">
          <div className="box-status">
            <strong>Trạng thái </strong>{' '}
            {box.isAvailable ? <Tag color="green">Trống</Tag> : <Tag color="red">Đã đặt</Tag>}
          </div>
          {box.lastOrder && (
            <>
              <strong>Đơn hàng gần nhất </strong>
              <ul>
                <li>
                  Mã đơn hàng: <Link to={`${PATH.ORDER}/${box.lastOrder.id}`}>{box.lastOrder.id}</Link>
                </li>
                <li>Loại đơn hàng: {dropdownRenderOrderType(box.lastOrder.type)}</li>
                <li>Trạng thái đơn hàng: {dropdownRenderOrderStatus(box.lastOrder.status)}</li>
              </ul>
            </>
          )}
        </div>
      ) : (
        <div>Box đang bị khóa</div>
      ),
    })) || [];

  return (
    <div className="box-container">
      {isLoading && <Spin spinning={isLoading} />}
      {dataLocker && data && data.length > 0 ? (
        data.map((box: IBoxItem, index: number) => (
          <Dropdown
            menu={{ items: items.filter(item => item?.key === box.id) }}
            placement="top"
            arrow={{ pointAtCenter: true }}
          >
            <div
              className="box-item"
              style={{
                backgroundColor: `${box?.isActive ? (box?.isAvailable ? '#95de64' : '#f87171') : '#adb5bd'}`,
              }}
            >
              <span style={{ position: 'absolute', top: 10, right: 10, color: '#fff' }}>{index + 1}</span>
              {box?.isActive ? (
                box?.isAvailable ? (
                  <UnlockOutlined style={{ fontSize: '1.2rem', color: '#2b8a3e' }} />
                ) : (
                  <LockOutlined style={{ fontSize: '1.2rem', color: '#c92a2a' }} />
                )
              ) : (
                <StopOutlined style={{ fontSize: '1.2rem', color: '#868e96' }} />
              )}
            </div>
          </Dropdown>
        ))
      ) : (
        <Empty />
      )}
    </div>
  );
};

export default TableBox;
