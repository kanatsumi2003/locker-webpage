import type { IRangePicker } from '@/interface/layout/index.interface';

import { Card, Col, Row } from 'antd';

import StatisticCustom from '@/components/statistic/statisticCustom';
import { useLockerStatisticsQuery } from '@/services/lockerService';
import { mapLockerEventColor } from '@/utils/colorAdapter';
import { mappingEventValue } from '@/utils/insertMiddleString';

interface Props {
  rangePicker?: IRangePicker;
  lockerId: number;
}

const StatisticCounter = ({ rangePicker, lockerId }: Props) => {
  // QUERY
  const { data } = useLockerStatisticsQuery({ ...rangePicker, id: lockerId });

  return (
    <Row gutter={[12, 12]} style={{ height: '100%' }}>
      <Col span={24} className="locker-status-statistic">
        <Row gutter={[12, 12]} style={{ height: '100%' }}>
          {data?.map((d, i) => (
            <Col flex={'1 0 50%'} key={i}>
              <Card bordered={false} style={{ background: mapLockerEventColor(d.event), height: '100%' }}>
                <StatisticCustom title={mappingEventValue(d.event)} value={d.count} />
              </Card>
            </Col>
          ))}
        </Row>
      </Col>
    </Row>
  );
};

export default StatisticCounter;
