import type { ILocation } from '@/interface/location/location';
import type { ILockerItem, ILockerParams, LOCKER_STATUS } from '@/interface/locker/locker';
import type { IStoreItem } from '@/interface/store/store';
import type { ColumnsType, TableProps } from 'antd/es/table';

import { Button, Image, Space, Tooltip } from 'antd';
import React, { useState } from 'react';
import { useNavigate } from 'react-router';
import { Link } from 'react-router-dom';

import TableCustom from '@/components/core/table';
import CustomPagination from '@/components/pagination/customPagination';
import { useLockersQuery } from '@/services/lockerService';
import { dropdownRenderLockerStatus } from '@/utils/dropdownRender';

import FilterLocker from './filterLocker';

interface Props {
  storeId?: number;
  staffId?: number;
}

const TableLocker = ({ storeId, staffId }: Props) => {
  const navigate = useNavigate();

  // STATE
  const [lockerParams, setLockerParams] = useState<ILockerParams>({ pageNumber: 1, pageSize: 20, storeId });

  // QUERY
  const { data, isLoading, isError } = useLockersQuery(lockerParams);

  // ACTION ON TABLE (SORT,...)
  const onChange: TableProps<ILockerItem>['onChange'] = (pagination, filters, sorter: any, extra) => {
    setLockerParams({
      ...lockerParams,
      sortColumn: sorter?.columnKey,
      sortDir: sorter?.order === 'ascend' ? 'Asc' : 'Desc',
    });
  };

  // TABLE COLUMNS
  const tableColumns: ColumnsType<ILockerItem> = [
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      width: 150,
      render: (image: string) => <Image src={image} alt={image} />,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      ellipsis: true,
      render: (name: string) => (
        <Tooltip placement="bottom" title={name}>
          {name}
        </Tooltip>
      ),
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
      ellipsis: true,
      render: (code: string) => (
        <Tooltip placement="bottom" title={code}>
          {code}
        </Tooltip>
      ),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      width: 130,
      render: (status: LOCKER_STATUS) => dropdownRenderLockerStatus(status),
    },
    {
      title: 'N.o.boxes',
      dataIndex: 'boxCount',
      key: 'boxCount',
    },
    {
      title: 'Location',
      key: 'location',
      children: [
        {
          title: 'Province',
          dataIndex: 'location',
          key: 'province',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.province?.name}>
              {location?.province?.name}
            </Tooltip>
          ),
        },
        {
          title: 'District',
          dataIndex: 'location',
          key: 'district',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.district?.name}>
              {location?.district?.name}
            </Tooltip>
          ),
        },
        {
          title: 'Ward',
          dataIndex: 'location',
          key: 'ward',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.ward?.name}>
              {location?.ward?.name}
            </Tooltip>
          ),
        },
        {
          title: 'Address',
          dataIndex: 'location',
          key: 'address',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.address}>
              {location?.address}
            </Tooltip>
          ),
        },
      ],
    },
    {
      title: 'Store',
      dataIndex: 'store',
      key: 'store',
      ellipsis: true,
      render: (store: IStoreItem) => (
        <Tooltip placement="bottom" title={store?.name}>
          <Link to={`/stores/${store?.id}`}>{store?.name}</Link>
        </Tooltip>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      align: 'center',
      fixed: 'right',
      render: (_: unknown, record: ILockerItem) => <Button onClick={() => navigate(`${record?.id}`)}>Details</Button>,
    },
  ];

  return (
    <Space direction="vertical">
      <FilterLocker setParams={setLockerParams} storeId={storeId} staffId={staffId} />
      <TableCustom
        onChange={onChange}
        sortDirections={['ascend', 'descend']}
        showSorterTooltip={false}
        bordered
        loading={isLoading}
        rowKey={(record: ILockerItem) => record.id}
        dataSource={data?.items}
        columns={tableColumns}
      />
      <CustomPagination<ILockerParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setLockerParams}
      />
    </Space>
  );
};

export default TableLocker;
