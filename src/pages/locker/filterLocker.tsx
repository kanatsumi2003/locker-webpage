import type { IAddress } from '@/interface/location/location';
import type { ILockerParams } from '@/interface/locker/locker';
import type { Dispatch, SetStateAction } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form, Select } from 'antd';
import React, { useState } from 'react';

import CustomSearch from '@/components/input/customSearch';
import CustomSelect from '@/components/input/customSelect';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';
import { LOCKER_STATUS } from '@/interface/locker/locker';
import { useAddressesQuery } from '@/services/addressService';
import { dropdownRenderLockerStatus } from '@/utils/dropdownRender';

import StaffSelect from '../staff/staffSelect';
import StoreSelect from '../store/storeSelect';

const { Option } = Select;

interface Props {
  storeId?: number;
  staffId?: number;
  setParams: Dispatch<SetStateAction<ILockerParams>>;
}

const FilterLocker = ({ storeId, staffId, setParams }: Props) => {
  const [form] = Form.useForm();

  // STATE
  const [parentCodeDistrict, setParentCodeDistrict] = useState<string | undefined>('');
  const [parentCodeWard, setParentCodeWard] = useState<string | undefined>('');

  // QUERY
  const { data: dataProvince, isLoading: isLoadingProvince } = useAddressesQuery();
  const { data: dataDistrict, isLoading: isLoadingDistrict } = useAddressesQuery(
    {
      parentCode: parentCodeDistrict,
    },
    {
      skip: !parentCodeDistrict,
    },
  );
  const { data: dataWard, isLoading: isLoadingWard } = useAddressesQuery(
    {
      parentCode: parentCodeWard,
    },
    {
      skip: !parentCodeWard,
    },
  );

  const handleResetFilter = () => {
    form.resetFields();
    setParams({ pageNumber: 1, pageSize: 10 });
  };

  const onValuesChange = (changedValues: ILockerParams) => {
    setParams((prev: ILockerParams) => ({ ...prev, ...changedValues }));
  };

  return (
    <Form form={form} name="filter-locker" className="filter-group" layout="vertical" onValuesChange={onValuesChange}>
      {/* SEARCH LOCKER */}
      <Form.Item label="Search" name="search">
        <CustomSearch<ILockerParams> placeholder="Search..." query="search" setParams={setParams} />
      </Form.Item>

      <ScrollWrapper>
        {/* FILTER LOCKER BY STORE */}
        {!storeId && (
          <Form.Item label="Store" name="storeId">
            <StoreSelect
              showSearch
              allowClear
              placeholder="Store"
              onChange={value => setParams((prev: ILockerParams) => ({ ...prev, ...{ storeId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER LOCKER BY STAFF */}
        {!staffId && (
          <Form.Item label="Staff" name="staffId">
            <StaffSelect
              showSearch
              allowClear
              placeholder="Staff"
              onChange={value => setParams((prev: ILockerParams) => ({ ...prev, ...{ staffId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER LOCKER BY LOCKER_STATUS */}
        <Form.Item label="Status" name="status">
          <CustomSelect<ILockerParams> setParams={setParams} name="status" placeholder="Status">
            {Object.values(LOCKER_STATUS).map((status: LOCKER_STATUS) => (
              <Option value={status} key={status}>
                {dropdownRenderLockerStatus(status)}
              </Option>
            ))}
          </CustomSelect>
        </Form.Item>

        {/* FILTER LOCKER BY PROVINCE */}
        <Form.Item label="Province" name="provinceCode">
          <CustomSelect<ILockerParams>
            setParams={setParams}
            name="provinceCode"
            placeholder="Province"
            loading={isLoadingProvince}
            options={dataProvince?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            onClear={() => {
              setParentCodeWard(undefined);
              form?.resetFields(['districtCode', 'wardCode']);
            }}
            onChange={(value: string) => {
              form?.setFieldsValue({ provinceCode: value, districtCode: undefined });
              setParentCodeDistrict(value);
            }}
          />
        </Form.Item>

        {/* FILTER LOCKER BY DISTRICT */}
        <Form.Item label="District" name={'districtCode'}>
          <CustomSelect<ILockerParams>
            setParams={setParams}
            name="districtCode"
            placeholder="District"
            loading={isLoadingDistrict}
            options={dataDistrict?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            onChange={(value: string) => {
              form?.setFieldsValue({
                provinceCode: parentCodeDistrict,
                wardCode: undefined,
                districtCode: value,
              });
              setParentCodeWard(value);
            }}
            disabled={!parentCodeDistrict}
          />
        </Form.Item>

        {/* FILTER LOCKER BY WARD */}
        <Form.Item label="Ward" name="wardCode">
          <CustomSelect<ILockerParams>
            setParams={setParams}
            name="wardCode"
            placeholder="Ward"
            loading={isLoadingWard}
            options={dataWard?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            disabled={!parentCodeWard}
          />
        </Form.Item>

        {/* BUTTON RESET FILTER */}
        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large" className="reset-btn">
          Reset Filters
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default FilterLocker;
