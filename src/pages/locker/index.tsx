import type { FC } from 'react';

import '../../styles/index.less';

import { PlusOutlined } from '@ant-design/icons';
import { Button, Space } from 'antd';
import { useDispatch } from 'react-redux';

import { openDrawer } from '@/stores/layout.store';

import TableLocker from './tableLocker';

const LockerPage: FC = () => {
  const dispatch = useDispatch();

  // ACTIVITY ON CLICK BUTTON
  const handleCreateLocker = () => {
    dispatch(openDrawer({ title: 'Create Locker' }));
  };

  return (
    <>
      <Space style={{ margin: '1rem 0' }}>
        <Button type="primary" onClick={handleCreateLocker}>
          <PlusOutlined />
        </Button>
      </Space>

      <TableLocker />
    </>
  );
};

export default LockerPage;
