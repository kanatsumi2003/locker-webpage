import type { OptionType } from '@/interface';
import type { IAddress } from '@/interface/location/location';
import type { ILockerBody, ILockerItem } from '@/interface/locker/locker';
import type { FormInstance, InputRef, UploadFile } from 'antd';
import type { DragEndEvent, LatLngTuple } from 'leaflet';

import { Button, Col, Form, Input, Modal, Row, Select } from 'antd';
import { createContext, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import CustomMap from '@/components/map/map';
import { Toast } from '@/components/toast/toast';
import DraggerUpload from '@/components/upload/draggerUpload';
import { STAFF_STATUS } from '@/interface/staff/staff';
import { STORE_STATUS } from '@/interface/store/store';
import { LocaleFormatter } from '@/locales';
import { useAddressesQuery, useLazyMapQuery } from '@/services/addressService';
import { useCreateLockerMutation, useUpdateLockerMutation } from '@/services/lockerService';
import { closeDrawer } from '@/stores/layout.store';
import { toLowerCaseNonAccentVietnamese } from '@/utils/formatSearch';
import { getFile } from '@/utils/getFile';
import { required } from '@/utils/validate';

import StaffSelect from '../staff/staffSelect';
import StoreSelect from '../store/storeSelect';

const { confirm } = Modal;

interface Context {
  form: FormInstance<any>;
}

export const LockerContext = createContext<Context | null>(null);

const FormLocker = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const initRef = useRef<InputRef>(null);

  // REDUX STATE
  const selectedRecord: ILockerItem[] = useSelector(state => state.table.selectedRecord);
  const { visible } = useSelector(state => state.drawer);

  // STATE
  const [storeId, setStoreId] = useState<number>();
  const [parentCodeDistrict, setParentCodeDistrict] = useState<string>('');
  const [parentCodeWard, setParentCodeWard] = useState<string>('');
  const [fileMap, setFileMap] = useState<Map<number, UploadFile[]>>(
    selectedRecord[0]
      ? new Map<number, UploadFile[]>().set(0, [
          { uid: '0', url: selectedRecord[0]?.image, name: selectedRecord[0]?.image },
        ])
      : new Map(),
  );
  const [location, setLocation] = useState<LatLngTuple>();

  // MUTATION
  const [createLocker, { isSuccess, isError, error }] = useCreateLockerMutation();
  const [updateLocker, { isSuccess: isSuccessUpdate, isError: isErrorUpdate, error: errorUpdate }] =
    useUpdateLockerMutation();

  useEffect(() => {
    const timeout = setTimeout(() => {
      initRef?.current?.focus();
      setParentCodeDistrict(form?.getFieldValue('location')?.provinceCode || '');
      setParentCodeWard(form?.getFieldValue('location')?.districtCode || '');
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [visible]);

  // QUERY
  const { data: dataProvince, isLoading: isLoadingProvince } = useAddressesQuery();
  const { data: dataDistrict, isLoading: isLoadingDistrict } = useAddressesQuery(
    {
      parentCode: parentCodeDistrict,
    },
    {
      skip: !parentCodeDistrict,
    },
  );
  const { data: dataWard, isLoading: isLoadingWard } = useAddressesQuery(
    {
      parentCode: parentCodeWard,
    },
    {
      skip: !parentCodeWard,
    },
  );
  const [trigger, { data: dataMap }] = useLazyMapQuery();

  // NOTIFICATION AFTER CRUD
  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: error?.message || 'Create Locker Failed!' });
      }, 100);
    }

    if (isErrorUpdate) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: errorUpdate?.message || 'Update Locker Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Create Locker Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
    }

    if (isSuccessUpdate) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Update Locker Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isSuccessUpdate, isError, isErrorUpdate]);

  console.log(selectedRecord[0]);

  useEffect(() => {
    if (selectedRecord.length) {
      form.setFieldsValue({
        ...selectedRecord[0],
        location: {
          provinceCode: selectedRecord[0]?.location?.province?.code,
          districtCode: selectedRecord[0]?.location?.district?.code,
          wardCode: selectedRecord[0]?.location?.ward?.code,
          address: selectedRecord[0]?.location?.address,
        },
        storeId: selectedRecord[0]?.store?.id,
      });
      setLocation([selectedRecord[0]?.location?.latitude, selectedRecord[0]?.location?.longitude]);
    } else {
      form.resetFields();
    }
  }, [selectedRecord]);

  // SUBMIT FORM VALUES
  const onFinish = (values: ILockerBody) => {
    const isUpdateLocker = selectedRecord.length;

    if (isUpdateLocker) {
      confirm({
        title: 'Confirm',
        content: 'Are you sure to update locker?',
        onOk: () => updateLocker({ ...values, id: selectedRecord[0]?.id }),
      });
    } else {
      createLocker({
        ...values,
        location: { ...values?.location, latitude: location?.at(0), longitude: location?.at(1) },
      });
    }
  };

  const onValuesChange = (changedValues: ILockerBody, allValues: ILockerBody) => {
    const provinceName = dataProvince?.find(province => province.code === allValues?.location?.provinceCode)?.name;
    const districtName = dataDistrict?.find(district => district.code === allValues?.location?.districtCode)?.name;
    const wardName = dataWard?.find(ward => ward.code === allValues?.location?.wardCode)?.name;

    const queryLocation = [provinceName, districtName, wardName].filter(item => item).join(', ');

    (changedValues?.location?.provinceCode ||
      changedValues?.location?.provinceCode ||
      changedValues?.location?.wardCode) &&
      trigger({ q: queryLocation });
  };

  useEffect(() => {
    dataMap && setLocation(dataMap?.features?.at(0)?.geometry?.coordinates);
  }, [dataMap]);

  const onMarkerDragEnd = (e: DragEndEvent) => {
    setLocation([e?.target?._latlng?.lat, e?.target?._latlng?.lng]);
  };

  return (
    <LockerContext.Provider value={{ form }}>
      <div style={{ marginBottom: '1rem' }}>
        <Row gutter={[12, 12]}>
          <Col span={24}>
            <Form form={form} name={'locker'} layout="vertical" onFinish={onFinish} onValuesChange={onValuesChange}>
              <Form.Item label={<LocaleFormatter id="app.locker.field.name" />} name="name" rules={[required('Name')]}>
                <Input ref={initRef} />
              </Form.Item>

              <Form.Item label="Store" name="storeId" rules={[required('Store')]}>
                <StoreSelect
                  onChange={value => setStoreId(value)}
                  showSearch
                  allowClear
                  storeStatus={STORE_STATUS.ACTIVE}
                  size="middle"
                />
              </Form.Item>

              <Form.Item label="Staff" name="staffIds" rules={[required('Staff')]}>
                <StaffSelect
                  showSearch
                  allowClear
                  staffStatus={STAFF_STATUS.ACTIVE}
                  storeId={storeId}
                  size="middle"
                  mode="multiple"
                  maxTagCount={'responsive'}
                  disabled={!storeId}
                />
              </Form.Item>

              <Row gutter={[12, 12]}>
                <Col md={{ span: 8 }} span={24}>
                  <Form.Item
                    label={<LocaleFormatter id="app.locker.field.province" />}
                    name={['location', 'provinceCode']}
                    rules={[required('Province')]}
                  >
                    <Select
                      options={dataProvince?.map((item: IAddress) => {
                        return {
                          label: item.name,
                          value: item.code,
                        };
                      })}
                      loading={isLoadingProvince}
                      allowClear
                      onClear={() => {
                        setParentCodeWard('');
                      }}
                      showSearch
                      filterOption={(input: string, option: OptionType | undefined) =>
                        toLowerCaseNonAccentVietnamese(option?.label).indexOf(toLowerCaseNonAccentVietnamese(input)) >=
                        0
                      }
                      onChange={(value: string) => {
                        form.setFieldValue('location', { provinceCode: value, districtCode: '' });
                        setParentCodeDistrict(value);
                      }}
                    />
                  </Form.Item>
                </Col>

                <Col md={{ span: 8 }} span={24}>
                  <Form.Item
                    label={<LocaleFormatter id="app.locker.field.district" />}
                    name={['location', 'districtCode']}
                    rules={[required('District')]}
                  >
                    <Select
                      disabled={!parentCodeDistrict}
                      options={dataDistrict?.map((item: IAddress) => {
                        return {
                          label: item.name,
                          value: item.code,
                        };
                      })}
                      loading={isLoadingDistrict}
                      allowClear
                      showSearch
                      filterOption={(input: string, option: OptionType | undefined) =>
                        toLowerCaseNonAccentVietnamese(option?.label).indexOf(toLowerCaseNonAccentVietnamese(input)) >=
                        0
                      }
                      onChange={(value: string) => {
                        form.setFieldValue('location', {
                          provinceCode: parentCodeDistrict,
                          wardCode: '',
                          districtCode: value,
                        });
                        setParentCodeWard(value);
                      }}
                    />
                  </Form.Item>
                </Col>

                <Col md={{ span: 8 }} span={24}>
                  <Form.Item
                    label={<LocaleFormatter id="app.locker.field.ward" />}
                    name={['location', 'wardCode']}
                    rules={[required('Ward')]}
                  >
                    <Select
                      disabled={!parentCodeWard}
                      options={dataWard?.map((item: IAddress) => {
                        return {
                          label: item.name,
                          value: item.code,
                        };
                      })}
                      loading={isLoadingWard}
                      allowClear
                      showSearch
                      filterOption={(input: string, option: OptionType | undefined) =>
                        toLowerCaseNonAccentVietnamese(option?.label).indexOf(toLowerCaseNonAccentVietnamese(input)) >=
                        0
                      }
                    />
                  </Form.Item>
                </Col>
              </Row>

              <Form.Item label={'Address'} name={['location', 'address']} rules={[required('Address')]}>
                <Input />
              </Form.Item>

              <CustomMap
                {...(location && {
                  center: { location },
                  markers: [
                    {
                      location,
                      popup: undefined,
                    },
                  ],
                  markerDraggable: true,
                  onMarkerDragEnd,
                })}
                scrollWheelZoom={true}
              />

              <Form.Item label="Image" name={'image'} getValueFromEvent={getFile}>
                <DraggerUpload
                  multiple={false}
                  fileMap={fileMap}
                  setFileMap={setFileMap}
                  fieldKey={0}
                  name="image"
                  form={form}
                />
              </Form.Item>

              <Form.Item label="Description" name={'description'}>
                <Input.TextArea rows={3} />
              </Form.Item>
            </Form>
            <div style={{ float: 'right', marginTop: '1rem' }}>
              <Button type="primary" htmlType="submit" form="locker">
                <LocaleFormatter id="app.locker.action.done" />
              </Button>
            </div>
          </Col>
        </Row>
      </div>
    </LockerContext.Provider>
  );
};

export default FormLocker;
