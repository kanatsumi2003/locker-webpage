import type { FormInstance, FormListFieldData, InputRef } from 'antd';

import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, InputNumber, Row, Typography } from 'antd';
import React, { useEffect, useRef } from 'react';

import { required } from '@/utils/validate';

const { Title } = Typography;

interface Props {
  form: FormInstance<any>;
}

const FormHardware = ({ form }: Props) => {
  const initRef = useRef<InputRef>(null);

  useEffect(() => {
    const timeout = setTimeout(() => {
      initRef?.current?.focus();
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  return (
    <div key="hardwares">
      <Form.List name="hardwares" initialValue={[]}>
        {(fields, { add, remove }) => (
          <Row gutter={[12, 12]} key={new Date().getUTCMilliseconds()}>
            {fields.map((field: FormListFieldData) => (
              <Col span={12} key={field.key}>
                <Title level={3}>{`Hardware ${field.key + 1}`}</Title>

                <Row gutter={[12, 12]}>
                  <Col md={{ span: 12 }} span={24}>
                    <Form.Item label="Name" name={[field.name, 'name']} rules={[required('Name')]}>
                      <Input ref={initRef} />
                    </Form.Item>
                  </Col>

                  <Col md={{ span: 12 }} span={24}>
                    <Form.Item label="Code" name={[field.name, 'code']} rules={[required('Code')]}>
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>

                <Row gutter={[12, 12]}>
                  <Col md={{ span: 12 }} span={24}>
                    <Form.Item label="Brand" name={[field.name, 'brand']} rules={[required('Brand')]}>
                      <Input />
                    </Form.Item>
                  </Col>

                  <Col md={{ span: 12 }} span={24}>
                    <Form.Item label="Price" name={[field.name, 'price']}>
                      <InputNumber
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        min={0}
                        style={{ width: '100%' }}
                      />
                    </Form.Item>
                  </Col>
                </Row>

                <Row>
                  <Col span={24}>
                    <Form.Item label="Description" name={[field.name, 'description']}>
                      <Input.TextArea rows={3} />
                    </Form.Item>
                  </Col>
                </Row>

                <Button
                  type="dashed"
                  danger
                  onClick={() => {
                    remove(field.name);
                  }}
                  block
                  size={'large'}
                  icon={<MinusCircleOutlined />}
                  disabled={fields.length === 1}
                >
                  Remove
                </Button>
              </Col>
            ))}

            <Button
              type="dashed"
              onClick={() => {
                add();
              }}
              block
              size={'large'}
              icon={<PlusCircleOutlined />}
              style={{ margin: 0, marginTop: '1rem' }}
            >
              Add
            </Button>
          </Row>
        )}
      </Form.List>
    </div>
  );
};

export default FormHardware;
