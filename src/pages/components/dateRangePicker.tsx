import type { Dayjs } from 'dayjs';
import type { Dispatch, SetStateAction } from 'react';

import { css } from '@emotion/react';
import { DatePicker } from 'antd';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import React from 'react';

const { RangePicker } = DatePicker;

dayjs.extend(utc);

const rangePresets: {
  label: string;
  value: [Dayjs, Dayjs];
}[] = [
  { label: 'Today', value: [dayjs().startOf('date'), dayjs().endOf('date')] },
  { label: 'Yesterday', value: [dayjs().add(-1, 'd').startOf('date'), dayjs().endOf('date')] },
  { label: 'Last 7 days', value: [dayjs().add(-7, 'd').startOf('date'), dayjs().endOf('date')] },
  { label: 'Last 30 days', value: [dayjs().add(-30, 'd').startOf('date'), dayjs().endOf('date')] },
  { label: 'This month', value: [dayjs().startOf('M').startOf('date'), dayjs().endOf('M').endOf('date')] },
  { label: 'This year', value: [dayjs().startOf('y').startOf('date'), dayjs().endOf('y').endOf('date')] },
  {
    label: 'Last year',
    value: [dayjs().add(-1, 'y').startOf('y').startOf('date'), dayjs().add(-1, 'y').endOf('y').endOf('date')],
  },
];

interface Props<T> {
  setParams: Dispatch<SetStateAction<T>>;
}

const DateRangePicker = <T extends object = object>({ setParams }: Props<T>) => {
  const onRangeChange = (dates: null | (Dayjs | null)[], dateStrings: string[]) => {
    setParams((prev: T) => ({
      ...prev,
      from: dates ? dates[0]?.utc().format() : undefined,
      to: dates ? dates[1]?.utc().format() : undefined,
    }));
  };

  return <RangePicker size="large" presets={rangePresets} onChange={onRangeChange} style={{ minWidth: 160 }} />;
};

export default DateRangePicker;
