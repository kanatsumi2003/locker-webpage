import type { IAddress, ILocationParams } from '@/interface/location/location';
import type { SizeType } from 'antd/es/config-provider/SizeContext';
import type { Dispatch, SetStateAction } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form } from 'antd';
import { useState } from 'react';

import CustomSelect from '@/components/input/customSelect';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';
import { useAddressesQuery } from '@/services/addressService';

const LocationFilter = <T extends ILocationParams>({
  setParams,
  size,
}: {
  setParams: Dispatch<SetStateAction<T>>;
  size: SizeType;
}) => {
  const [form] = Form.useForm();

  // STATE
  const [parentCodeDistrict, setParentCodeDistrict] = useState<string | undefined>('');
  const [parentCodeWard, setParentCodeWard] = useState<string | undefined>('');

  // QUERY
  const { data: dataProvince, isLoading: isLoadingProvince } = useAddressesQuery();
  const { data: dataDistrict, isLoading: isLoadingDistrict } = useAddressesQuery(
    {
      parentCode: parentCodeDistrict,
    },
    {
      skip: !parentCodeDistrict,
    },
  );
  const { data: dataWard, isLoading: isLoadingWard } = useAddressesQuery(
    {
      parentCode: parentCodeWard,
    },
    {
      skip: !parentCodeWard,
    },
  );

  const onValuesChange = (changedValues: T) => {
    setParams((prev: T) => ({ ...prev, ...changedValues }));
  };

  const handleResetFilter = () => {
    form?.resetFields();
    setParams((prev: T) => ({
      ...prev,
      provinceCode: undefined,
      districtCode: undefined,
      wardCode: undefined,
    }));

    setParentCodeDistrict(undefined);
    setParentCodeWard(undefined);
  };

  return (
    <Form
      form={form}
      name="filter-location"
      className="filter-group"
      style={{ marginTop: 0 }}
      layout="vertical"
      onValuesChange={onValuesChange}
    >
      <ScrollWrapper>
        <Form.Item label="Tỉnh/Thành phố" name="provinceCode">
          <CustomSelect<T>
            setParams={setParams}
            size={size}
            name="provinceCode"
            placeholder="Tỉnh"
            loading={isLoadingProvince}
            options={dataProvince?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            onClear={() => {
              setParentCodeWard(undefined);
              form?.resetFields(['districtCode', 'wardCode']);
            }}
            onChange={(value: string) => {
              form?.setFieldsValue({ provinceCode: value, districtCode: undefined });
              setParentCodeDistrict(value);
            }}
          />
        </Form.Item>

        <Form.Item label="Quận/Huyện" name={'districtCode'}>
          <CustomSelect<T>
            setParams={setParams}
            size={size}
            name="districtCode"
            placeholder="Quận/huyện"
            loading={isLoadingDistrict}
            options={dataDistrict?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            onChange={(value: string) => {
              form?.setFieldsValue({
                provinceCode: parentCodeDistrict,
                wardCode: undefined,
                districtCode: value,
              });
              setParentCodeWard(value);
            }}
            disabled={!parentCodeDistrict}
          />
        </Form.Item>

        <Form.Item label="Phường/Xã" name="wardCode">
          <CustomSelect<T>
            setParams={setParams}
            name="wardCode"
            size={size}
            placeholder="Phường/xã"
            loading={isLoadingWard}
            options={dataWard?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            disabled={!parentCodeWard}
          />
        </Form.Item>

        {/* BUTTON RESET FILTER */}
        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="middle" className="reset-btn">
          Reset
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default LocationFilter;
