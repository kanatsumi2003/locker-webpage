import { PlusOutlined } from '@ant-design/icons';
import { Button, Form, Space } from 'antd';
import { useDispatch } from 'react-redux';

import { openDrawer } from '@/stores/layout.store';

import TableStore from './tableStore';

const StorePage = () => {
  const dispatch = useDispatch();

  const [form] = Form.useForm();

  // ACTIVITY ON CLICK BUTTON
  const handleCreateStore = () => {
    dispatch(openDrawer({ title: 'Create Store' }));
  };

  return (
    <>
      <Space style={{ margin: '1rem 0' }}>
        <Button type="primary" onClick={handleCreateStore}>
          <PlusOutlined />
        </Button>
      </Space>

      <TableStore />
    </>
  );
};

export default StorePage;
