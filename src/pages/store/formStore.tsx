import type { OptionType } from '@/interface';
import type { IAddress } from '@/interface/location/location';
import type { ICreateStoreParams, IStoreItem } from '@/interface/store/store';
import type { InputRef, UploadFile } from 'antd';

import { Button, Col, Form, Input, Row, Select } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Toast } from '@/components/toast/toast';
import DraggerUpload from '@/components/upload/draggerUpload';
import { useAddressesQuery } from '@/services/addressService';
import { useCreateStoreMutation, useUpdateStoreMutation } from '@/services/storeService';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';
import { toLowerCaseNonAccentVietnamese } from '@/utils/formatSearch';
import { getFile } from '@/utils/getFile';
import { required } from '@/utils/validate';

const FormStore = () => {
  const [form] = Form.useForm();
  const initRef = useRef<InputRef>(null);
  const dispatch = useDispatch();

  // REDUX STATE
  const selectedRecord: IStoreItem[] = useSelector(state => state.table.selectedRecord);
  const { visible } = useSelector(state => state.drawer);

  // STATE
  const [parentCodeDistrict, setParentCodeDistrict] = useState<string>('');
  const [parentCodeWard, setParentCodeWard] = useState<string>('');
  const [fileMap, setFileMap] = useState<Map<number, UploadFile[]>>(
    selectedRecord[0]
      ? new Map<number, UploadFile[]>().set(0, [
          { uid: '0', url: selectedRecord[0]?.image, name: selectedRecord[0]?.image },
        ])
      : new Map(),
  );
  const isUpdate = selectedRecord.length;

  // FOCUS ON FIRST FORM FIELD
  useEffect(() => {
    const timeout = setTimeout(() => {
      initRef?.current?.focus();
      setParentCodeDistrict(form?.getFieldValue('location')?.provinceCode || '');
      setParentCodeWard(form?.getFieldValue('location')?.districtCode || '');
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [visible]);

  useEffect(() => {
    isUpdate
      ? form.setFieldsValue({
          ...selectedRecord[0],
          location: {
            provinceCode: selectedRecord[0]?.location?.province?.code,
            districtCode: selectedRecord[0]?.location?.district?.code,
            wardCode: selectedRecord[0]?.location?.ward?.code,
            address: selectedRecord[0]?.location?.address,
          },
        })
      : form.resetFields();
  }, [selectedRecord]);

  // QUERY
  const { data: dataProvince, isLoading: isLoadingProvince } = useAddressesQuery();
  const { data: dataDistrict, isLoading: isLoadingDistrict } = useAddressesQuery(
    {
      parentCode: parentCodeDistrict,
    },
    {
      skip: !parentCodeDistrict,
    },
  );
  const { data: dataWard, isLoading: isLoadingWard } = useAddressesQuery(
    {
      parentCode: parentCodeWard,
    },
    {
      skip: !parentCodeWard,
    },
  );

  // MUTATION
  const [createStore, { isSuccess, isError, error }] = useCreateStoreMutation();
  const [updateStore, { isSuccess: isSuccessUpdate, isError: isErrorUpdate, error: errorUpdate }] =
    useUpdateStoreMutation();

  const onFinish = (values: ICreateStoreParams) => {
    isUpdate
      ? updateStore({
          id: selectedRecord[0].id,
          ...values,
        })
      : createStore(values);
  };

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(
        () => Toast({ type: 'error', message: 'Error', description: error?.message || 'Create Store Failed!' }),
        100,
      );
    }

    if (isErrorUpdate) {
      timeout = setTimeout(() => {
        Toast({
          type: 'error',
          message: 'Error',
          description: errorUpdate?.message || 'Update Store Failed!',
        });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Create Store Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
    }

    if (isSuccessUpdate) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Update Service Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
      dispatch(refreshRowKeys());
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, isSuccessUpdate, isErrorUpdate]);

  return (
    <Form form={form} name="store" layout="vertical" onFinish={onFinish}>
      <Row gutter={[12, 12]}>
        <Col md={{ span: 16 }} span={24}>
          <Form.Item label="Name" name="name" rules={[required('Name')]}>
            <Input />
          </Form.Item>
        </Col>

        <Col md={{ span: 8 }} span={24}>
          <Form.Item label="Contact Phone" name="contactPhone" rules={[required('Contact phone')]}>
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={[12, 12]}>
        <Col md={{ span: 8 }} span={24}>
          <Form.Item label={'Province'} name={['location', 'provinceCode']} rules={[required('Province')]}>
            <Select
              options={dataProvince?.map((item: IAddress) => {
                return {
                  label: item.name,
                  value: item.code,
                };
              })}
              loading={isLoadingProvince}
              allowClear
              onClear={() => {
                setParentCodeWard('');
              }}
              showSearch
              filterOption={(input: string, option: OptionType | undefined) =>
                toLowerCaseNonAccentVietnamese(option?.label).indexOf(toLowerCaseNonAccentVietnamese(input)) >= 0
              }
              onChange={(value: string) => {
                form.setFieldValue('location', { provinceCode: value, districtCode: '' });
                setParentCodeDistrict(value);
              }}
            />
          </Form.Item>
        </Col>

        <Col md={{ span: 8 }} span={24}>
          <Form.Item label={'District'} name={['location', 'districtCode']} rules={[required('District')]}>
            <Select
              disabled={!parentCodeDistrict}
              options={dataDistrict?.map((item: IAddress) => {
                return {
                  label: item.name,
                  value: item.code,
                };
              })}
              loading={isLoadingDistrict}
              allowClear
              showSearch
              filterOption={(input: string, option: OptionType | undefined) =>
                toLowerCaseNonAccentVietnamese(option?.label).indexOf(toLowerCaseNonAccentVietnamese(input)) >= 0
              }
              onChange={(value: string) => {
                form.setFieldValue('location', {
                  provinceCode: parentCodeDistrict,
                  wardCode: '',
                  districtCode: value,
                });
                setParentCodeWard(value);
              }}
            />
          </Form.Item>
        </Col>

        <Col md={{ span: 8 }} span={24}>
          <Form.Item label={'Ward'} name={['location', 'wardCode']} rules={[required('Ward')]}>
            <Select
              disabled={!parentCodeWard}
              options={dataWard?.map((item: IAddress) => {
                return {
                  label: item.name,
                  value: item.code,
                };
              })}
              loading={isLoadingWard}
              allowClear
              showSearch
              filterOption={(input: string, option: OptionType | undefined) =>
                toLowerCaseNonAccentVietnamese(option?.label).indexOf(toLowerCaseNonAccentVietnamese(input)) >= 0
              }
            />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item label={'Address'} name={['location', 'address']} rules={[required('Address')]}>
        <Input />
      </Form.Item>

      <Form.Item label="Image" name={'image'} rules={[required('Image')]} getValueFromEvent={getFile}>
        <DraggerUpload
          multiple={false}
          fileMap={fileMap}
          setFileMap={setFileMap}
          fieldKey={0}
          name="image"
          form={form}
        />
      </Form.Item>

      <Button type="primary" htmlType="submit" form="store" style={{ float: 'right' }}>
        {isUpdate ? 'Update' : 'Create'}
      </Button>
    </Form>
  );
};

export default FormStore;
