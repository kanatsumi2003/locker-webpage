import type { IAddress } from '@/interface/location/location';
import type { IStoreParams } from '@/interface/store/store';
import type { Dispatch, SetStateAction } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form, Select } from 'antd';
import React, { useState } from 'react';

import CustomSearch from '@/components/input/customSearch';
import CustomSelect from '@/components/input/customSelect';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';
import { STORE_STATUS } from '@/interface/store/store';
import { useAddressesQuery } from '@/services/addressService';
import { dropdownRenderStoreStatus } from '@/utils/dropdownRender';

const { Option } = Select;

interface Props {
  params: IStoreParams;
  setParams: Dispatch<SetStateAction<IStoreParams>>;
}

const FilterStore = ({ setParams, params }: Props) => {
  const [form] = Form.useForm();

  // STATE
  const [parentCodeDistrict, setParentCodeDistrict] = useState<string | undefined>('');
  const [parentCodeWard, setParentCodeWard] = useState<string | undefined>('');

  // QUERY
  const { data: dataProvince, isLoading: isLoadingProvince } = useAddressesQuery();
  const { data: dataDistrict, isLoading: isLoadingDistrict } = useAddressesQuery(
    {
      parentCode: parentCodeDistrict,
    },
    {
      skip: !parentCodeDistrict,
    },
  );
  const { data: dataWard, isLoading: isLoadingWard } = useAddressesQuery(
    {
      parentCode: parentCodeWard,
    },
    {
      skip: !parentCodeWard,
    },
  );

  const handleResetFilter = () => {
    form.resetFields();
    setParams({ pageNumber: 1, pageSize: 10 });
  };

  const onValuesChange = (changedValues: IStoreParams) => {
    setParams((prev: IStoreParams) => ({ ...prev, ...changedValues }));
  };

  return (
    <Form form={form} name="filter-store" className="filter-group" layout="vertical" onValuesChange={onValuesChange}>
      {/* SEARCH STORE */}
      <Form.Item label="Search" name="search">
        <CustomSearch<IStoreParams> placeholder="Search..." query="search" setParams={setParams} />
      </Form.Item>

      <ScrollWrapper>
        {/* FILTER STORE BY STORE_STATUS */}
        <Form.Item label="Status" name="status">
          <CustomSelect<IStoreParams> setParams={setParams} name="status" placeholder="Status">
            {Object.values(STORE_STATUS).map((status: STORE_STATUS) => (
              <Option value={status} key={status}>
                {dropdownRenderStoreStatus(status)}
              </Option>
            ))}
          </CustomSelect>
        </Form.Item>

        {/* FILTER STORE BY PROVINCE */}
        <Form.Item label="Province" name={'provinceCode'}>
          <CustomSelect<IStoreParams>
            setParams={setParams}
            name="provinceCode"
            placeholder="Province"
            loading={isLoadingProvince}
            options={dataProvince?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            onClear={() => {
              setParentCodeWard(undefined);
              form?.resetFields(['districtCode', 'wardCode']);
            }}
            onChange={(value: string) => {
              form?.setFieldsValue({ provinceCode: value, districtCode: undefined });
              setParentCodeDistrict(value);
            }}
          />
        </Form.Item>

        {/* FILTER STORE BY DISTRICT */}
        <Form.Item label="District" name={'districtCode'}>
          <CustomSelect<IStoreParams>
            setParams={setParams}
            name="districtCode"
            placeholder="District"
            loading={isLoadingDistrict}
            options={dataDistrict?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            onChange={(value: string) => {
              form?.setFieldsValue({
                provinceCode: parentCodeDistrict,
                wardCode: undefined,
                districtCode: value,
              });
              setParentCodeWard(value);
            }}
            disabled={!parentCodeDistrict}
          />
        </Form.Item>

        {/* FILTER STORE BY WARD */}
        <Form.Item label="Ward" name={'wardCode'}>
          <CustomSelect<IStoreParams>
            setParams={setParams}
            name="wardCode"
            placeholder="Ward"
            loading={isLoadingWard}
            options={dataWard?.map((item: IAddress) => {
              return {
                label: item.name,
                value: item.code,
              };
            })}
            disabled={!parentCodeWard}
          />
        </Form.Item>

        {/* BUTTON RESET FILTER */}
        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large" className="reset-btn">
          Reset Filters
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default FilterStore;
