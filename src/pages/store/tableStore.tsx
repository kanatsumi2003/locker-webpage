import type { ILocation } from '@/interface/location/location';
import type { IStoreItem, IStoreParams, STORE_STATUS } from '@/interface/store/store';
import type { ColumnsType } from 'antd/es/table';
import type { FC } from 'react';

import { Button, Image, Space, Tooltip } from 'antd';
import { TableProps } from 'antd/es/table';
import dayjs from 'dayjs';
import React, { useState } from 'react';
import { useNavigate } from 'react-router';

import StoreImgPlaceHolder from '@/assets/placeholder/placeholder-store.png';
import TableCustom from '@/components/core/table';
import CustomPagination from '@/components/pagination/customPagination';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { useStoresQuery } from '@/services/storeService';
import { dropdownRenderStoreStatus } from '@/utils/dropdownRender';

import FilterStore from './filterStore';

const TableStore: FC = () => {
  const navigate = useNavigate();

  // STATE
  const [params, setParams] = useState<IStoreParams>({ pageNumber: 1, pageSize: 20 });

  // QUERY
  const { data, isLoading } = useStoresQuery(params);

  // TABLE COLUMNS
  const tableColumns: ColumnsType<IStoreItem> = [
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      align: 'center',
      width: 150,
      render: (image: string) => <Image src={image || StoreImgPlaceHolder} alt={image} />,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      ellipsis: true,
      render: (name: string) => (
        <Tooltip placement="bottom" title={name}>
          {name}
        </Tooltip>
      ),
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status: STORE_STATUS) => dropdownRenderStoreStatus(status),
    },
    {
      title: 'Location',
      key: 'location',
      children: [
        {
          title: 'Province',
          dataIndex: 'location',
          key: 'province',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.province?.name}>
              {location?.province?.name}
            </Tooltip>
          ),
        },
        {
          title: 'District',
          dataIndex: 'location',
          key: 'district',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.district?.name}>
              {location?.district?.name}
            </Tooltip>
          ),
        },
        {
          title: 'Ward',
          dataIndex: 'location',
          key: 'ward',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.ward?.name}>
              {location?.ward?.name}
            </Tooltip>
          ),
        },
        {
          title: 'Address',
          dataIndex: 'location',
          key: 'address',
          ellipsis: true,
          render: (location: ILocation) => (
            <Tooltip placement="bottom" title={location?.address}>
              {location?.address}
            </Tooltip>
          ),
        },
      ],
    },
    {
      title: 'Created At',
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      sorter: {
        compare: () => {
          setParams({ ...params, sortColumn: 'createdAt', sortDir: params.sortDir === 'Asc' ? 'Desc' : 'Asc' });

          return 1;
        },
      },
      render: (createdAt: string) => dayjs(createdAt).format(FULL_TIME_FORMAT),
    },
    {
      title: 'Updated At',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      align: 'center',
      sorter: {
        compare: () => {
          setParams({ ...params, sortColumn: 'updatedAt', sortDir: params.sortDir === 'Asc' ? 'Desc' : 'Asc' });

          return 1;
        },
      },
      render: (updatedAt: string) => dayjs(updatedAt).format(FULL_TIME_FORMAT),
    },
    {
      title: 'Action',
      key: 'action',
      align: 'center',
      fixed: 'right',
      render: (_: unknown, record: IStoreItem) => (
        <Button onClick={() => navigate(`/stores/${record.id}`)}>Details</Button>
      ),
    },
  ];

  return (
    <Space direction="vertical">
      <FilterStore setParams={setParams} params={params} />

      <TableCustom
        bordered
        loading={isLoading}
        rowKey={(record: IStoreItem) => record.id}
        dataSource={data?.items}
        columns={tableColumns}
      />

      <CustomPagination<IStoreParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setParams}
      />
    </Space>
  );
};

export default TableStore;
