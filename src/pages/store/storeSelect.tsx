import type { IStoreItem, IStoreParams, STORE_STATUS } from '@/interface/store/store';
import type { SelectProps } from 'antd';
import type { FC } from 'react';

import React, { useState } from 'react';

import InfiniteSelect from '@/components/infiniteSelect/infiniteSelect';
import { useStoresQuery } from '@/services/storeService';

interface Props extends SelectProps {
  storeStatus?: STORE_STATUS;
}

const StoreSelect: FC<Props> = ({ storeStatus, ...rest }) => {
  const [storeParams, setStoreParams] = useState<IStoreParams>({ pageNumber: 1, pageSize: 10, status: storeStatus });

  // QUERY
  const { data } = useStoresQuery(storeParams);

  const onLoad = (params: any) => {
    setStoreParams(params);
  };

  const dataSelect = data?.items?.map((item: IStoreItem) => ({
    label: item?.name,
    value: item?.id,
  }));

  return <InfiniteSelect {...rest} onLoad={onLoad} data={dataSelect} filter={{ status: storeStatus }} />;
};

export default StoreSelect;
