import type { MyTabsOption } from '@/components/business/tabs';

import { Descriptions, Divider, Select, Skeleton, Space, Typography } from 'antd';
import { useParams } from 'react-router-dom';

import { BaseTabs } from '@/components/business/tabs';
import { NotFoundCustom } from '@/components/notFound/notFoundCustom';
import { STATUS_CODE } from '@/constants/common';
import { useStoreQuery } from '@/services/storeService';

import TableLocker from '../locker/tableLocker';
import TableOrder from '../lockerDetail/order/tableOrder';
import TableStaff from '../staff/tableStaff';
import StoreDashboard from './dashboard';
import Detail from './detail';

const StoreDetailPage = () => {
  const { id } = useParams();
  // QUERY
  const { data, isLoading, error: errorGetStoreDetail } = useStoreQuery({ id: Number(id) }, { skip: !id });

  const options: MyTabsOption[] = [
    { label: 'Staff', tabKey: '1', value: 1, children: <TableStaff storeId={Number(id)} /> },
    {
      label: 'Locker',
      tabKey: '2',
      value: 2,
      children: <TableLocker storeId={Number(id)} />,
    },
    {
      label: 'Order',
      tabKey: '3',
      value: 3,
      children: <TableOrder storeId={Number(id)} />,
    },
  ];

  //Return not found
  if (errorGetStoreDetail?.code === STATUS_CODE.BAD_REQUEST || !data) {
    return <NotFoundCustom loading={isLoading} />;
  }

  return (
    <Skeleton loading={isLoading} active>
      <Space direction="vertical" style={{ marginBottom: '1rem' }} size={12}>
        <Detail store={data} />
        <Divider orientationMargin={0} orientation="left">
          THỐNG KÊ
        </Divider>
        <StoreDashboard storeId={data.id} />
        <BaseTabs options={options} />
      </Space>
    </Skeleton>
  );
};

export default StoreDetailPage;
