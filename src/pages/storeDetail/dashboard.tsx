import type { IRangePicker } from '@/interface/layout/index.interface';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Col, Form, Row } from 'antd';
import { useState } from 'react';

import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';

import DateRangePicker from '../components/dateRangePicker';
import DashboardLockerLocation from '../dashboard/dashboardLockerLocation';
import DashboardLockerStatus from '../dashboard/dashboardLockers';
import DashboardRevenue from '../dashboard/dashboardRevenue';
import DashboardStatistic from '../dashboard/dashboardStatistic';
import StoreStatisticCount from './statisticCount';

const StoreDashboard = ({ storeId }: { storeId: number }) => {
  const [rangePicker, setRangePicker] = useState<IRangePicker>({});

  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setRangePicker({
      from: undefined,
      to: undefined,
    });
  };

  return (
    <Row gutter={[12, 12]}>
      <Col span={24}>
        <Form form={form} className="filter-group" layout="vertical">
          <ScrollWrapper>
            <Form.Item name={['from', 'to']}>
              <DateRangePicker setParams={setRangePicker} />
            </Form.Item>

            <Form.Item>
              <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large">
                Reset
              </Button>
            </Form.Item>
          </ScrollWrapper>
        </Form>
      </Col>
      <Col xl={10} span={24}>
        <DashboardStatistic title="Thống kê đơn hàng" storeId={storeId} rangePicker={rangePicker} />
      </Col>
      <Col xl={14} span={24}>
        <Row gutter={[12, 12]} style={{ height: '100%' }}>
          <Col span={24}>
            <StoreStatisticCount storeId={storeId} rangePicker={rangePicker} />
          </Col>
          <Col span={24}>
            <DashboardRevenue title="Báo cáo doanh thu" storeId={storeId} />
          </Col>
        </Row>
      </Col>
      <Col xl={10} span={24}>
        <DashboardLockerStatus title={'Trạng thái locker'} storeId={storeId} />
      </Col>
      <Col xl={14} span={24}>
        <DashboardLockerLocation title={'Vị trí Locker'} storeId={storeId} />
      </Col>
    </Row>
  );
};

export default StoreDashboard;
