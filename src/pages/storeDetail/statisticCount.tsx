import type { IRangePicker } from '@/interface/layout/index.interface';

import { DatabaseOutlined, TagOutlined, UserSwitchOutlined } from '@ant-design/icons';
import { Col, Row } from 'antd';

import StatisticItem from '@/components/statistic/statisticItem';
import { useDashboardOverviewQuery } from '@/services/dashboardService';

const StoreStatisticCount = ({ storeId, rangePicker }: { storeId: number; rangePicker?: IRangePicker }) => {
  const { data } = useDashboardOverviewQuery({ ...rangePicker, storeId });

  return (
    <Row gutter={[12, 12]} style={{ height: '100%' }}>
      <Col xl={12} span={24}>
        <StatisticItem title={'Nhân viên'} value={data?.staffCount} icon={<UserSwitchOutlined />} color="#597ef7" />
      </Col>
      <Col xl={12} span={24}>
        <StatisticItem title={'Locker'} value={data?.lockerCount} icon={<DatabaseOutlined />} color="#ffc53d" />
      </Col>
    </Row>
  );
};

export default StoreStatisticCount;
