import type { IStoreItem } from '@/interface/store/store';

import { EditOutlined } from '@ant-design/icons';
import { Button, Card, Col, Descriptions, Image, Modal, Row, Select } from 'antd';
import Title from 'antd/es/typography/Title';
import dayjs from 'dayjs';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { Toast } from '@/components/toast/toast';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { STORE_STATUS } from '@/interface/store/store';
import { useUpdateStoreStatusMutation } from '@/services/storeService';
import { openDrawer } from '@/stores/layout.store';
import { setSelectedRecord } from '@/stores/table.store';
import { dropdownRenderStoreStatus } from '@/utils/dropdownRender';

const { Item } = Descriptions;
const { Option } = Select;
const { confirm } = Modal;

const Detail = ({ store }: { store: IStoreItem }) => {
  const { id } = useParams();
  const dispatch = useDispatch();
  // MUTATION
  const [updateStoreStatus, { isSuccess, isError, error }] = useUpdateStoreStatusMutation();

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: error?.message || 'Update Store Status Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Change Store Status Successfully!' });
      }, 100);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, store]);

  const handleChangeStatusStore = (status: STORE_STATUS) => {
    confirm({
      title: 'Confirm',
      content: 'Are you sure to update store status?',
      onOk: () => {
        updateStoreStatus({ id: Number(id), status });
      },
    });
  };

  const handleUpdateStore = () => {
    dispatch(setSelectedRecord([store]));
    dispatch(openDrawer({ title: 'Update Store' }));
  };

  return (
    <Card
      title={
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <Title level={3} style={{ margin: 0 }}>
            Store Detail
          </Title>
          <Button icon={<EditOutlined />} onClick={handleUpdateStore} type="primary" />
        </div>
      }
    >
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <Descriptions bordered column={{ md: 3, sm: 1, xs: 1 }} layout="vertical">
            <Item label="Hình ảnh" span={3}>
              <Image src={store?.image} alt={store?.image} style={{ width: 300, height: 300, margin: 'auto' }} />
            </Item>
            <Item label="Name">{store.name}</Item>
            <Item label="Contact Phone">{store.contactPhone}</Item>
            <Item label="Status">
              <Select
                defaultValue={store.status}
                value={store.status}
                onChange={(value: STORE_STATUS) => handleChangeStatusStore(value)}
              >
                {Object.values(STORE_STATUS).map((status: STORE_STATUS) => (
                  <Option value={status} key={status}>
                    {dropdownRenderStoreStatus(status)}
                  </Option>
                ))}
              </Select>
            </Item>
            <Item label="Province">{store.location?.province?.name}</Item>
            <Item label="District">{store.location?.district?.name}</Item>
            <Item label="Ward">{store.location?.ward?.name}</Item>
            <Item label="Address" span={3}>
              {store.location?.address}
            </Item>
            <Item label="Created At">{dayjs(store.createdAt).format(FULL_TIME_FORMAT)}</Item>
            <Item label="Updated At">{dayjs(store.updatedAt).format(FULL_TIME_FORMAT)}</Item>
          </Descriptions>
        </Col>
      </Row>
    </Card>
  );
};

export default Detail;
