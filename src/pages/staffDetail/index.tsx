import type { MyTabsOption } from '@/components/business/tabs';

import { EditOutlined, ShopOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Card, Col, Descriptions, Divider, Image, Modal, Row, Select, Skeleton, Space, Typography } from 'antd';
import dayjs from 'dayjs';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useParams } from 'react-router-dom';

import { BaseTabs } from '@/components/business/tabs';
import { Toast } from '@/components/toast/toast';
import { FULL_TIME_FORMAT, STATUS_CODE } from '@/constants/common';
import { STAFF_STATUS } from '@/interface/staff/staff';
import { STORE_STATUS } from '@/interface/store/store';
import { useStaffByIdQuery, useUpdateStaffStatusMutation } from '@/services/staffService';
import { openDrawer } from '@/stores/layout.store';
import { setSelectedRecord } from '@/stores/table.store';
import { dropdownRenderStaffStatus, dropdownRenderStoreStatus } from '@/utils/dropdownRender';

import NotFoundPage from '../404';
import TableLocker from '../locker/tableLocker';
import TableOrder from '../lockerDetail/order/tableOrder';

const { Item } = Descriptions;
const { Option } = Select;
const { confirm } = Modal;
const { Title, Paragraph } = Typography;

const StaffDetailPage = () => {
  const { id } = useParams();

  const dispatch = useDispatch();

  // QUERY
  const { data, isLoading, error: errorGetStaffDetail } = useStaffByIdQuery({ id: Number(id) }, { skip: !id });

  // MUTATION
  const [updateStaffStatus, { isSuccess, isError, error }] = useUpdateStaffStatusMutation();

  const handleChangeStatusStaff = (status: STAFF_STATUS) => {
    confirm({
      title: 'Confirm',
      content: 'Are you sure to update staff status?',
      onOk: () => {
        updateStaffStatus({ id: Number(id), status });
      },
    });
  };

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: error?.message || 'Update Staff Status Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Change Staff Status Successfully!' });
      }, 100);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError]);

  const handleUpdateStaff = () => {
    dispatch(setSelectedRecord([data]));
    dispatch(openDrawer({ title: 'Update Staff' }));
  };

  const options: MyTabsOption[] = [
    {
      label: 'Locker',
      tabKey: '1',
      value: 1,
      children: <TableLocker staffId={Number(id)} />,
    },
    {
      label: 'Order',
      tabKey: '2',
      value: 2,
      children: <TableOrder staffId={Number(id)} />,
    },
  ];

  return (
    <Skeleton loading={isLoading}>
      {errorGetStaffDetail?.code === STATUS_CODE.BAD_REQUEST ? (
        <NotFoundPage />
      ) : (
        <div>
          <Card
            title={
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Title level={3} style={{ margin: 0 }}>
                  Staff Detail
                </Title>
                <Button icon={<EditOutlined />} onClick={handleUpdateStaff} type="primary" />
              </div>
            }
          >
            <Row gutter={[12, 12]}>
              <Col span={24}>
                <Descriptions
                  title={
                    <Space>
                      <UserOutlined />
                      Staff
                    </Space>
                  }
                  bordered
                  column={{ md: 3, sm: 1, xs: 1 }}
                  layout="vertical"
                >
                  <Item label="Hình ảnh" span={3}>
                    <Image src={data?.avatar} alt={data?.avatar} style={{ width: 300, height: 300, margin: 'auto' }} />
                  </Item>
                  <Item label="Full Name">{data?.fullName || ''}</Item>
                  <Item label="Phone Number">{data?.phoneNumber || ''}</Item>
                  <Item label="Status">
                    <Select
                      defaultValue={data?.status}
                      value={data?.status}
                      onChange={(value: STAFF_STATUS) => handleChangeStatusStaff(value)}
                    >
                      {Object.values(STAFF_STATUS).map((status: STAFF_STATUS) => (
                        <Option value={status} key={status}>
                          {dropdownRenderStaffStatus(status)}
                        </Option>
                      ))}
                    </Select>
                  </Item>
                  <Item label="Description" span={3}>
                    {data?.description || ''}
                  </Item>
                  <Item label="Created At">
                    {data?.createdAt ? dayjs(data?.createdAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="Updated At">
                    {data?.updatedAt ? dayjs(data?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="">{}</Item>
                </Descriptions>

                <Divider />

                <Descriptions
                  title={
                    <Space>
                      <ShopOutlined />
                      Store
                    </Space>
                  }
                  bordered
                  column={{ md: 3, sm: 1, xs: 1 }}
                  layout="vertical"
                >
                  <Item label="Name">{data?.store?.name}</Item>
                  <Item label="Contact Phone">{data?.store?.contactPhone}</Item>
                  <Item label="Status">{data?.store?.status && dropdownRenderStoreStatus(data?.store?.status)}</Item>
                  <Item label="Province">{data?.store?.location?.province?.name}</Item>
                  <Item label="District">{data?.store?.location?.district?.name}</Item>
                  <Item label="Ward">{data?.store?.location?.ward?.name}</Item>
                  <Item label="Address" span={3}>
                    {data?.store?.location?.address}
                  </Item>
                  <Item label="Created At">
                    {data?.createdAt ? dayjs(data?.createdAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="Updated At">
                    {data?.updatedAt ? dayjs(data?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="">{}</Item>
                </Descriptions>
              </Col>
            </Row>
          </Card>

          <BaseTabs options={options} />
        </div>
      )}
    </Skeleton>
  );
};

export default StaffDetailPage;
