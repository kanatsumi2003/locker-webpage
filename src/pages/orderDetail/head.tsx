import type { IOrderDetailItem } from '@/interface/order/order';

import { Button, Card, Col, Row, Space, Typography } from 'antd';
import React from 'react';

const { Text, Link } = Typography;

interface Props {
  data: IOrderDetailItem | undefined;
}

const Head = ({ data }: Props) => {
  return (
    <Row gutter={[12, 12]} style={{ marginBottom: '4px' }}>
      <Col md={{ span: 2 }} span={24}></Col>
      <Col md={{ span: 20 }} span={24}>
        <Card bordered={false}>
          <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
            <Button style={{ maxWidth: '8rem' }}>Notify</Button>
            <Space>
              <div>
                <Text>Order ID:</Text>
                &nbsp;
                <Text copyable>{data?.id}</Text>
              </div>

              <div>
                <Text>Pin Code:</Text>
                &nbsp;
                <Text copyable>{data?.pinCode}</Text>
              </div>
            </Space>
          </div>
        </Card>
      </Col>
    </Row>
  );
};

export default Head;
