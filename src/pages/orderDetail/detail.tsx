import type { IOrderDetailItem, IUpdateOrderParams } from '@/interface/order/order';

import './style.less';

import { ExportOutlined, ImportOutlined } from '@ant-design/icons';
import { Button, Card, Col, Descriptions, Divider, Form, Input, InputNumber, Row, Typography } from 'antd';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Toast } from '@/components/toast/toast';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { FEE_TYPE } from '@/interface/service/service';
import { LocaleFormatter } from '@/locales';
// import { useUpdateOrderMutation } from '@/services/orderService';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
// import { dropdownRender as dropdownRenderFeeType } from '../service/formService';
import { dropdownRenderLockerStatus } from '@/utils/dropdownRender';
import { required } from '@/utils/validate';

const { Text } = Typography;

interface Props {
  data: IOrderDetailItem | undefined;
}

const Detail = ({ data }: Props) => {
  const [form] = Form.useForm();

  const dispatch = useDispatch();
  const rowKeys = useSelector(state => state.table.selectedRowKeys);

  // MUTATION
  // const [updateOrder, { isSuccess, isError, error }] = useUpdateOrderMutation();

  // const onFinish = (values: IUpdateOrderParams) => {
  //   updateOrder({
  //     id: Number(rowKeys[0]),
  //     ...values,
  //   });
  // };

  // useEffect(() => {
  //   let timeout: NodeJS.Timeout;

  //   if (isError) {
  //     timeout = setTimeout(() => {
  //       Toast({ type: 'error', message: 'Error', description: error?.message });
  //     }, 100);
  //   }

  //   if (isSuccess) {
  //     timeout = setTimeout(() => {
  //       Toast({ type: 'success', message: 'Success', description: 'Update Order Successfully!' });
  //       dispatch(closeDrawer());
  //       dispatch(refreshRowKeys());
  //     }, 100);
  //   }
  // }, [isSuccess, isError]);

  // useEffect(() => {
  //   data && form.setFieldsValue(data);
  // }, [data]);

  return (
    <Form
      form={form}
      name="orderDetails"
      layout="vertical"
      // onFinish={onFinish}
    >
      {/* <Row gutter={[12, 12]}>
        <Col md={{ span: 2 }} span={24}></Col>
        <Col md={{ span: 20 }} span={24}>
          <Card bordered={false}>
            <Row>
              <Col span={24}>
                <Row justify="center">
                  <Typography.Title level={4}>{<LocaleFormatter id="app.locker.action.title" />}</Typography.Title>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.order.field.lockerName" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>{data?.locker?.name}</Text>
                  </Col>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.order.field.lockerAddress" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>{data?.locker?.location?.address}</Text>
                  </Col>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.order.field.lockerStatus" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>
                      {data?.locker?.status && dropdownRenderLockerStatus(data?.locker?.status)}
                    </Text>
                  </Col>
                </Row>
              </Col>

              <Divider />
              <Col span={24}>
                <Row justify="center">
                  <Typography.Title level={4}>{<LocaleFormatter id="app.service.action.title" />}</Typography.Title>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.service.field.name" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>{data?.service?.name}</Text>
                  </Col>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.service.field.feeType" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>
                      {data?.service?.feeType && dropdownRenderFeeType(data?.service?.feeType)}
                    </Text>
                  </Col>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.service.field.fee" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>
                      {data?.service?.fee && formatDecimalPrecision(data?.service?.fee)}
                    </Text>
                  </Col>
                </Row>
                <Row style={{ marginBottom: '8px' }}>
                  <Col md={{ span: 6 }} span={24}>
                    <Text>{<LocaleFormatter id="app.service.field.description" />}:</Text>
                  </Col>
                  <Col md={{ span: 18 }} span={24}>
                    <Text style={{ color: '#A7A7A7' }}>{data?.service?.description}</Text>
                  </Col>
                </Row>
              </Col>
            </Row>

            <Row className="custom-row" gutter={[12, 12]} align="middle"></Row>

            <Row className="mb-64" justify="center">
              <Typography.Title level={3} className="mt-16">
                Delivery Information
              </Typography.Title>
              <br />
            </Row>
            <Row>
              <Col span={12}>
                <Row justify="center">
                  <ExportOutlined style={{ fontSize: '24px' }} /> <br />
                </Row>
                <Row justify="center">
                  <Text strong>
                    {' '}
                    <LocaleFormatter id="app.order.field.sender" />{' '}
                  </Text>{' '}
                  <br />
                </Row>
               
                <Row justify="center">
                  <Text copyable>{data?.orderPhone}</Text>
                </Row>
              </Col>
              <Col span={12}>
                <Row justify="center">
                  <ImportOutlined style={{ fontSize: '24px' }} /> <br />
                </Row>
                <Row justify="center">
                  <Text strong>
                    {' '}
                    <LocaleFormatter id="app.order.field.receiver" />{' '}
                  </Text>{' '}
                  <br />
                </Row>
               
                <Row justify="center">
                  <Text copyable>{data?.receivePhone}</Text>
                </Row>
              </Col>
            </Row>
            <Row className="mt-32">
              <Descriptions bordered column={1} style={{ width: '100%' }}>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.sendBox" />}>
                  {data?.sendBoxOrder}
                </Descriptions.Item>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.receiveBox" />}>
                  {data?.receiveBoxOrder}
                </Descriptions.Item>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.receiveTime" />}>
                  {data?.receiveTime && dayjs(data?.receiveTime).format(FULL_TIME_FORMAT)}
                </Descriptions.Item>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.actualReceiveTime" />}>
                  {data?.actualReceiveTime && dayjs(data?.actualReceiveTime).format(FULL_TIME_FORMAT)}
                </Descriptions.Item>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.amount" />}>
                  <Form.Item name="amount" style={{ margin: 'auto' }} initialValue={data?.amount}>
                    <InputNumber
                      formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      style={{ width: '100%' }}
                      min={0}
                      disabled={data?.service?.feeType === FEE_TYPE?.BY_TIME}
                      onChange={value =>
                        data?.service?.feeType === FEE_TYPE?.BY_UNIT &&
                        value &&
                        form.setFieldValue('fee', data?.service?.fee * value)
                      }
                    />
                  </Form.Item>
                </Descriptions.Item>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.fee" />}>
                  <Form.Item name="fee" style={{ margin: 'auto' }} initialValue={data?.fee} rules={[required('Fee')]}>
                    <InputNumber
                      formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      min={0}
                      style={{ width: '100%' }}
                      disabled={data?.service?.feeType === FEE_TYPE?.BY_TIME}
                    />
                  </Form.Item>
                </Descriptions.Item>
                <Descriptions.Item label={<LocaleFormatter id="app.order.field.description" />}>
                  <Form.Item name="description" style={{ margin: 'auto' }} initialValue={data?.description}>
                    <Input.TextArea
                      rows={3}
                      style={{ width: '100%' }}
                      disabled={data?.service?.feeType === FEE_TYPE?.BY_TIME}
                    />
                  </Form.Item>
                </Descriptions.Item>
                
              </Descriptions>
            </Row>
            {data?.service?.feeType !== FEE_TYPE?.BY_TIME && (
              <Row className="mt-32 mb-32" justify="center">
                <Button htmlType="submit" form="orderDetails" type="primary">
                  Update
                </Button>
              </Row>
            )}
          </Card>
        </Col>
        <Col md={{ span: 2 }} span={24}></Col>
      </Row> */}
    </Form>
  );
};

export default Detail;
