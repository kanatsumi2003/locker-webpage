import type { IOrderItem } from '@/interface/order/order';

import './style.less';

import {
  CheckCircleOutlined,
  DatabaseOutlined,
  FileTextOutlined,
  Loading3QuartersOutlined,
  RightOutlined,
} from '@ant-design/icons';
import { Card, Col, Row, Typography } from 'antd';
import React from 'react';

const { Text } = Typography;

interface Props {
  data: IOrderItem | undefined;
}

const TimelineEx = ({ data }: Props) => {
  return (
    <Row gutter={[12, 12]} style={{ marginBottom: '4px' }}>
      <Col md={{ span: 2 }} span={24}></Col>
      <Col md={{ span: 20 }} span={24}>
        <Card bordered={false}>
          <Row gutter={[12, 12]} justify="space-around" align="middle">
            <Col md={{ span: 1 }} span={24}></Col>
            <Col md={{ span: 4 }} span={24}>
              <Row justify="center">
                <FileTextOutlined style={{ fontSize: '24px' }} /> <br />
              </Row>
              <Row justify="center">
                <Text strong>Waiting</Text> <br />
              </Row>
              <Row justify="center">
                <Text className="timestamp">9:14 16-04-2023</Text>
              </Row>
            </Col>
            <Col md={{ span: 2 }} span={24}>
              <Row justify="center">
                <RightOutlined />
              </Row>
            </Col>
            <Col md={{ span: 4 }} span={24}>
              <Row justify="center">
                <Loading3QuartersOutlined style={{ fontSize: '24px' }} /> <br />
              </Row>
              <Row justify="center">
                <Text strong>Processing</Text> <br />
              </Row>
              <Row justify="center">
                <Text className="timestamp">18:14 16-04-2023</Text>
              </Row>
            </Col>
            <Col md={{ span: 2 }} span={24}>
              <Row justify="center">
                <RightOutlined />
              </Row>
            </Col>
            <Col md={{ span: 4 }} span={24}>
              <Row justify="center">
                <DatabaseOutlined style={{ fontSize: '24px' }} /> <br />
              </Row>
              <Row justify="center">
                <Text strong>Delivered</Text> <br />
              </Row>
              <Row justify="center">
                <Text className="timestamp">16:14 17-04-2023</Text>
              </Row>
            </Col>
            <Col md={{ span: 2 }} span={24}>
              <Row justify="center">
                <RightOutlined />
              </Row>
            </Col>
            <Col md={{ span: 4 }} span={24} className="on-stage">
              <Row justify="center">
                <CheckCircleOutlined style={{ fontSize: '24px' }} /> <br />
              </Row>
              <Row justify="center">
                <Text strong>Completed</Text> <br />
              </Row>
              <Row justify="center">
                <Text className="timestamp">9:14 18-04-2023</Text>
              </Row>
            </Col>
            <Col md={{ span: 1 }} span={24}></Col>
          </Row>
        </Card>
      </Col>
      <Col md={{ span: 2 }} span={24}></Col>
    </Row>
  );
};

export default TimelineEx;
