import type { IOrderDetailItem, ITimeLineItem } from '@/interface/order/order';

import './style.less';

import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  CloudServerOutlined,
  DatabaseOutlined,
  FileTextOutlined,
  Loading3QuartersOutlined,
  RightOutlined,
} from '@ant-design/icons';
import { Card, Col, Row, Space, Typography } from 'antd';
import dayjs from 'dayjs';
import React from 'react';

import { ORDER_STATUS } from '@/interface/order/order';

const { Text } = Typography;

interface Props {
  data: IOrderDetailItem | undefined;
}

const Timeline = ({ data }: Props) => {
  return (
    <Row gutter={[12, 12]} style={{ marginBottom: '4px' }}>
      <Col xs={{ span: 2 }} span={24}></Col>
      <Col xs={{ span: 20 }} span={24}>
        <Card bordered={false}>
          {data?.timelines?.map((item: ITimeLineItem, index: number) => (
            <Row
              // key={item?.time}
              gutter={[12, 12]}
              justify="space-around"
              align="middle"
              className={`mb-16 ${index === data?.timelines?.length - 1 && 'on-stage'}`}
            >
              <Col xs={{ span: 11 }} span={24}>
                <Row justify="end">
                  {/* <Text className="be-gray">{dayjs(item?.time)?.format('HH:mm DD/MM/YYYY')}</Text> */}
                </Row>
              </Col>

              <Col xs={{ span: 2 }} span={24}>
                <Row justify="center">
                  <RightOutlined style={{ color: '#A7A7A7' }} />
                </Row>
              </Col>
              <Col xs={{ span: 11 }} span={24}>
                <Row justify="start">
                  <Space>
                    {item?.status === ORDER_STATUS.INITIALIZED && (
                      <>
                        <CloudServerOutlined style={{ fontSize: '24px' }} />
                      </>
                    )}
                    {item?.status === ORDER_STATUS.WAITING && (
                      <>
                        <FileTextOutlined style={{ fontSize: '24px' }} />
                      </>
                    )}
                    {item?.status === ORDER_STATUS.PROCESSING && (
                      <Loading3QuartersOutlined style={{ fontSize: '24px' }} />
                    )}
                    {item?.status === ORDER_STATUS.RETURNED && (
                      <>
                        <DatabaseOutlined style={{ fontSize: '24px' }} />
                      </>
                    )}
                    {item?.status === ORDER_STATUS.COMPLETED && (
                      <>
                        <CheckCircleOutlined style={{ fontSize: '24px' }} />
                      </>
                    )}
                    {item?.status === ORDER_STATUS.CANCELED && (
                      <>
                        <CloseCircleOutlined style={{ fontSize: '24px' }} />
                      </>
                    )}
                    <Text strong>{item?.status}</Text> <br />
                  </Space>
                </Row>
              </Col>
            </Row>
          ))}
        </Card>
      </Col>
      <Col xs={{ span: 2 }} span={24}></Col>
    </Row>
  );
};

export default Timeline;
