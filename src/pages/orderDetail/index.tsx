import type { ITimeLineItem } from '@/interface/order/order';

import {
  CreditCardOutlined,
  EditOutlined,
  InboxOutlined,
  LockOutlined,
  ShopOutlined,
  TableOutlined,
  UserOutlined,
} from '@ant-design/icons';
import {
  Button,
  Card,
  Col,
  Descriptions,
  Divider,
  Image,
  Row,
  Skeleton,
  Space,
  Spin,
  Timeline,
  Typography,
} from 'antd';
import dayjs from 'dayjs';
import { useSelector } from 'react-redux';
import { Link, useParams } from 'react-router-dom';

import PersonPlaceholder from '@/assets/placeholder/person-placeholder.png';
import { FULL_TIME_FORMAT, STATUS_CODE } from '@/constants/common';
import { BOX_STATUS } from '@/interface/box/box';
import { useOrderQuery } from '@/services/orderService';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import {
  dropdownRenderAccountStatus,
  dropdownRenderBoxStatus,
  dropdownRenderLockerStatus,
  dropdownRenderOrderStatus,
  dropdownRenderOrderType,
  dropdownRenderStaffStatus,
  dropdownRenderStoreStatus,
} from '@/utils/dropdownRender';

import NotFoundPage from '../404';
import Detail from './detail';
import Head from './head';

const { Item } = Descriptions;
const { Title } = Typography;
const { Paragraph } = Typography;

const OrderDetailPage = () => {
  const { id } = useParams();
  const rowKeys = useSelector(state => state.table.selectedRowKeys);

  // QUERY
  const { data, isLoading, error: errorGetOrderDetail } = useOrderQuery({ id: Number(id) }, { skip: !id });

  const handleUpdateOrder = () => {};

  return (
    // <Spin spinning={isLoading}>
    //   <Head data={data} />
    //   <Timeline data={data} />
    //   <Detail data={data} />
    // </Spin>
    <Skeleton loading={isLoading} active>
      {errorGetOrderDetail?.code === STATUS_CODE.BAD_REQUEST ? (
        <NotFoundPage />
      ) : (
        <Card
          title={
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
              <Title level={3} style={{ margin: 0 }}>
                Order Detail
              </Title>
            </div>
          }
        >
          <Row gutter={[12, 12]}>
            <Col lg={{ span: 15 }} span={24}>
              <Space direction="vertical" size="large" style={{ width: '100%' }}>
                {/* STORE INFORMATION */}
                <Descriptions
                  title={
                    <Space>
                      <ShopOutlined />
                      Store
                    </Space>
                  }
                  bordered
                  column={{ md: 3, sm: 1 }}
                  layout="vertical"
                >
                  <Item label="Store">
                    <Link to={`/stores/${data?.locker?.store?.id}`}>{data?.locker?.store?.name || ''}</Link>
                  </Item>
                  <Item label="Contact Phone">{data?.locker?.store?.contactPhone || ''}</Item>
                  <Item label="Status">
                    {data?.locker?.store?.status ? dropdownRenderStoreStatus(data?.store?.status) : ''}
                  </Item>
                  <Item label="Province">{data?.locker?.store?.location?.province?.name || ''}</Item>
                  <Item label="District">{data?.locker?.store?.location?.district?.name || ''}</Item>
                  <Item label="Ward">{data?.locker?.store?.location?.ward?.name || ''}</Item>
                  <Item label="Address" span={3}>
                    {data?.locker?.store?.location?.address || ''}
                  </Item>
                  <Item label="Created At">
                    {data?.locker?.store?.createdAt
                      ? dayjs(data?.locker?.store?.createdAt).format(FULL_TIME_FORMAT)
                      : ''}
                  </Item>
                  <Item label="Updated At">
                    {data?.locker?.store?.updatedAt
                      ? dayjs(data?.locker?.store?.updatedAt).format(FULL_TIME_FORMAT)
                      : ''}
                  </Item>
                  <Item label="">{}</Item>
                </Descriptions>

                {/* LOCKER INFORMATION */}
                <Descriptions
                  title={
                    <Space>
                      <TableOutlined />
                      Locker
                    </Space>
                  }
                  bordered
                  column={{ md: 3, sm: 1 }}
                  layout="vertical"
                >
                  <Item label="Locker" span={2}>
                    <Link to={`/lockers/${data?.locker?.id}`}>{data?.locker?.name}</Link>
                  </Item>
                  <Item label="Code">{data?.locker?.code}</Item>
                  <Item label="Mac Address">{data?.locker?.macAddress}</Item>
                  <Item label="IP Address">{data?.locker?.ipAddress}</Item>
                  <Descriptions.Item label="Status">
                    {data?.locker?.status ? dropdownRenderLockerStatus(data?.locker?.status) : ''}
                  </Descriptions.Item>
                  <Item label="Province">{data?.locker?.location?.province?.name}</Item>
                  <Item label="District">{data?.locker?.location?.district?.name}</Item>
                  <Item label="Ward">{data?.locker?.location?.ward?.name}</Item>
                  <Item label="Address" span={3}>
                    {data?.locker?.location?.address}
                  </Item>
                  <Item label="Description" span={3}>
                    {data?.locker?.description}
                  </Item>
                  <Item label="Created At">
                    {data?.createdAt ? dayjs(data?.createdAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="Updated At">
                    {data?.updatedAt ? dayjs(data?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="">{}</Item>
                </Descriptions>

                {/* SENDER INFORMATION */}
                <Space direction="vertical">
                  <Descriptions
                    title={
                      <Space>
                        <UserOutlined />
                        Sender
                      </Space>
                    }
                    bordered
                    column={{ md: 3, sm: 1 }}
                    layout="vertical"
                  >
                    <Item label="Avatar" span={3}>
                      <Image
                        src={data?.sender?.avatar || PersonPlaceholder}
                        alt={data?.sender?.avatar}
                        style={{ maxHeight: 200 }}
                      />
                    </Item>

                    <Item label="Name">{data?.sender?.fullName || ''}</Item>
                    <Item label="Phone Number">{data?.sender?.phoneNumber || ''}</Item>
                    <Item label="Status">
                      {data?.sender?.status ? dropdownRenderAccountStatus(data?.sender?.status) : ''}
                    </Item>

                    <Item label="Description" span={3}>
                      {data?.sender?.description || ''}
                    </Item>
                    <Item label="Created At">
                      {data?.sender?.createdAt ? dayjs(data?.sender?.createdAt).format(FULL_TIME_FORMAT) : ''}
                    </Item>
                    <Item label="Updated At">
                      {data?.sender?.updatedAt ? dayjs(data?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                    </Item>
                    <Item label="">{}</Item>
                  </Descriptions>
                </Space>

                {/* RECEIVER INFORMATION */}
                <Space direction="vertical">
                  <Descriptions
                    title={
                      <Space>
                        <UserOutlined />
                        Receiver
                      </Space>
                    }
                    bordered
                    column={{ md: 3, sm: 1 }}
                    layout="vertical"
                  >
                    <Item label="Avatar" span={3}>
                      <Image
                        src={data?.receiver?.avatar || PersonPlaceholder}
                        alt={data?.receiver?.avatar}
                        style={{ maxHeight: 200 }}
                      />
                    </Item>

                    <Item label="Name">{data?.receiver?.fullName || ''}</Item>
                    <Item label="Phone Number">{data?.receiver?.phoneNumber || ''}</Item>
                    <Item label="Status">
                      {data?.receiver?.status ? dropdownRenderAccountStatus(data?.receiver?.status) : ''}
                    </Item>

                    <Item label="Description" span={3}>
                      {data?.receiver?.description || ''}
                    </Item>
                    <Item label="Created At">
                      {data?.receiver?.createdAt ? dayjs(data?.receiver?.createdAt).format(FULL_TIME_FORMAT) : ''}
                    </Item>
                    <Item label="Updated At">
                      {data?.receiver?.updatedAt ? dayjs(data?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                    </Item>
                    <Item label="">{}</Item>
                  </Descriptions>
                </Space>

                {/* SEND BOX INFORMATION */}
                <Descriptions
                  title={
                    <Space>
                      <LockOutlined />
                      Send Box
                    </Space>
                  }
                  bordered
                  column={{ md: 3, sm: 1 }}
                  layout="vertical"
                >
                  <Item label="Number">{data?.sendBox?.number || ''}</Item>
                  <Item label="Pin">{data?.sendBox?.pinNo || ''}</Item>
                  <Item label="Status">
                    {data?.sendBox?.isAvailable
                      ? dropdownRenderBoxStatus(BOX_STATUS.ACTIVE)
                      : dropdownRenderBoxStatus(BOX_STATUS.INACTIVE)}
                  </Item>
                </Descriptions>

                {/* RECEIVER BOX INFORMATION */}
                <Descriptions
                  title={
                    <Space>
                      <LockOutlined />
                      Receive Box
                    </Space>
                  }
                  bordered
                  column={{ md: 3, sm: 1 }}
                  layout="vertical"
                >
                  <Item label="Number">{data?.receiveBox?.number || ''}</Item>
                  <Item label="Pin">{data?.receiveBox?.pinNo || ''}</Item>
                  <Item label="Status">
                    {data?.receiveBox?.isAvailable
                      ? dropdownRenderBoxStatus(BOX_STATUS.ACTIVE)
                      : dropdownRenderBoxStatus(BOX_STATUS.INACTIVE)}
                  </Item>
                </Descriptions>

                {/* STAFF INFORMATION */}
                <Space direction="vertical">
                  <Descriptions
                    title={
                      <Space>
                        <UserOutlined />
                        Staff
                      </Space>
                    }
                    bordered
                    column={{ md: 3, sm: 1 }}
                    layout="vertical"
                  >
                    <Item label="Avatar" span={3}>
                      <Image
                        src={data?.staff?.avatar || PersonPlaceholder}
                        alt={data?.staff?.avatar}
                        style={{ maxHeight: 200 }}
                      />
                    </Item>

                    <Item label="Full Name">{data?.staff?.fullName || ''}</Item>
                    <Item label="Phone Number">{data?.staff?.phoneNumber || ''}</Item>
                    <Item label="Status">
                      {data?.staff?.status ? dropdownRenderStaffStatus(data?.staff?.status) : ''}
                    </Item>
                    <Item label="Description" span={3}>
                      {data?.staff?.description || ''}
                    </Item>
                    <Item label="Created At">
                      {data?.staff?.createdAt ? dayjs(data?.staff?.createdAt).format(FULL_TIME_FORMAT) : ''}
                    </Item>
                    <Item label="Updated At">
                      {data?.staff?.updatedAt ? dayjs(data?.staff?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                    </Item>
                    <Item label="">{}</Item>
                  </Descriptions>
                </Space>

                {/* ORDER INFORMATION */}
                <Descriptions
                  title={
                    <Space>
                      <InboxOutlined />
                      Order
                    </Space>
                  }
                  bordered
                  column={{ md: 2, sm: 1 }}
                  layout="vertical"
                >
                  <Item label="Type">{data?.type ? dropdownRenderOrderType(data?.type) : ''}</Item>
                  <Item label="Status">{data?.status ? dropdownRenderOrderStatus(data?.status) : ''}</Item>
                  <Item label="Price (VND)">{data?.price ? formatDecimalPrecision(data?.price) : 0}</Item>
                  <Item label="Discount (VND)">{data?.discount ? formatDecimalPrecision(data?.discount) : 0}</Item>
                  <Item label="Extra Count (hours)">
                    {data?.extraCount ? formatDecimalPrecision(data.extraCount) : 0}
                  </Item>
                  <Item label="Extra Fee (VND)">{data?.extraFee ? formatDecimalPrecision(data?.extraFee) : 0}</Item>
                  <Item label="Description" span={2}>
                    {data?.description || ''}
                  </Item>
                  <Item label="Created At">
                    {data?.createdAt ? dayjs(data?.createdAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="Updated At">
                    {data?.updatedAt ? dayjs(data?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                </Descriptions>

                {/* BILL INFORMATION */}
                <Descriptions
                  title={
                    <Space>
                      <CreditCardOutlined />
                      Bill
                    </Space>
                  }
                  bordered
                  column={{ md: 2, sm: 1 }}
                  layout="vertical"
                >
                  <Item label="Amount (VND)">
                    {data?.bill?.amount ? formatDecimalPrecision(data?.bill?.amount) : 0}
                  </Item>
                  <Item label="Method">{data?.bill?.method || ''} </Item>
                  <Item label="Content" span={2}>
                    {data?.bill?.content || ''}
                  </Item>
                  <Item label="Created At">
                    {data?.bill?.createdAt ? dayjs(data?.bill?.createdAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                  <Item label="Updated At">
                    {data?.bill?.updatedAt ? dayjs(data?.bill?.updatedAt).format(FULL_TIME_FORMAT) : ''}
                  </Item>
                </Descriptions>
              </Space>
            </Col>

            <Col lg={{ span: 1 }} span={0}>
              <Divider type="vertical" style={{ height: '100%' }} />
            </Col>

            <Col lg={{ span: 8 }} span={24}>
              <Title level={5}>Timelines</Title>
              <Timeline mode="left" style={{ marginTop: 24 }}>
                {data?.timelines?.map((item: ITimeLineItem) => (
                  <Timeline.Item
                    label={
                      item?.createdAt
                        ? dayjs(item?.createdAt).format(FULL_TIME_FORMAT)
                        : dayjs(item?.updatedAt).format(FULL_TIME_FORMAT)
                    }
                  >
                    {dropdownRenderOrderStatus(item?.status)}
                  </Timeline.Item>
                ))}
              </Timeline>
            </Col>
          </Row>
        </Card>
      )}
    </Skeleton>
  );
};

export default OrderDetailPage;
