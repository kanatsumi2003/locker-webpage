import type { IUpdateOrderParams } from '@/interface/order/order';

import { Button, Col, Form, Input, InputNumber, Row } from 'antd';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Toast } from '@/components/toast/toast';
// import { useUpdateOrderMutation } from '@/services/orderService';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';
import { required } from '@/utils/validate';

const FormUpdateOrder = () => {
  const initRef = useRef<HTMLInputElement>(null);
  const dispatch = useDispatch();
  const rowKeys = useSelector(state => state.table.selectedRowKeys);

  const [form] = Form.useForm();

  // MUTATION
  // const [updateOrder, { isSuccess, isError }] = useUpdateOrderMutation();

  // useEffect(() => {
  //   const timeout = setTimeout(() => {
  //     initRef?.current?.focus();
  //   }, 100);

  //   return () => {
  //     clearTimeout(timeout);
  //   };
  // }, []);

  // const onFinish = (values: IUpdateOrderParams) => {
  //   updateOrder({
  //     id: Number(rowKeys[0]),
  //     ...values,
  //   });
  // };

  // useEffect(() => {
  //   let timeout: NodeJS.Timeout;

  //   if (isError) {
  //     timeout = setTimeout(() => {
  //       Toast({ type: 'error', message: 'Error', description: 'Update Order Failed!' });
  //     }, 100);
  //   }

  //   if (isSuccess) {
  //     timeout = setTimeout(() => {
  //       Toast({ type: 'success', message: 'Success', description: 'Update Order Successfully!' });
  //       dispatch(closeDrawer());
  //       dispatch(refreshRowKeys());
  //     }, 100);
  //   }
  // }, [isSuccess, isError]);

  return (
    <Form
      form={form}
      name="updateOrder"
      layout="vertical"
      // onFinish={onFinish}
    >
      <Row gutter={[12, 12]}>
        <Col md={{ span: 12 }} span={24}>
          <Form.Item label="Amount" name="amount" rules={[required('Amount')]}>
            <InputNumber ref={initRef} style={{ width: '100%' }} />
          </Form.Item>
        </Col>

        <Col md={{ span: 12 }} span={24}>
          <Form.Item label="Fee" name="fee" rules={[required('Fee')]}>
            <InputNumber
              formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              min={0}
              style={{ width: '100%' }}
            />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item label="Description" name="description">
        <Input.TextArea rows={3} />
      </Form.Item>

      <div style={{ float: 'right', marginTop: '1rem' }}>
        <Button htmlType="submit" form="updateOrder">
          Save
        </Button>
      </div>
    </Form>
  );
};

export default FormUpdateOrder;
