import type { FC } from 'react';

import './index.less';

import { Col, Row } from 'antd';
import Lottie from 'lottie-react';

import laundry_animation from '../../assets/lottie/laundry_animation.json';
import FormLogin from './formLogin';

const LoginForm: FC = () => {
  return (
    <Row>
      <Col md={{ span: 12 }} lg={{ span: 14 }} className="lottie-background">
        <Lottie animationData={laundry_animation} loop={true} />
      </Col>
      <Col md={{ span: 12 }} lg={{ span: 10 }} span={24} className="login-wrapper">
        <FormLogin />
      </Col>
    </Row>
  );
};

export default LoginForm;
