import type { ILoginParams } from '@/interface/auth/login';

import { Button, Form, Input, Typography } from 'antd';
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { Toast } from '@/components/toast/toast';
import TokenService from '@/config/tokenService';
import { PATH } from '@/constants/common';
import { useLoginMutation } from '@/services/authService';
import { token } from '@/styles/token';
import { required } from '@/utils/validate';

const { Title } = Typography;

const FormLogin = () => {
  const navigate = useNavigate();
  // MUTATION
  const [login, { isSuccess, isError, data, error }] = useLoginMutation();

  const onFinish = (values: ILoginParams) => {
    login(values);
  };

  useEffect(() => {
    let timeOut: NodeJS.Timeout;

    if (isError) {
      timeOut = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: error?.message || 'Login Failed' });
      }, 100);
    }

    if (isSuccess) {
      timeOut = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Login Successfully!' });
      });

      if (data) {
        TokenService.setAccessToken(data.accessToken);
        TokenService.setRefreshToken(data.refreshToken);
      }

      navigate(PATH.HOME);
    }

    return () => {
      clearTimeout(timeOut);
    };
  }, [isSuccess, isError]);

  return (
    <div className="login-form">
      <Title level={3} style={{ color: token.color.primary, textAlign: 'center' }}>
        Laundry Locker
      </Title>

      <Form<ILoginParams> layout="vertical" onFinish={onFinish} name="login">
        <Form.Item label="Username" name="username" rules={[required('Username')]}>
          <Input size="large" />
        </Form.Item>

        <Form.Item label="Password" name="password" rules={[required('Password')]}>
          <Input.Password size="large" />
        </Form.Item>

        <Button type="primary" size="large" style={{ width: '100%', marginTop: 8 }} htmlType="submit" form="login">
          Login
        </Button>
      </Form>
    </div>
  );
};

export default FormLogin;
