import type { InputRef } from 'antd';

import { Col, Form, Input, InputNumber, Row } from 'antd';
import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';

import { required } from '@/utils/validate';

const FormHardware = () => {
  const initRef = useRef<InputRef>(null);

  const { visible } = useSelector(state => state.drawer);

  useEffect(() => {
    const timeout = setTimeout(() => {
      initRef?.current?.focus();
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [visible]);

  return (
    <Row gutter={[12, 12]} key={new Date().getUTCMilliseconds()}>
      <Col span={24}>
        <Row gutter={[12, 12]}>
          <Col md={{ span: 12 }} span={24}>
            <Form.Item label="Name" name={'name'} rules={[required('Name')]}>
              <Input ref={initRef} />
            </Form.Item>
          </Col>

          <Col md={{ span: 12 }} span={24}>
            <Form.Item label="Code" name={'code'} rules={[required('Code')]}>
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={[12, 12]}>
          <Col md={{ span: 12 }} span={24}>
            <Form.Item label="Brand" name={'brand'} rules={[required('Brand')]}>
              <Input />
            </Form.Item>
          </Col>

          <Col md={{ span: 12 }} span={24}>
            <Form.Item label="Price" name={'price'}>
              <InputNumber
                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                min={0}
                style={{ width: '100%' }}
              />
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <Form.Item label="Description" name={'description'}>
              <Input.TextArea rows={3} />
            </Form.Item>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default FormHardware;
