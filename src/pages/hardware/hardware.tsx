import type { IHardwareItem } from '@/interface/hardware/hardware';

import { Button, Form, Modal } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { Toast } from '@/components/toast/toast';
import { useCreateHardwareMutation, useUpdateHardwareMutation } from '@/services/hardwareService';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';

import FormHardware from './formHardware';

const { confirm } = Modal;

const Hardware = () => {
  const [form] = Form.useForm();
  const { id } = useParams();
  const dispatch = useDispatch();
  const selectedRecord: IHardwareItem[] = useSelector(state => state.table.selectedRecord);

  // MUTATION
  const [createHardware, { isSuccess, isError }] = useCreateHardwareMutation();
  const [updateHardware, { isSuccess: isSuccessUpdate, isError: isErrorUpdate }] = useUpdateHardwareMutation();

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: 'Create Hardware Failed!' });
      }, 100);
    }

    if (isErrorUpdate) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: 'Update Hardware Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Create Hardware Successfully!' });
        dispatch(closeDrawer());
      }, 100);
    }

    if (isSuccessUpdate) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Update Hardware Successfully!' });
        dispatch(closeDrawer());
      }, 100);
      dispatch(refreshRowKeys());
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, isSuccessUpdate, isErrorUpdate]);

  useEffect(() => {
    selectedRecord.length ? form.setFieldsValue(selectedRecord[0]) : form.resetFields();
  }, [selectedRecord]);

  const onFinish = (values: Omit<IHardwareItem, 'id'>) => {
    if (selectedRecord.length) {
      confirm({
        title: 'Confirm',
        content: 'Are you sure to update this hardware?',
        onOk: () => updateHardware({ id: Number(id), hardwareId: selectedRecord[0]?.id, ...values }),
      });
    } else {
      createHardware({
        id: Number(id),
        ...values,
      });
    }
  };

  return (
    <>
      <Form form={form} name="service" layout="vertical" onFinish={onFinish}>
        <FormHardware />
      </Form>
      <div style={{ float: 'right', marginTop: '1rem' }}>
        <Button htmlType="submit" form="service" type="primary">
          Save
        </Button>
      </div>
    </>
  );
};

export default Hardware;
