import type { MyTabsOption } from '@/components/business/tabs';

import { EditOutlined } from '@ant-design/icons';
import { Button, Card, Col, Descriptions, Image, Modal, Row, Select, Skeleton, Space, Spin, Typography } from 'antd';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { BaseTabs } from '@/components/business/tabs';
import { Toast } from '@/components/toast/toast';
import { STATUS_CODE } from '@/constants/common';
import { SERVICE_STATUS } from '@/interface/service/service';
import { useServiceQuery, useUpdateServiceStatusMutation } from '@/services/serviceService';
import { openDrawer } from '@/stores/layout.store';
import { setSelectedRecord } from '@/stores/table.store';
import { dropdownRenderServiceStatus } from '@/utils/dropdownRender';

import NotFoundPage from '../404';
import TableOrder from '../lockerDetail/order/tableOrder';

const { Item } = Descriptions;
const { Option } = Select;
const { confirm } = Modal;
const { Title } = Typography;

const ServiceDetailPage = () => {
  const { id } = useParams();

  const dispatch = useDispatch();

  // QUERY
  const { data, isLoading, error: errorGetServiceDetail } = useServiceQuery({ id: Number(id) }, { skip: !id });

  // MUTATION
  const [updateServiceStatus, { isSuccess, isError, error }] = useUpdateServiceStatusMutation();

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: error?.message || 'Update Service Status Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Change Service Status Successfully!' });
      }, 100);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, data]);

  const handleChangeStatusService = (status: SERVICE_STATUS) => {
    confirm({
      title: 'Confirm',
      content: 'Are you sure to update service status?',
      onOk: () => {
        updateServiceStatus({ id: Number(id), status });
      },
    });
  };

  const handleUpdateService = () => {
    dispatch(setSelectedRecord([data]));
    dispatch(openDrawer({ title: 'Update Service' }));
  };

  const options: MyTabsOption[] = [
    {
      label: 'Order',
      tabKey: '1',
      value: 1,
      children: <TableOrder serviceId={Number(id)} />,
    },
  ];

  return (
    <Skeleton loading={isLoading} active>
      {errorGetServiceDetail?.code === STATUS_CODE.BAD_REQUEST ? (
        <NotFoundPage />
      ) : (
        <div>
          <Card
            title={
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Title level={3} style={{ margin: 0 }}>
                  Service Detail
                </Title>
                <Button icon={<EditOutlined />} onClick={handleUpdateService} type="primary" />
              </div>
            }
          >
            <Row gutter={[12, 12]}>
              <Col lg={{ span: 8 }} span={24}>
                <Image src={data?.image} alt={data?.image} className="image-border" />
              </Col>
              <Col lg={{ span: 16 }} span={24}>
                <Descriptions bordered column={{ md: 2, sm: 1 }} layout="vertical">
                  <Item label="Name">{data?.name}</Item>
                  <Item label="Status">
                    <Select
                      defaultValue={data?.status}
                      value={data?.status}
                      onChange={(value: SERVICE_STATUS) => handleChangeStatusService(value)}
                    >
                      {Object.values(SERVICE_STATUS).map((status: SERVICE_STATUS) => (
                        <Option value={status} key={status}>
                          {dropdownRenderServiceStatus(status)}
                        </Option>
                      ))}
                    </Select>
                  </Item>
                  <Item label="Price">{data?.price}</Item>
                  <Item label="Unit">{data?.unit}</Item>
                  <Item label="Description">{data?.description}</Item>
                </Descriptions>
              </Col>
            </Row>
          </Card>

          <BaseTabs options={options} />
        </div>
      )}
    </Skeleton>
  );
};

export default ServiceDetailPage;
