import type { FC } from 'react';

import {
  DashboardOutlined,
  InboxOutlined,
  ShopOutlined,
  TableOutlined,
  TagOutlined,
  UserOutlined,
} from '@ant-design/icons';

import { ReactComponent as DashboardSvg } from '@/assets/menu/dashboard.svg';
import { ReactComponent as GuideSvg } from '@/assets/menu/guide.svg';
import { ReactComponent as LockerSvg } from '@/assets/menu/locker.svg';
import { ReactComponent as OrderSvg } from '@/assets/menu/order.svg';
import { ReactComponent as ServiceSvg } from '@/assets/menu/service.svg';
import { ReactComponent as StaffSvg } from '@/assets/menu/staff.svg';
import { ReactComponent as StoreSvg } from '@/assets/menu/store.svg';
import { ICON } from '@/constants/common';

interface CustomIconProps {
  type: string;
}

export const CustomIcon: FC<CustomIconProps> = props => {
  const { type } = props;
  let com = <GuideSvg />;

  switch (type) {
    case ICON.DASHBOARD:
      com = <DashboardOutlined />;
      break;
    case ICON.STORE:
      com = <ShopOutlined />;
      break;
    case ICON.LOCKER:
      com = <TableOutlined />;
      break;
    case ICON.SERVICE:
      com = <TagOutlined />;
      break;
    case ICON.ORDER:
      com = <InboxOutlined />;
      break;
    case ICON.STAFF:
      com = <UserOutlined />;
    default:
      break;
  }

  return <span className="anticon">{com}</span>;
};
