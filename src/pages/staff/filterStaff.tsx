import type { IStaffParams } from '@/interface/staff/staff';
import type { Dispatch, SetStateAction } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form, Input, Select, Space } from 'antd';
import React, { useState } from 'react';

import CustomSearch from '@/components/input/customSearch';
import CustomSelect from '@/components/input/customSelect';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';
import { STAFF_STATUS } from '@/interface/staff/staff';
import { dropdownRenderStaffStatus } from '@/utils/dropdownRender';

import LockerSelect from '../lockerDetail/lockerSelect';
import StoreSelect from '../store/storeSelect';

const { Option } = Select;

interface Props {
  storeId?: number;
  lockerId?: number;
  setParams: Dispatch<SetStateAction<IStaffParams>>;
}

const FilterStaff = ({ setParams, storeId, lockerId }: Props) => {
  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setParams({ pageNumber: 1, pageSize: 10 });
  };

  const onValuesChange = (changedValues: IStaffParams) => {
    setParams((prev: IStaffParams) => ({ ...prev, ...changedValues }));
  };

  return (
    <Form form={form} name="filter-staff" className="filter-group" layout="vertical" onValuesChange={onValuesChange}>
      {/* SEARCH STAFF */}
      <Form.Item label="Search" name="query">
        <CustomSearch<IStaffParams> placeholder="Search..." query="search" setParams={setParams} />
      </Form.Item>

      <ScrollWrapper>
        {/* FILTER STAFF BY STORE */}
        {!storeId && (
          <Form.Item label="Store" name="storeId">
            <StoreSelect
              showSearch
              allowClear
              placeholder="Store"
              onChange={value => setParams((prev: IStaffParams) => ({ ...prev, ...{ storeId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER ORDER BY LOCKER */}
        {!lockerId && (
          <Form.Item label="Locker" name="lockerId">
            <LockerSelect
              showSearch
              allowClear
              placeholder="Locker"
              onChange={value => setParams((prev: IStaffParams) => ({ ...prev, ...{ lockerId: value } }))}
            />
          </Form.Item>
        )}

        {/* FILTER STAFF BY STATUS */}
        <Form.Item label="Status" name="status">
          <CustomSelect<IStaffParams> setParams={setParams} name="status" placeholder="Status">
            <Option value={STAFF_STATUS.ACTIVE}>{dropdownRenderStaffStatus(STAFF_STATUS.ACTIVE)}</Option>
            <Option value={STAFF_STATUS.INACTIVE}>{dropdownRenderStaffStatus(STAFF_STATUS.INACTIVE)}</Option>
          </CustomSelect>
        </Form.Item>

        {/* BUTTON RESET FILTER */}
        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large" className="reset-btn">
          Reset Filters
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default FilterStaff;
