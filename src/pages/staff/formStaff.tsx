import type { ICreateStaffParams, IStaffItem } from '@/interface/staff/staff';
import type { InputRef, UploadFile } from 'antd';

import { Button, Col, Form, Input, Row } from 'antd';
import RandExp from 'randexp';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Toast } from '@/components/toast/toast';
import DraggerUpload from '@/components/upload/draggerUpload';
import { PASSWORD_REGEX, PASSWORD_REGEX_GENERATOR } from '@/constants/common';
import { STORE_STATUS } from '@/interface/store/store';
import { useCreateStaffMutation, useUpdateStaffMutation } from '@/services/staffService';
import { closeDrawer } from '@/stores/layout.store';
import { getFile } from '@/utils/getFile';
import { required } from '@/utils/validate';

import StoreSelect from '../store/storeSelect';

const FormStaff = () => {
  const [form] = Form.useForm();
  const initRef = useRef<InputRef>(null);
  const dispatch = useDispatch();

  // REDUX STATE
  const selectedRecord: IStaffItem[] = useSelector(state => state.table.selectedRecord);
  const { visible } = useSelector(state => state.drawer);

  // STATE
  const [fileMap, setFileMap] = useState<Map<number, UploadFile[]>>(
    selectedRecord[0]
      ? new Map<number, UploadFile[]>().set(0, [
          { uid: '0', url: selectedRecord[0]?.avatar, name: selectedRecord[0]?.avatar },
        ])
      : new Map(),
  );

  const isUpdate = selectedRecord.length > 0;

  useEffect(() => {
    const timeout = setTimeout(() => {
      initRef?.current?.focus();
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [visible]);

  useEffect(() => {
    if (isUpdate) {
      form.setFieldsValue({ ...selectedRecord[0], storeId: selectedRecord[0]?.store?.id });
    } else {
      form.resetFields();
    }
  }, [selectedRecord]);

  // MUTATION
  const [createStaff, { isSuccess, isError, error }] = useCreateStaffMutation();
  const [updateStaff, { isSuccess: isSuccessUpdate, isError: isErrorUpdate, error: errorUpdate }] =
    useUpdateStaffMutation();

  const onFinish = (values: ICreateStaffParams) => {
    isUpdate
      ? updateStaff({
          id: selectedRecord[0].id,
          ...values,
        })
      : createStaff(values);
  };

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(
        () => Toast({ type: 'error', message: 'Error', description: error?.message || 'Create Staff Failed!' }),
        100,
      );
    }

    if (isErrorUpdate) {
      timeout = setTimeout(
        () => Toast({ type: 'error', message: 'Error', description: errorUpdate?.message || 'Update Staff Failed!' }),
        100,
      );
    }

    if (isSuccess) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Create Staff Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
    }

    if (isSuccessUpdate) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Update Staff Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, isSuccessUpdate, isErrorUpdate]);

  return (
    <Form form={form} name="staff" layout="vertical" onFinish={onFinish}>
      <Row gutter={[12, 12]}>
        <Col md={{ span: 12 }} span={24}>
          <Form.Item label="Full Name" name="fullName" rules={[required('Full Name')]}>
            <Input ref={initRef} />
          </Form.Item>
        </Col>

        <Col md={{ span: 12 }} span={24}>
          <Form.Item label="Phone Number" name="phoneNumber" rules={[required('Phone Number')]}>
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={[12, 12]}>
        <Col md={{ span: 12 }} span={24}>
          <Form.Item
            label="Password"
            name="password"
            rules={[
              required('Password'),
              {
                pattern: new RegExp(PASSWORD_REGEX),
                message:
                  'Password must have minimum 8 characters, contain at least one lower character, one upper character, digit or special symbol',
              },
            ]}
            initialValue={new RandExp(PASSWORD_REGEX_GENERATOR).gen()}
          >
            <Input.Password disabled={isUpdate} />
          </Form.Item>
        </Col>

        <Col md={{ span: 12 }} span={24}>
          <Form.Item label="Store" name="storeId" rules={[required('Store')]}>
            <StoreSelect showSearch allowClear storeStatus={STORE_STATUS.ACTIVE} size="middle" />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item label="Avatar" name={'avatar'} rules={[required('Image')]} getValueFromEvent={getFile}>
        <DraggerUpload
          multiple={false}
          fileMap={fileMap}
          setFileMap={setFileMap}
          fieldKey={0}
          name="avatar"
          form={form}
        />
      </Form.Item>

      <Form.Item label="Description" name="description">
        <Input.TextArea rows={4} />
      </Form.Item>

      <Button type="primary" htmlType="submit" form="staff" style={{ float: 'right' }}>
        {isUpdate ? 'Update' : 'Create'}
      </Button>
    </Form>
  );
};

export default FormStaff;
