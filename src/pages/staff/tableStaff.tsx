import type { IStaffItem, IStaffParams, STAFF_STATUS } from '@/interface/staff/staff';
import type { IStoreItem } from '@/interface/store/store';
import type { ColumnsType } from 'antd/es/table';

import { Button, Image, Space, Tooltip, Typography } from 'antd';
import dayjs from 'dayjs';
import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import PersonPlaceholder from '@/assets/placeholder/person-placeholder.png';
import TableCustom from '@/components/core/table';
import CustomPagination from '@/components/pagination/customPagination';
import { FULL_TIME_FORMAT } from '@/constants/common';
import { useStaffsQuery } from '@/services/staffService';
import { dropdownRenderStaffStatus } from '@/utils/dropdownRender';

import FilterStaff from './filterStaff';

const { Paragraph } = Typography;

interface Props {
  storeId?: number;
  lockerId?: number;
}

const TableStaff: React.FC<Props> = ({ storeId, lockerId }) => {
  const navigate = useNavigate();

  // STATE
  const [staffParams, setStaffParams] = useState<IStaffParams>({ pageNumber: 1, pageSize: 20, storeId, lockerId });

  // QUERY
  const { data, isLoading } = useStaffsQuery(staffParams);

  // TABLE COLUMNS
  const tableColumns: ColumnsType<IStaffItem> = [
    {
      title: 'Avatar',
      dataIndex: 'avatar',
      key: 'avatar',
      width: 150,
      render: (avatar: string) => <Image src={avatar || PersonPlaceholder} alt={avatar} />,
    },
    {
      title: 'Full Name',
      dataIndex: 'fullName',
      key: 'fullName',
    },
    {
      title: 'Phone Number',
      dataIndex: 'phoneNumber',
      key: 'phoneNumber',
      align: 'center',
      render: (phoneNumber: number) => phoneNumber && <Paragraph copyable>{phoneNumber}</Paragraph>,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status: STAFF_STATUS) => dropdownRenderStaffStatus(status),
    },
    {
      title: 'Store',
      dataIndex: 'store',
      key: 'store',
      ellipsis: true,
      render: (store: IStoreItem) => (
        <Tooltip placement="bottom" title={store?.name}>
          <Link to={`/stores/${store?.id}`}>{store?.name}</Link>
        </Tooltip>
      ),
    },
    {
      title: 'Created At',
      dataIndex: 'createdAt',
      key: 'createdAt',
      align: 'center',
      sorter: {
        compare: () => {
          setStaffParams({
            ...staffParams,
            sortColumn: 'createdAt',
            sortDir: staffParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
          });

          return 1;
        },
      },
      render: (createdAt: string) => dayjs(createdAt).format(FULL_TIME_FORMAT),
    },
    {
      title: 'Updated At',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      align: 'center',
      sorter: {
        compare: () => {
          setStaffParams({
            ...staffParams,
            sortColumn: 'updatedAt',
            sortDir: staffParams.sortDir === 'Asc' ? 'Desc' : 'Asc',
          });

          return 1;
        },
      },
      render: (updatedAt: string) => dayjs(updatedAt).format(FULL_TIME_FORMAT),
    },
    {
      title: 'Action',
      key: 'action',
      align: 'center',
      fixed: 'right',
      render: (_: unknown, record: IStaffItem) => (
        <Button onClick={() => navigate(`/staffs/${record?.id}`)}>Details</Button>
      ),
    },
  ];

  return (
    <Space direction="vertical">
      <FilterStaff setParams={setStaffParams} storeId={storeId} lockerId={lockerId} />

      <TableCustom<IStaffItem>
        sortDirections={['ascend', 'descend']}
        showSorterTooltip={false}
        bordered
        loading={isLoading}
        rowKey={(record: IStaffItem) => record.id.toString()}
        dataSource={data?.items}
        columns={tableColumns}
      />

      <CustomPagination<IStaffParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setStaffParams}
      />
    </Space>
  );
};

export default TableStaff;
