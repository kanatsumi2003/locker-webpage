import type { IStaffItem, IStaffParams, STAFF_STATUS } from '@/interface/staff/staff';
import type { SelectProps } from 'antd';
import type { FC } from 'react';

import React, { useEffect, useState } from 'react';

import InfiniteSelect from '@/components/infiniteSelect/infiniteSelect';
import { useStaffsQuery } from '@/services/staffService';

interface Props extends SelectProps {
  staffStatus?: STAFF_STATUS;
  storeId?: number;
}

const StaffSelect: FC<Props> = ({ staffStatus, storeId, ...rest }) => {
  const [staffParams, setStaffParams] = useState<IStaffParams>({
    pageNumber: 1,
    pageSize: 10,
    status: staffStatus,
    storeId,
  });

  // QUERY
  const { data } = useStaffsQuery(staffParams);

  const onLoad = (params: any) => {
    setStaffParams(params);
  };

  const dataSelect = data?.items?.map((item: IStaffItem) => ({
    label: item?.fullName,
    value: item?.id,
  }));

  useEffect(() => {
    setStaffParams(prev => ({ ...prev, storeId }));
  }, [storeId]);

  return (
    <InfiniteSelect {...rest} onLoad={onLoad} data={dataSelect} filter={{ status: staffStatus, storeId: storeId }} />
  );
};

export default StaffSelect;
