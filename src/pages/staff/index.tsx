import { PlusOutlined } from '@ant-design/icons';
import { Button, Space } from 'antd';
import { useDispatch } from 'react-redux';

import { openDrawer } from '@/stores/layout.store';

import TableStaff from './tableStaff';

const StaffPage = () => {
  const dispatch = useDispatch();

  const handleCreateStaff = () => {
    dispatch(openDrawer({ title: 'Create Staff' }));
  };

  return (
    <>
      <Space style={{ margin: '1rem 0' }}>
        <Button type="primary" onClick={handleCreateStaff}>
          <PlusOutlined />
        </Button>
      </Space>
      <TableStaff />
    </>
  );
};

export default StaffPage;
