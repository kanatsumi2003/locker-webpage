import type { IServiceItem } from '@/interface/service/service';

import { Button, Form, Modal } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { Toast } from '@/components/toast/toast';
import { useCreateServiceMutation, useUpdateServiceMutation } from '@/services/serviceService';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';

import FormService from './formService';

const { confirm } = Modal;

const Service = () => {
  const [form] = Form.useForm();
  const { id } = useParams();
  const dispatch = useDispatch();
  const selectedRecord: IServiceItem[] = useSelector(state => state.table.selectedRecord);

  // MUTATION
  const [createService, { isSuccess, isError }] = useCreateServiceMutation();
  const [updateService, { isSuccess: isSuccessUpdate, isError: isErrorUpdate }] = useUpdateServiceMutation();

  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: 'Create Service Failed!' });
      }, 100);
    }

    if (isErrorUpdate) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: 'Update Service Failed!' });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Create Service Successfully!' });
        dispatch(closeDrawer());
      }, 100);
    }

    if (isSuccessUpdate) {
      timeout = setTimeout(() => {
        Toast({ type: 'success', message: 'Success', description: 'Update Service Successfully!' });
        dispatch(closeDrawer());
      }, 100);
      dispatch(refreshRowKeys());
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, isSuccessUpdate, isErrorUpdate]);

  useEffect(() => {
    selectedRecord.length ? form.setFieldsValue(selectedRecord[0]) : form.resetFields();
  }, [selectedRecord]);

  const onFinish = (values: IServiceItem) => {
    if (selectedRecord.length) {
      confirm({
        title: 'Confirm',
        content: 'Are you sure to update this service?',
        // onOk: () => updateService({ lockerId: Number(id), serviceId: selectedRecord[0]?.id, ...values }),
      });
    } else {
      // createService({
      //   lockerId: Number(id),
      //   ...values,
      // });
    }
  };

  return (
    <>
      <Form form={form} name="service" layout="vertical" onFinish={onFinish}>
        {/* <FormService form={form} /> */}
      </Form>
      <div style={{ float: 'right', marginTop: '1rem' }}>
        <Button htmlType="submit" form="service" type="primary">
          Save
        </Button>
      </div>
    </>
  );
};

export default Service;
