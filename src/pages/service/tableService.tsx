import type { IServiceItem, IServiceParams, SERVICE_STATUS } from '@/interface/service/service';
import type { ColumnsType } from 'antd/es/table';

import { Button, Image, Space, Tooltip } from 'antd';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import TableCustom from '@/components/core/table';
import CustomPagination from '@/components/pagination/customPagination';
import { useServicesQuery } from '@/services/serviceService';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import { dropdownRenderServiceStatus } from '@/utils/dropdownRender';

import FilterService from './filterService';

const TableService = () => {
  const navigate = useNavigate();

  // STATE
  const [serviceParams, setServiceParams] = useState<IServiceParams>({ pageNumber: 1, pageSize: 20 });

  // QUERY
  const { data, isLoading, isError } = useServicesQuery(serviceParams);

  // TABLE COLUMNS
  const tableColumns: ColumnsType<IServiceItem> = [
    {
      title: 'Image',
      dataIndex: 'image',
      key: 'image',
      align: 'center',
      width: 150,
      render: (image: string) => <Image src={image} alt={image} height={100} />,
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      ellipsis: true,
      render: (name: string) => (
        <Tooltip placement="bottom" title={name}>
          {name}
        </Tooltip>
      ),
    },
    {
      title: 'Price',
      key: 'price',
      dataIndex: 'price',
      align: 'right',
      render: (price: number) => <>{formatDecimalPrecision(price)}</>,
    },
    {
      title: 'Unit',
      key: 'unit',
      dataIndex: 'unit',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      align: 'center',
      render: (status: SERVICE_STATUS) => dropdownRenderServiceStatus(status),
    },
    {
      title: 'Action',
      key: 'action',
      align: 'center',
      fixed: 'right',
      render: (_: unknown, record: IServiceItem) => (
        <Button onClick={() => navigate(`/services/${record.id}`)}>Details</Button>
      ),
    },
  ];

  return (
    <Space direction="vertical">
      <FilterService setParams={setServiceParams} />
      <TableCustom<IServiceItem>
        sortDirections={['ascend', 'descend']}
        showSorterTooltip={false}
        bordered
        loading={isLoading}
        rowKey={(record: IServiceItem) => record.id}
        dataSource={data?.items}
        columns={tableColumns}
      />
      <CustomPagination<IServiceParams>
        pageNumber={data?.pageNumber}
        pageSize={data?.pageSize}
        total={data?.totalCount}
        setParams={setServiceParams}
      />
    </Space>
  );
};

export default TableService;
