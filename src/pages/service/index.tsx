import { PlusOutlined } from '@ant-design/icons';
import { Button, Space } from 'antd';
import { useDispatch } from 'react-redux';

import { openDrawer } from '@/stores/layout.store';

import TableService from './tableService';

const ServicePage = () => {
  const dispatch = useDispatch();

  // ACTIVITY ON CLICK BUTTON
  const handleCreateService = () => {
    dispatch(openDrawer({ title: 'Create Service' }));
  };

  return (
    <>
      <Space style={{ margin: '1rem 0' }}>
        <Button type="primary" onClick={handleCreateService}>
          <PlusOutlined />
        </Button>
      </Space>

      <TableService />
    </>
  );
};

export default ServicePage;
