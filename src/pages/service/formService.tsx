import type { IServiceItem } from '@/interface/service/service';
import type { InputRef } from 'antd';
import type { UploadFile } from 'antd/es/upload';

import { PoundCircleOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, InputNumber, Row, Tag } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Toast } from '@/components/toast/toast';
import DraggerUpload from '@/components/upload/draggerUpload';
import { CURRENCY_UNIT } from '@/constants/common';
import { FEE_TYPE } from '@/interface/service/service';
import { useCreateServiceMutation, useUpdateServiceMutation } from '@/services/serviceService';
import { closeDrawer } from '@/stores/layout.store';
import { refreshRowKeys } from '@/stores/table.store';
import { formatDecimalPrecision } from '@/utils/decimalPrecision';
import { getFile } from '@/utils/getFile';
import { insertMiddleString } from '@/utils/insertMiddleString';
import { required } from '@/utils/validate';

const FormService = () => {
  const [form] = Form.useForm();
  const initRef = useRef<InputRef>(null);
  const dispatch = useDispatch();

  // REDUX STATE
  const selectedRecord: IServiceItem[] = useSelector(state => state.table.selectedRecord);
  const { visible } = useSelector(state => state.drawer);

  // STATE
  const [fileMap, setFileMap] = useState<Map<number, UploadFile[]>>(
    selectedRecord[0]
      ? new Map<number, UploadFile[]>().set(0, [
          { uid: '0', url: selectedRecord[0]?.image, name: selectedRecord[0]?.image },
        ])
      : new Map(),
  );

  // MUTATION
  const [createService, { isSuccess, isError, error }] = useCreateServiceMutation();
  const [updateService, { isSuccess: isSuccessUpdate, isError: isErrorUpdate, error: errorUpdate }] =
    useUpdateServiceMutation();

  const isUpdate = selectedRecord.length;

  // FOCUS ON FIRST FORM FIELD
  useEffect(() => {
    const timeout = setTimeout(() => {
      initRef?.current?.focus();
    }, 100);

    return () => {
      clearTimeout(timeout);
    };
  }, [visible]);

  const onFinish = (values: IServiceItem) => {
    isUpdate ? updateService({ ...values, id: Number(selectedRecord[0]?.id) }) : createService(values);
  };

  useEffect(() => {
    isUpdate
      ? form.setFieldsValue({
          ...selectedRecord[0],
        })
      : form.resetFields();
  }, [selectedRecord]);

  // NOTIFICATION
  useEffect(() => {
    let timeout: NodeJS.Timeout;

    if (isError) {
      timeout = setTimeout(() => {
        Toast({ type: 'error', message: 'Error', description: error?.message || 'Create Service Failed!' });
      }, 100);
    }

    if (isErrorUpdate) {
      timeout = setTimeout(() => {
        Toast({
          type: 'error',
          message: 'Error',
          description: errorUpdate?.message || 'Update Service Failed!',
        });
      }, 100);
    }

    if (isSuccess) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Create Service Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
    }

    if (isSuccessUpdate) {
      timeout = setTimeout(
        () => Toast({ type: 'success', message: 'Success', description: 'Update Service Successfully!' }),
        100,
      );
      dispatch(closeDrawer());
      dispatch(refreshRowKeys());
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isSuccess, isError, isSuccessUpdate, isErrorUpdate]);

  return (
    <Form form={form} name="service" layout="vertical" onFinish={onFinish}>
      <Row gutter={[12, 12]}>
        <Col md={{ span: 8 }} span={24}>
          <Form.Item label="Name" name="name" rules={[required('Name')]}>
            <Input size="large" />
          </Form.Item>
        </Col>

        <Col md={{ span: 8 }} span={24}>
          <Form.Item label="Price" name="price" rules={[required('Price')]}>
            <InputNumber
              size="large"
              addonAfter={CURRENCY_UNIT.VI}
              style={{ width: '100%' }}
              min={0}
              formatter={value => formatDecimalPrecision(Number(value)) ?? ''}
            />
          </Form.Item>
        </Col>

        <Col md={{ span: 8 }} span={24}>
          <Form.Item label="Unit" name="unit" rules={[required('Unit')]}>
            <Input size="large" />
          </Form.Item>
        </Col>
      </Row>

      <Form.Item label="Image" name={'image'} rules={[required('Image')]} getValueFromEvent={getFile}>
        <DraggerUpload
          multiple={false}
          fileMap={fileMap}
          setFileMap={setFileMap}
          fieldKey={0}
          name="image"
          form={form}
        />
      </Form.Item>

      <Form.Item label="Description" name={'description'}>
        <Input.TextArea rows={3} />
      </Form.Item>

      <Button type="primary" htmlType="submit" form="service" style={{ float: 'right' }}>
        {isUpdate ? 'Update' : 'Create'}
      </Button>
    </Form>
  );
};

export default FormService;
