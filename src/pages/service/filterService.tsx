import type { IServiceParams } from '@/interface/service/service';
import type { Dispatch, SetStateAction } from 'react';

import { UndoOutlined } from '@ant-design/icons';
import { Button, Form, Select, Space } from 'antd';
import React from 'react';

import CustomSearch from '@/components/input/customSearch';
import CustomSelect from '@/components/input/customSelect';
import ScrollWrapper from '@/components/scrollWrapper/scrollWrapper';
import { SERVICE_STATUS } from '@/interface/service/service';
import { dropdownRenderServiceStatus } from '@/utils/dropdownRender';

interface Props {
  setParams: Dispatch<SetStateAction<IServiceParams>>;
}

const { Option } = Select;

const FilterService = ({ setParams }: Props) => {
  const [form] = Form.useForm();

  const handleResetFilter = () => {
    form.resetFields();
    setParams({ pageNumber: 1, pageSize: 10 });
  };

  const onValuesChange = (changedValues: IServiceParams) => {
    setParams((prev: IServiceParams) => ({ ...prev, ...changedValues }));
  };

  return (
    <Form form={form} name="filter-service" className="filter-group" layout="vertical" onValuesChange={onValuesChange}>
      {/* SEARCH SERVICE */}
      <Form.Item label="Search" name="search">
        <CustomSearch<IServiceParams> placeholder="Search..." query="search" setParams={setParams} />
      </Form.Item>

      <ScrollWrapper>
        <Form.Item label="Status" name="status">
          <CustomSelect<IServiceParams> setParams={setParams} name="status" placeholder="Status">
            {Object.values(SERVICE_STATUS).map((status: SERVICE_STATUS) => (
              <Option value={status} key={status}>
                {dropdownRenderServiceStatus(status)}
              </Option>
            ))}
          </CustomSelect>
        </Form.Item>

        {/* BUTTON RESET FILTER */}
        <Button icon={<UndoOutlined />} onClick={handleResetFilter} size="large" className="reset-btn">
          Reset Filters
        </Button>
      </ScrollWrapper>
    </Form>
  );
};

export default FilterService;
