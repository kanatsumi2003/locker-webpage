import './styles/index.less';
import './mock';

import { ThemeProvider } from '@emotion/react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './App';
import { theme } from './constants/theme';
import store from './stores';

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>,
  document.getElementById('root'),
);
